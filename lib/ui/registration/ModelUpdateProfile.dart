class ModelUpdateProfile {
  String success;
  String status;
  String message;
  Data data;

  ModelUpdateProfile({this.success, this.status, this.message, this.data});

  ModelUpdateProfile.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String id;
  String title;
  String email;
  String phoneNumber;
  String startTime;
  String endTime;
  String status;

  Data(
      {this.id,
        this.title,
        this.email,
        this.phoneNumber,
        this.startTime,
        this.endTime,
        this.status});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['status'] = this.status;
    return data;
  }
}
