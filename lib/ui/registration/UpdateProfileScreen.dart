import 'dart:io';

import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:amen_inch/ui/registration/RegisterActions.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/ui/registration/RegisterViewModel.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:amen_inch/utils/Validation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gender_selection/gender_selection.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';

class UpdateProfileScreen extends StatefulWidget {
  bool isForEdit;
  ModelUserProfile userProfile;

  UpdateProfileScreen(
      {Key key, @required this.isForEdit, @required this.userProfile})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return Register();
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);

class Register extends State<UpdateProfileScreen> {
  TextEditingController nameController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController countryController = TextEditingController();

  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  DateTime selectedDate = DateTime.now();

  Country selectedCountry = CountryPickerUtils.getCountryByIsoCode('IN');
  final TextEditingController controller = TextEditingController();

  FocusNode firstNameNode = new FocusNode();
  FocusNode ageNode = new FocusNode();
  FocusNode emailNode = new FocusNode();
  FocusNode countryNode = new FocusNode();
  FocusNode phoneNode = new FocusNode();
  FocusNode addressNode = new FocusNode();
  final _formKey = GlobalKey<FormState>();

  String selectedGender, selectedImage = "";

  PickedFile _imageFile;
  dynamic _pickImageError;
  bool isFromSelection = false;
  String _retrieveDataError;

  final ImagePicker _picker = ImagePicker();
  Store _store;

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
        maxWidth: 512,
        maxHeight: 512,
        imageQuality: 100,
      );

      _store.dispatch(performUpdateWithImage(_store, File(pickedFile.path)));

      setState(() {
        _imageFile = pickedFile;
        isFromSelection = true;
      });
    } catch (e) {
      setState(() {
        _pickImageError = e;
        isFromSelection = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, RegisterViewModel>(
      converter: (store) => RegisterViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) {
        _store = store;
        if (widget.isForEdit) {
          nameController.text = widget.userProfile.data.name;
         // ageController.text = widget.userProfile.data.age;
         // selectedGender = widget.userProfile.data.gender;
          DateFormat dateFormat = DateFormat("dd-MM-yyyy");

          DateTime dateTime;

         /* if (widget.userProfile.data.dob.isNotEmpty) {
            //dateTime = dateFormat.parse(widget.userProfile.data.dob);
            selectedDate = dateTime;
          }*/
          emailController.text = widget.userProfile.data.email;
          phoneController.text = widget.userProfile.data.phoneNumber;
         // countryController.text = widget.userProfile.data.countryCode;
          selectedImage = widget.userProfile.data.profileImage;
          //getPhoneNumber(widget.userProfile.data.phoneNumber);
        }
      },
    );
  }

  buildContent(RegisterViewModel viewModel, BuildContext context) {
    String title = widget.isForEdit
        ? AppUtils.getLocalizedString("EDIT_PROFILE")
        : AppUtils.getLocalizedString("REGISTER");

    return UIHelper.getScreen(
      scaffold: Scaffold(
        appBar: UIHelper.getAppBar(AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            title, 0, false, context, "", ""),
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      _onImageButtonPressed(ImageSource.gallery,
                          context: context);
                    },
                    child: Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(top: 20),
                          width: 150,
                          height: 150,
                          padding: EdgeInsets.all(5.0),
                          child: !isFromSelection
                              ? CachedNetworkImage(
                                  imageUrl: selectedImage,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    width: 150.0,
                                    height: 150.0,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                  placeholder: (context, url) =>
                                      CircularProgressIndicator(),
                                  errorWidget: (context, url, error) =>
                                      Icon(Icons.error),
                                )
                              : Container(
                                  width: 150,
                                  height: 150,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: FileImage(File(_imageFile.path)),
                                        fit: BoxFit.fill),
                                  ),
                                )),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: nameController,
                      keyboardType: TextInputType.text,
                      focusNode: firstNameNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, Constants.ENTER_FIRST_NAME);
                      },
                      onFieldSubmitted: (v) {},
                      decoration: InputDecoration(
                        labelText: AppUtils.getLocalizedString("NAME"),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: GenderSelection(
                      size: 50,
                      femaleImage: NetworkImage(
                          "https://cdn1.iconfinder.com/data/icons/website-internet/48/website_-_female_user-512.png"),
                      maleImage: NetworkImage(
                          "https://icon-library.net/images/avatar-icon/avatar-icon-4.jpg"),
                      selectedGenderIconBackgroundColor: Colors.amber,
                      selectedGenderTextStyle: TextStyle(
                          color: Colors.amber,
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                      selectedGender: this.selectedGender ==
                              AppUtils.getLocalizedString("MALE")
                          ? Gender.Male
                          : Gender.Female,
                      onChanged: (Gender gender) {
                        print(gender);
                        if (gender.index == 0) {
                          this.selectedGender =
                              AppUtils.getLocalizedString("MALE");
                        } else {
                          this.selectedGender =
                              AppUtils.getLocalizedString("FEMALE");
                        }
                      },
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _selectDate(context);
                    },
                    child: Container(
                      padding: EdgeInsets.only(top: 10, left: 5),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        children: <Widget>[
                          Text(
                            AppUtils.getLocalizedString("DATE_OF_BIRTH"),
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.black,
                            ),
                          ),
                          Container(
                            padding:
                                EdgeInsets.only(top: 10, bottom: 10, left: 5),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text("${selectedDate.toLocal()}".split(' ')[0]),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 1,
                    child: Container(
                      margin: EdgeInsets.all(10),
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(
                    height: 1,
                    child: Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      color: Colors.grey,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: ageController,
                      keyboardType: TextInputType.number,
                      focusNode: ageNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, Constants.ENTER_FIRST_NAME);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(emailNode);
                      },
                      decoration: InputDecoration(
                        labelText:    AppUtils.getLocalizedString("AGE"),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      focusNode: emailNode,
                      validator: (t) {
                        return Validation().validateEmail(t);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(countryNode);
                      },
                      decoration: InputDecoration(
                        labelText:  AppUtils.getLocalizedString("EMAIL"),
                      ),
                    ),
                  ),
                  /* Container(
                    child: InternationalPhoneNumberInput(
                      onInputChanged: (PhoneNumber number) {
                        print(number.phoneNumber);
                          this.number = number;
                      },
                      onInputValidated: (bool value) {
                        print(value);
                      },
                      selectorConfig: SelectorConfig(
                        selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
                      ),
                      ignoreBlank: true,
                      autoValidateMode: AutovalidateMode.disabled,
                      selectorTextStyle: TextStyle(color: Colors.black),
                      initialValue: number,
                      textFieldController: phoneController,
                      inputBorder:
                          OutlineInputBorder(borderSide: BorderSide.none),
                      formatInput: false,
                      keyboardType: TextInputType.numberWithOptions(
                          signed: true, decimal: true),
                      onSaved: (PhoneNumber number) {
                        print('On Saved: $number');
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(phoneNode);
                      },
                    ),
                  ),*/
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    margin: EdgeInsets.only(bottom: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: CountryPickerDropdown(
                            initialValue: 'in',
                            itemBuilder: _buildDropdownItem,
                            onValuePicked: (Country country) {
                              selectedCountry = country;
                              print("${country.name}");
                            },
                          ),
                        ),
                        Expanded(
                          child: TextFormField(
                            focusNode: phoneNode,
                            controller: phoneController,
                            validator: (t) {
                              return Validation().validatePhoneNumber(t);
                            },
                            decoration: InputDecoration(
                              labelText: AppUtils.getLocalizedString("PHONE_NUMBER"),
                              border: InputBorder.none,
                            ),
                            onFieldSubmitted: (v) {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 1,
                    child: Container(
                      color: Colors.grey,
                    ),
                  ),
                  /* Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: countryController,
                      keyboardType: TextInputType.phone,
                      focusNode: countryNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, Constants.ENTER_FIRST_NAME);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(phoneNode);
                      },
                      decoration: InputDecoration(
                        labelText: 'Country Code',
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: phoneController,
                      keyboardType: TextInputType.phone,
                      focusNode: phoneNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, Constants.ENTER_FIRST_NAME);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(emailNode);
                      },
                      decoration: InputDecoration(
                        labelText: 'Phone Number',
                      ),
                    ),
                  ),*/
                  SizedBox(
                    height: 1,
                    child: Container(
                      margin: EdgeInsets.only(left: 10, right: 10),
                      color: Colors.grey,
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 20),
                      child: SizedBox(
                        height: 50,
                        child: UIHelper.getFlatButton(context,
                            backGroundColor:
                                AppUtils.hexToColor(AppConfig.COLOR_SECONDARY),
                            buttonName: AppUtils.getLocalizedString("UPDATE"), doOnPress: () {
                          if (_formKey.currentState.validate()) {
                            print(nameController.text);
                            print(ageController.text);
                            print(emailController.text);
                            print(phoneController.text);
                            _performRegister(viewModel);
                          }
                        }),
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDropdownItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}(${country.isoCode})"),
          ],
        ),
      );

  onFailure(String error) {
    _showMessage(error);
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1980, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<void> _performRegister(RegisterViewModel viewModel) async {
    if (selectedCountry == null) {
      _showMessage(AppUtils.getLocalizedString("PLEASE_SELECT_COUNTRY_CODE"));
      return;
    }

    String phoneNumber =
        phoneController.text.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
    phoneNumber.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String formatted = formatter.format(selectedDate);

    viewModel.update(
        nameController.text,
        selectedGender,
        ageController.text,
        formatted,
        emailController.text,
        phoneNumber,
        "+" + selectedCountry.phoneCode,
        onFailure);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
