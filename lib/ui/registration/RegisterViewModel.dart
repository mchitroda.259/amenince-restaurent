import 'dart:io';

import 'package:amen_inch/ui/registration/RegisterModel.dart';
import 'package:redux/redux.dart';

import 'RegisterActions.dart';

class RegisterViewModel {
  bool isLoading;
  Function(String, String, String, String, String, String, String, String,
      Function) register;
  Function(String, String, String, String, String, String, String, Function)
      update;
  String name,
      gender,
      dob,
      email,
      phoneNumber,
      countryCode,
      password,
      deviceToken;
  File file;
  RegisterModel registerModel;

  RegisterViewModel({
    this.isLoading,
    this.register(
      String name,
      String gender,
      String dob,
      String email,
      String phoneNumber,
      String countryCode,
      String password,
      String deviceToken,
      Function onFailure,
    ),
    this.update(
      String name,
      String gender,
      String age,
      String dob,
      String email,
      String phoneNumber,
      String countryCode,
      Function onFailure,
    ),
    this.registerModel,
    this.name,
    this.gender,
    this.dob,
    this.email,
    this.phoneNumber,
    this.countryCode,
    this.password,
    this.deviceToken,
    this.file
  });

  static RegisterViewModel fromStore(Store store) {
    return RegisterViewModel(
      isLoading: store.state.registerState.isLoading,
      registerModel: store.state.registerState.registerModel,
      name: store.state.registerState.name,
      gender: store.state.registerState.gender,
      dob: store.state.registerState.dob,
      email: store.state.registerState.email,
      phoneNumber: store.state.registerState.phoneNumber,
      countryCode: store.state.registerState.countryCode,
      password: store.state.registerState.password,
      deviceToken: store.state.registerState.deviceToken,
      register: (String name,
          String gender,
          String dob,
          String email,
          String phoneNumber,
          String countryCode,
          String password,
          String deviceToken,
          Function onFailure) {
        store.dispatch(registerAction(store, name, gender, dob, email,
            phoneNumber, countryCode, password, deviceToken, onFailure));
      },
      update: (String name, String gender, String age, String dob, String email,
          String phoneNumber, String countryCode, Function onFailure) {
        store.dispatch(updateAction(store, name, gender, age, dob, email,
            phoneNumber, countryCode, onFailure));
      },
    );
  }
}
