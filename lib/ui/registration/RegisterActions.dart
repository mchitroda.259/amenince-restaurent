import 'dart:io';

import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/ui/otpVerification/OTPPassScreen.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'ModelUpdateProfile.dart';
import 'RegisterModel.dart';
import 'RegistrerReducer.dart';
import 'package:http/http.dart' as http;

ThunkAction registerAction(
    Store store,
    String name,
    String gender,
    String dob,
    String email,
    String phoneNumber,
    String countryCode,
    String password,
    String deviceToken,
    Function onFailure) {
  return (Store store) async {
    new Future(() async {
      store.dispatch(new SetLoadingForRegister(true));
      performRegister(store, name, gender, dob, email, phoneNumber, countryCode,
              password, deviceToken)
          .then((loginModel) async {
        store.dispatch(new SetLoadingForRegister(false));
        if (loginModel.success == "1") {

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken(loginModel.data.token);
          PreferencesHelper.setUserId(loginModel.data.id);
          Constants.userId = loginModel.data.id;

          NavigationHelper().pushScreen(OTPScreen(
            phoneNumber: phoneNumber,
            countryCode: countryCode,
          ));
        } else {
          onFailure(loginModel.message.toString());
          print(loginModel.message.toString());
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForRegister(false));
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<RegisterModel> performRegister(
    Store store,
    String name,
    String gender,
    String dob,
    String email,
    String phoneNumber,
    String countryCode,
    String password,
    String deviceToken) async {
  ApiProvider apiProvider = ApiProvider();
  Map body;
  body = {
    'name': name,
    /*  'gender': gender,
    'dob': dob,*/
    'email': email,
    'phone_number': phoneNumber,
    'country_code': countryCode,
    'password': password,
    'device_token': deviceToken
  };

  print("Params : " + body.toString());

  final response = await apiProvider
      .doPost(APIEndpoints.BASE_URL, APIEndpoints.REGISTRATION, body: body);
  return RegisterModel.fromJson(response);
}

ThunkAction updateAction(
    Store store,
    String name,
    String gender,
    String age,
    String dob,
    String email,
    String phoneNumber,
    String countryCode,
    Function onFailure) {
  return (Store store) async {
    new Future(() async {
      store.dispatch(new SetLoadingForRegister(true));
      performUpdate(
              store, name, gender, age, dob, email, phoneNumber, countryCode)
          .then((loginModel) async {
        store.dispatch(new SetLoadingForRegister(false));
        if (loginModel is ModelUpdateProfile) {
          if (loginModel.success == "1") {
            onFailure(loginModel.message);
            NavigationHelper().pushScreenOnTop(Routes.MAIN_SCREEN);
          } else {
            onFailure(AppUtils.getLocalizedString("FAILED_TO_UPDATE_PROFILE"));
            print(loginModel.message.toString());
            store.dispatch(new SetLoadingForRegister(false));
          }
        } else {
          store.dispatch(new SetLoadingForRegister(false));
          if (loginModel.status == "401") {
            onFailure(loginModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(loginModel.message.toString());
            print(loginModel.message.toString());
          }
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForRegister(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performUpdate(
    Store store,
    String name,
    String gender,
    String age,
    String dob,
    String email,
    String phoneNumber,
    String countryCode) async {
  ApiProvider apiProvider = ApiProvider();
  Map body;
  body = {
    'name': name,
    'gender': gender,
    'age': age,
    'dob': dob,
    'email': email,
    'phone_number': phoneNumber,
    'country_code': countryCode,
  };

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.UPDATE_PROFILE,
      body: body);
  ResponseModel responseModel = ResponseModel.fromJson(response);

  if (responseModel.status == "200")
    return ModelUpdateProfile.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}

Future<dynamic> performUpdateWithImage(Store store, File file) async {
  ApiProvider apiProvider = ApiProvider();
  String token = await PreferencesHelper.tokenString;
  print('TOKEN :- ' + token);
  var uri = Uri.parse(APIEndpoints.BASE_URL + APIEndpoints.UPDATE_PROFILE_PIC);
  var request = http.MultipartRequest('POST', uri);

  Map<String, String> headers = {
    'Content-Type': 'multipart/form-data',
    'Authorization': 'Bearer $token',
    'Accept-Language': 'en',
  };

  request.headers.addAll(headers);

  await http.MultipartFile.fromPath('profile_image', file.path)
      .then((value) => request.files.add(value));

  final response = await apiProvider.doMultipartRequest(request);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelUpdateProfile.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}
