import 'package:amen_inch/ui/registration/RegisterState.dart';
import 'package:redux/redux.dart';

final registerReducer = combineReducers<RegisterState>([
  TypedReducer<RegisterState, SetLoadingForRegister>(_setLoadingForRegister),
  TypedReducer<RegisterState, SetPerformRegister>(_setPerformRegister),
]);

RegisterState _setPerformRegister(
    RegisterState state, SetPerformRegister action) {
  return state.copyWith(
      name: action.name,
      gender: action.gender,
      dob: action.dob,
      email: action.email,
      phoneNumber: action.phoneNumber,
      countryCode: action.countryCode,
      password: action.password,
      deviceToken: action.deviceToken);
}

RegisterState _setLoadingForRegister(
    RegisterState state, SetLoadingForRegister action) {
  return state.copyWith(isLoading: action.isLoading);
}

class SetLoadingForRegister {
  bool isLoading;

  SetLoadingForRegister(this.isLoading);
}

class SetPerformRegister {
  String name;
  String gender;
  String dob;
  String email;
  String phoneNumber;
  String countryCode;
  String password;
  String deviceToken;

  SetPerformRegister(this.name, this.gender, this.dob, this.email,
      this.phoneNumber, this.countryCode, this.password, this.deviceToken);
}
