import 'package:meta/meta.dart';

import 'RegisterModel.dart';

class RegisterState {
  bool isLoading = false;
  bool isLoggedin = false;

  RegisterModel registerModel;
  String name = "",
      gender = "",
      dob = "",
      email = "",
      phoneNumber = "",
      countryCode = "",
      password = "",
      deviceToken = "";

  RegisterState(
      {@required this.isLoggedin,
      @required this.isLoading,
      @required this.registerModel,
      this.name,
      this.gender,
      this.dob,
      this.email,
      this.phoneNumber,
      this.countryCode,
      this.password,
      this.deviceToken});

  factory RegisterState.initial() {
    return new RegisterState(
        isLoggedin: false,
        isLoading: false,
        registerModel: null,
        name: null,
        gender: null,
        dob: null,
        phoneNumber: null,
        countryCode: null,
        password: null,
        deviceToken: null);
  }

  RegisterState copyWith(
      {
      bool isLoading,
        RegisterModel registerModel,
        String name,
        String gender,
        String dob,
        String email,
        String phoneNumber,
        String countryCode,
        String password,
        String deviceToken}) {
    return new RegisterState(
        isLoading: isLoading ?? this.isLoading,
        registerModel: registerModel ?? this.registerModel,
        name: name ?? this.name,
        gender: gender ?? this.gender,
        dob: dob ?? this.dob,
        email: email ?? this.email,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        countryCode: countryCode ?? this.countryCode,
        password: password ?? this.password,
        deviceToken: deviceToken ?? this.deviceToken);
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'registerModel': this.registerModel,
      'name': this.name,
      'gender': this.gender,
      'dob': this.dob,
      'email': this.email,
      'phoneNumber': this.phoneNumber,
      'countryCode': this.countryCode,
      'password': this.password,
      'deviceToken': this.deviceToken,
    };
  }

  factory RegisterState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new RegisterState(
          isLoading: map['isLoading'] as bool,
          isLoggedin: map['isLoggedin'] as bool,
          registerModel: RegisterModel.fromJson(map['registerModel']),
          phoneNumber: map['phoneNumber'] as String,
          countryCode: map['countryCode'] as String);
    } else
      return new RegisterState.initial();
  }
}
