import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/ui/registration/RegisterViewModel.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:amen_inch/utils/Validation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:gender_selection/gender_selection.dart';
import 'package:intl/intl.dart';

class RegisterScreen extends StatefulWidget {
  bool isForEdit;
  ModelUserProfile userProfile;

  RegisterScreen(
      {Key key, @required this.isForEdit, @required this.userProfile})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return Register();
  }
}

class Register extends State<RegisterScreen> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  //TextEditingController addressController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController conformPasswordController = TextEditingController();
  DateTime selectedDate = DateTime.now();

  Country selectedCountry = CountryPickerUtils.getCountryByIsoCode('AM');

  final TextEditingController controller = TextEditingController();

  FocusNode firstNameNode = new FocusNode();
  FocusNode lastNameNode = new FocusNode();
  FocusNode emailNode = new FocusNode();
  FocusNode countryNode = new FocusNode();
  FocusNode phoneNode = new FocusNode();
  FocusNode addressNode = new FocusNode();
  FocusNode passwordNode = new FocusNode();
  FocusNode conformPasswordNode = new FocusNode();
  final _formKey = GlobalKey<FormState>();

  String selectedGender;

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, RegisterViewModel>(
      converter: (store) => RegisterViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
    );
  }

  buildContent(RegisterViewModel viewModel, BuildContext context) {
    String title = widget.isForEdit ? AppUtils.getLocalizedString("EDIT_PROFILE") : AppUtils.getLocalizedString("REGISTER");

    if (widget.isForEdit) {
      firstNameController.text = widget.userProfile.data.name;
      lastNameController.text = widget.userProfile.data.name;
      emailController.text = widget.userProfile.data.email;
      phoneController.text = widget.userProfile.data.phoneNumber;
      //addressController.text = widget.userProfile.data.name;
    }

    return UIHelper.getScreen(
      scaffold: Scaffold(
        appBar: UIHelper.getAppBar(AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            title, 0, false, context, "", ""),
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(6),
                    child: TextFormField(
                      controller: firstNameController,
                      keyboardType: TextInputType.text,
                      focusNode: firstNameNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, Constants.ENTER_FIRST_NAME);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(lastNameNode);
                      },
                      decoration: InputDecoration(
                        labelText: AppUtils.getLocalizedString("FIRST_NAME"),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(6),
                    child: TextFormField(
                      controller: lastNameController,
                      keyboardType: TextInputType.text,
                      focusNode: lastNameNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, Constants.ENTER_LAST_NAME);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(emailNode);
                      },
                      decoration: InputDecoration(
                        labelText: AppUtils.getLocalizedString("LAST_NAME"),
                      ),
                    ),
                  ),
                  /*Container(
                    padding: EdgeInsets.all(6),
                    child: GenderSelection(
                      size: 50,
                      femaleImage: NetworkImage(
                          "https://cdn1.iconfinder.com/data/icons/website-internet/48/website_-_female_user-512.png"),
                      maleImage: NetworkImage(
                          "https://icon-library.net/images/avatar-icon/avatar-icon-4.jpg"),
                      selectedGenderIconBackgroundColor: Colors.amber,
                      selectedGenderTextStyle: TextStyle(
                          color: Colors.amber,
                          fontSize: 14,
                          fontWeight: FontWeight.bold),
                      onChanged: (Gender gender) {
                        print(gender);
                        if (gender.index == 0) {
                          this.selectedGender = "Male";
                        } else {
                          this.selectedGender = "Female";
                        }
                      },
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      _selectDate(context);
                    },
                    child: Container(
                      padding: EdgeInsets.only(top: 10, left: 5),
                      alignment: Alignment.centerLeft,
                      child: Column(
                        children: <Widget>[
                          Text(
                            "Date of birth",
                            style: TextStyle(
                              fontSize: 12,
                              color:
                                  AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
                            ),
                          ),
                          Container(
                            padding:
                                EdgeInsets.only(top: 10, bottom: 10, left: 5),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Text("${selectedDate.toLocal()}".split(' ')[0]),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 1,
                    child: Container(
                      color: Colors.grey,
                    ),
                  ),*/
                  Container(
                    padding: EdgeInsets.all(6),
                    child: TextFormField(
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      focusNode: emailNode,
                      validator: (t) {
                        return Validation().validateEmail(t);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(countryNode);
                      },
                      decoration: InputDecoration(
                        labelText:AppUtils.getLocalizedString("EMAIL"),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    margin: EdgeInsets.only(bottom: 10),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: CountryPickerDropdown(
                            initialValue: 'am',
                            itemBuilder: _buildDropdownItem,
                            onValuePicked: (Country country) {
                              selectedCountry = country;
                              print("${country.name}");
                            },
                          ),
                        ),
                        Expanded(
                          child: TextFormField(
                            focusNode: phoneNode,
                            controller: phoneController,
                            validator: (t) {
                              return Validation().validatePhoneNumber(t);
                            },
                            decoration: InputDecoration(
                              labelText: AppUtils.getLocalizedString("PHONE_NUMBER"),
                              border: InputBorder.none,
                            ),
                            onFieldSubmitted: (v) {},
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 1,
                    child: Container(
                      color: Colors.grey,
                    ),
                  ),
                  /*Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: countryController,
                      keyboardType: TextInputType.phone,
                      focusNode: countryNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, Constants.ENTER_VALID_COUNTRY_CODE);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(phoneNode);
                      },
                      decoration: InputDecoration(
                        labelText: 'Country Code',
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                      controller: phoneController,
                      keyboardType: TextInputType.phone,
                      focusNode: phoneNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, Constants.ENTER_VALID_PHONE_NUMBER);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(addressNode);
                      },
                      decoration: InputDecoration(
                        labelText: 'Phone Number',
                      ),
                    ),
                  ),*/

                 /* Container(
                    padding: EdgeInsets.all(6),
                    child: TextFormField(
                      controller: addressController,
                      keyboardType: TextInputType.streetAddress,
                      focusNode: addressNode,
                      validator: (t) {
                        return Validation()
                            .validateString(t, "Please enter valid address!");
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context).requestFocus(passwordNode);
                      },
                      decoration: InputDecoration(
                        labelText: 'Address',
                      ),
                    ),
                  ),*/
                  Container(
                    padding: EdgeInsets.all(6),
                    child: TextFormField(
                      controller: passwordController,
                      keyboardType: TextInputType.visiblePassword,
                      focusNode: passwordNode,
                      obscureText: true,
                      validator: (t) {
                        return Validation().validatePassword(t);
                      },
                      onFieldSubmitted: (v) {
                        FocusScope.of(context)
                            .requestFocus(conformPasswordNode);
                      },
                      decoration: InputDecoration(
                        labelText: AppUtils.getLocalizedString("PASS"),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(6),
                    child: TextFormField(
                      controller: conformPasswordController,
                      keyboardType: TextInputType.visiblePassword,
                      focusNode: conformPasswordNode,
                      obscureText: true,
                      validator: (t) {
                        return Validation().validateConfirmPassword(
                            passwordController.text, t);
                      },
                      decoration: InputDecoration(
                        labelText: AppUtils.getLocalizedString("CONFIRM_PASS"),
                      ),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 20),
                      child: SizedBox(
                        height: 50,
                        child: UIHelper.getFlatButton(context,
                            backGroundColor:
                                AppUtils.hexToColor(AppConfig.COLOR_SECONDARY),
                            buttonName: AppUtils.getLocalizedString("REGISTER_CAPS"), doOnPress: () {
                          if (_formKey.currentState.validate()) {
                            print(firstNameController.text);
                            print(lastNameController.text);
                            print(emailController.text);
                            print(phoneController.text);
                           // print(addressController.text);
                            print(passwordController.text);
                            print(conformPasswordController.text);
/*
                            if (selectedDate == null) {
                              _showMessage("Please select the date of birth");
                            } else if (selectedGender == null ||
                                selectedGender.isEmpty) {
                              _showMessage("Please select the gender.");
                            } else {

                            }*/
                            _performRegister(viewModel);
                          }
                        }),
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildDropdownItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}(${country.isoCode})"),
          ],
        ),
      );

  onFailure(String error) {
    _showMessage(error);
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(1980, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  Future<void> _performRegister(RegisterViewModel viewModel) async {
    /*if (selectedCountry == null) {
      _showMessage("Please select country code.");
      return;
    }*/
    String phoneNumber =
        phoneController.text.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
    phoneNumber.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
    final DateFormat formatter = DateFormat('dd-MM-yyyy');
    final String formatted = formatter.format(selectedDate);
    viewModel.register(
        firstNameController.text.trim() + " " + lastNameController.text.trim(),
        "",
        "",
        emailController.text.trim(),
        phoneNumber,
        "+" + selectedCountry.phoneCode,
        passwordController.text.trim(),
        "0",
        onFailure);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
