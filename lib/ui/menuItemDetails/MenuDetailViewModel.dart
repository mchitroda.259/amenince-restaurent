import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:redux/redux.dart';

import 'ModelMenuDetail.dart';

class MenuDetailViewModel {
  bool isLoading;
  ModelMenuDetail modelFoodDetail;
  String cartCount;
  int newCartCount;

  MenuDetailViewModel(
      {this.isLoading,
        this.modelFoodDetail,
      this.cartCount,
      this.newCartCount});

    static MenuDetailViewModel fromStore(Store store) {
    return MenuDetailViewModel(
      isLoading: store.state.menuDetailState.isLoading,
      modelFoodDetail: store.state.menuDetailState.modelFoodDetail,
      cartCount: store.state.menuDetailState.cartCount,
      newCartCount: store.state.menuDetailState.newCartCount,
    );
  }
}
