import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:redux/redux.dart';

import 'MenuDetailState.dart';
import 'ModelMenuDetail.dart';

final menuDetailReducer = combineReducers<MenuDetailState>([
  TypedReducer<MenuDetailState, SetLoadingForMenuDetail>(_setLoading),
  TypedReducer<MenuDetailState, SetMenuDetailData>(_setMenuDetailData),
  TypedReducer<MenuDetailState, SetAddToCartData>(_setAddTOCartData),
  TypedReducer<MenuDetailState, SetCartCount>(_setCartCount),
  TypedReducer<MenuDetailState, UpdateCartCount>(_updateCartCount),


]);

MenuDetailState _setLoading(MenuDetailState state, SetLoadingForMenuDetail action) {
  return state.copyWith(isLoading: action.isLoading);
}

MenuDetailState _setCartCount(MenuDetailState state, SetCartCount action) {
  return state.copyWith(cartCount: action.cartCount);
}

MenuDetailState _updateCartCount(MenuDetailState state, UpdateCartCount action) {
  return state.copyWith(newCartCount: action.updatedCount);
}

MenuDetailState _setMenuDetailData(
    MenuDetailState state, SetMenuDetailData action) {
  return state.copyWith(modelFoodDetail: action.modelFoodDetail);
}

MenuDetailState _setAddTOCartData(
    MenuDetailState state, SetAddToCartData action) {
  return state.copyWith(modelAddToCart: action.modelAddToCart);
}


class SetLoadingForMenuDetail {
  bool isLoading;

  SetLoadingForMenuDetail(this.isLoading);
}


class SetCartCount {
  String cartCount;

  SetCartCount(this.cartCount);
}

class UpdateCartCount {
  int updatedCount;

  UpdateCartCount(this.updatedCount);
}

class SetMenuDetailData {
  ModelMenuDetail modelFoodDetail;

  SetMenuDetailData(this.modelFoodDetail);
}

class SetAddToCartData {
  ModelAddToCart modelAddToCart;

  SetAddToCartData(this.modelAddToCart);
}
