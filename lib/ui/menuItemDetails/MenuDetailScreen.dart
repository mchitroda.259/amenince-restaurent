import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/ui/menuItemDetails/MenuDetailActions.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/widgets/MenuDetailCounterView.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'MenuDetailViewModel.dart';
import 'ModelMenuDetail.dart';

class MenuDetailScreen extends StatefulWidget {
  String restaurantId, restaurantImage, restaurantName;
  Products products;
  bool isFromFav = false;

  MenuDetailScreen(
      {Key key,
      @required this.products,
      @required this.restaurantId,
      @required this.restaurantImage,
      @required this.restaurantName,
      @required this.isFromFav})
      : super(key: key);

  @override
  State createState() {
    return MenuDetail();
  }
}

class MenuDetail extends State<MenuDetailScreen> with WidgetsBindingObserver {
  int _currentIndex = 0;
  TextEditingController codeController = TextEditingController();
  Store _store;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      print("Detail resume");
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, MenuDetailViewModel>(
      converter: (store) => MenuDetailViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
      },
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  buildContent(MenuDetailViewModel viewModel, BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return UIHelper.getScreen(
      scaffold: Scaffold(
        appBar: UIHelper.getAppBar(
            AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            widget.restaurantName,
            16,
            false,
            context,
            viewModel.cartCount,
            widget.restaurantId),
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: SingleChildScrollView(
              child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 200,
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                          alignment: Alignment.center,
                          width: (width / 2) - 10,
                          height: 200,
                          padding: EdgeInsets.all(5.0),
                          child: CachedNetworkImage(
                            imageUrl: widget.products.image,
                            imageBuilder: (context, imageProvider) => Container(
                              width: 200.0,
                              height: 200.0,
                              decoration: BoxDecoration(
                                shape: BoxShape.rectangle,
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                image: DecorationImage(
                                    image: imageProvider, fit: BoxFit.cover),
                              ),
                            ),
                            placeholder: (context, url) =>
                                CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          ),
                        ),
                      ),
                      Container(
                        width: (width / 2) - 10,
                        height: 200,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: [
                                    Container(
                                      alignment: Alignment.center,
                                      width: 40,
                                      height: 30,
                                      padding: EdgeInsets.all(5.0),
                                      child: CachedNetworkImage(
                                        imageUrl: widget.restaurantImage,
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          width: 40.0,
                                          height: 30.0,
                                          decoration: BoxDecoration(
                                            shape: BoxShape.rectangle,
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                        placeholder: (context, url) =>
                                            CircularProgressIndicator(),
                                        errorWidget: (context, url, error) =>
                                            Icon(Icons.error),
                                      ),
                                    ),
                                    Container(
                                      alignment: Alignment.topLeft,
                                      width: (width / 2) - 50,
                                      padding: EdgeInsets.only(left: 5, top: 5),
                                      child: Text(widget.restaurantName,
                                          maxLines: 2,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  ],
                                ),
                                Container(
                                  alignment: Alignment.topLeft,
                                  width: (width / 2) - 10,
                                  padding: EdgeInsets.only(left: 5, top: 5),
                                  child: Text(widget.products.title,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                          fontWeight: FontWeight.normal)),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  alignment: Alignment.topLeft,
                                  padding: EdgeInsets.only(left: 5, top: 5),
                                  child: Text(widget.products.priceTxt,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold)),
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      top: 5, left: 5, bottom: 10),
                                  margin: EdgeInsets.only(
                                    right: 8,
                                  ),
                                  child: MenuDetailCounterView(
                                    counter:
                                        int.parse(widget.products.countInCart),
                                    store: _store,
                                    productId: widget.products.id,
                                    isFromState: false,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.only(left: 12, top: 5),
                child: Text(widget.products.description,
                    maxLines: 4,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                        fontWeight: FontWeight.normal)),
              ),
              Container(
                padding: EdgeInsets.only(left: 10, top: 10),
                margin: EdgeInsets.only(left: 10, right: 10, top: 15),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: new BorderRadius.circular(10.0)),
                child: TextField(
                  keyboardType: TextInputType.multiline,
                  minLines: 5,
                  //Normal textInputField will be displayed
                  maxLines: 5,
                  // when user presses enter it will adapt to it
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.zero,
                    hintStyle: new TextStyle(color: Colors.grey, fontSize: 14),
                    hintText:AppUtils.getLocalizedString("YOUR_COMMENT_TO_ORDER"),
                  ),
                ),
              ),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(top: 20, left: 10, right: 10),
                  child: SizedBox(
                    width: 200,
                    height: 35,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.black)),
                      onPressed: () {
                        if (int.parse(widget.products.countInCart) > 0) {
                          _store.dispatch(updateCartActions(
                              context,
                              widget.products.cartId,
                              "0",
                              viewModel.newCartCount,
                              widget.restaurantId,
                              widget.isFromFav,
                              onFailure));
                        } else {
                          _store.dispatch(addToCartActions(
                              context,
                              widget.products.id,
                              "0",
                              viewModel.newCartCount,
                              widget.restaurantId,
                              widget.isFromFav,
                              onFailure));
                        }
                      },
                      color: Colors.black,
                      textColor: Colors.white,
                      child: Text(AppUtils.getLocalizedString("BTN_ADD_TO_CART").toUpperCase(),
                          style: TextStyle(fontSize: 12)),
                    ),
                  ))
            ],
          )),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
