import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:flutter/material.dart';

import 'ModelMenuDetail.dart';

class MenuDetailState {
  bool isLoading = false;
  ModelMenuDetail modelFoodDetail;
  ModelAddToCart modelAddToCart;
  String cartCount;
  int newCartCount;

  MenuDetailState(
      {@required this.isLoading,
      @required this.modelFoodDetail,
      @required this.modelAddToCart,
      this.cartCount,
      this.newCartCount});

  factory MenuDetailState.initial() {
    return new MenuDetailState(
        isLoading: false,
        modelFoodDetail: null,
        modelAddToCart: null,
        cartCount: "",
        newCartCount: 0);
  }

  MenuDetailState copyWith(
      {bool isLoading,
      ModelMenuDetail modelFoodDetail,
      ModelAddToCart modelAddToCart,
      String cartCount,int newCartCount}) {
    return new MenuDetailState(
      isLoading: isLoading ?? this.isLoading,
      modelFoodDetail: modelFoodDetail ?? this.modelFoodDetail,
      modelAddToCart: modelAddToCart ?? this.modelAddToCart,
      cartCount: cartCount ?? this.cartCount,
      newCartCount: newCartCount ?? this.newCartCount,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'modelFoodDetail': this.modelFoodDetail,
      'modelAddToCart': this.modelAddToCart,
      'cartCount': this.cartCount,
      'newCartCount': this.newCartCount,
    };
  }

  factory MenuDetailState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new MenuDetailState(
        isLoading: map['isLoading'] as bool,
        modelFoodDetail: ModelMenuDetail.fromJson(map['modelFoodDetail']),
        modelAddToCart: ModelAddToCart.fromJson(map['modelAddToCart']),
        cartCount: map['cartCount'] as String,
        newCartCount: map['newCartCount'] as int,
      );
    } else
      return new MenuDetailState.initial();
  }
}
