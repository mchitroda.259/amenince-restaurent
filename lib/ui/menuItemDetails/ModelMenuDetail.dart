class ModelMenuDetail {
  String success;
  String status;
  String message;
  Data data;

  ModelMenuDetail({this.success, this.status, this.message, this.data});

  ModelMenuDetail.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      ModelMenuDetail.initial();
    } else {
      success = json['success'];
      status = json['status'];
      message = json['message'];
      data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  factory ModelMenuDetail.initial() {
    return new ModelMenuDetail(
        status: null, data: null, message: null, success: null);
  }
}

class Data {
  String id;
  String title;
  String image;
  String bannerImage;
  String phoneNumber;
  String email;
  String address;
  String delivery;
  String timing;
  String latitude;
  String longitude;
  String zipCode;
  String cityId;
  String stateId;
  String countryId;
  String totalReviews;
  String totalRatings;
  List<Reviews> reviews;
  List<Menu> menu;

  Data(
      {this.id,
      this.title,
      this.image,
      this.bannerImage,
      this.phoneNumber,
      this.email,
      this.address,
      this.delivery,
      this.timing,
      this.latitude,
      this.longitude,
      this.zipCode,
      this.cityId,
      this.stateId,
      this.countryId,
      this.totalReviews,
      this.totalRatings,
      this.reviews,
      this.menu});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    bannerImage = json['banner_image'];
    phoneNumber = json['phone_number'];
    email = json['email'];
    address = json['address'];
    delivery = json['delivery'];
    timing = json['timing'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    zipCode = json['zip_code'];
    cityId = json['city_id'];
    stateId = json['state_id'];
    countryId = json['country_id'];
    totalReviews = json['total_reviews'];
    totalRatings = json['total_ratings'];
    if (json['reviews'] != null) {
      reviews = new List<Reviews>();
      json['reviews'].forEach((v) {
        reviews.add(new Reviews.fromJson(v));
      });
    }
    if (json['menu'] != null) {
      menu = new List<Menu>();
      json['menu'].forEach((v) {
        menu.add(new Menu.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    data['banner_image'] = this.bannerImage;
    data['phone_number'] = this.phoneNumber;
    data['email'] = this.email;
    data['address'] = this.address;
    data['delivery'] = this.delivery;
    data['timing'] = this.timing;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['zip_code'] = this.zipCode;
    data['city_id'] = this.cityId;
    data['state_id'] = this.stateId;
    data['country_id'] = this.countryId;
    data['total_reviews'] = this.totalReviews;
    data['total_ratings'] = this.totalRatings;
    if (this.reviews != null) {
      data['reviews'] = this.reviews.map((v) => v.toJson()).toList();
    }
    if (this.menu != null) {
      data['menu'] = this.menu.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Reviews {
  String id;
  String name;
  String review;
  String rating;

  Reviews({this.id, this.name, this.review, this.rating});

  Reviews.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    review = json['review'];
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['review'] = this.review;
    data['rating'] = this.rating;
    return data;
  }
}

class Menu {
  String id;
  String title;
  String image;
  List<Products> products;
  bool isSelected = false;

  Menu({this.id, this.title, this.image, this.products});

  Menu.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    if (json['products'] != null) {
      products = new List<Products>();
      json['products'].forEach((v) {
        products.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    if (this.products != null) {
      data['products'] = this.products.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Products {
  String id;
  String title;
  String image;
  String ownerId;
  String cartId;
  String countInCart;
  String quantity;
  String price;
  String priceTxt;
  String description;
  String status;
  String isFav;

  Products(
      {this.id,
      this.title,
      this.image,
      this.ownerId,
      this.cartId,
      this.countInCart,
      this.quantity,
      this.price,
      this.priceTxt,
      this.description,
      this.status,
      this.isFav});

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    ownerId = json['owner_id'];
    cartId = json['cart_id'];
    countInCart = json['count_in_cart'];
    quantity = json['quantity'];
    price = json['price'];
    priceTxt = json['price_txt'];
    description = json['description'];
    status = json['status'];
    isFav = json['is_favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    data['owner_id'] = this.ownerId;
    data['cart_id'] = this.cartId;
    data['count_in_cart'] = this.countInCart;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    data['price_txt'] = this.priceTxt;
    data['description'] = this.description;
    data['status'] = this.status;
    data['is_favorite'] = this.isFav;
    return data;
  }
}
