import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/ui/detail/FoodDetailScreen.dart';
import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:flutter/material.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import 'MenuDetailReducer.dart';
import 'ModelMenuDetail.dart';

ThunkAction getMenuDetailActions(
    String id, String searchKeyword, Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForMenuDetail(true));
    new Future(() async {
      performGetMenuDetail(id, searchKeyword).then((dataModel) async {
        if (dataModel is ModelMenuDetail) {
          if (dataModel.success == "1") {
            PreferencesHelper.cartCount.then(
                (value) => {store.dispatch(SetCartCount(value.toString()))});
            store.dispatch(SetMenuDetailData(dataModel));
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        } else {
          if (dataModel.status == "401") {
            onFailure(dataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        }
        store.dispatch(new SetLoadingForMenuDetail(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForMenuDetail(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performGetMenuDetail(String id, String searchKeyword) async {
  ApiProvider apiProvider = ApiProvider();

  String userId = await PreferencesHelper.userId;
  Map body;

  body = {
    "restaurant_id": id,
    "user_id": userId,
    "search": searchKeyword == null ? '' : searchKeyword,
  };

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.GET_FOOD_DETAILS,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelMenuDetail.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}

ThunkAction addToCartActions(BuildContext context, String id, String clearCart,
    int qty, String resId, bool isFromFav, Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForMenuDetail(true));
    new Future(() async {
      performAddToCart(id, clearCart, qty).then((dataModel) async {
        if (dataModel.success == "1") {
          store.dispatch(SetAddToCartData(dataModel));
          NavigationHelper.navKey.currentState.pop(true);
          if (!isFromFav) {

          }
        } else {
          onFailure(dataModel.message.toString());
          print(dataModel.message.toString());
        }
        store.dispatch(new SetLoadingForMenuDetail(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForMenuDetail(false));
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<dynamic> performAddToCart(String id, String clearCart, int qty) async {
  ApiProvider apiProvider = ApiProvider();
  String userId = await PreferencesHelper.userId;

  Map body;

  body = {
    "product_id": id,
    "user_id": userId,
    "clear_cart": clearCart,
    "quantity": qty.toString()
  };

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.ADD_CART,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelAddToCart.fromJson(response);
  else
    /*if (responseModel.status == "201")
    return updateCart(id, clearCart, qty,);
  else*/
    return ResponseModel.fromJson(response);
}

ThunkAction updateCartActions(BuildContext context, String id, String clearCart,
    int qty, String resId, bool isFromFav, Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForMenuDetail(true));
    new Future(() async {
      updateCart(id, clearCart, qty).then((dataModel) async {
        if (dataModel.success == "1") {
          store.dispatch(SetAddToCartData(dataModel));
          NavigationHelper.navKey.currentState.pop(true);
          if (!isFromFav) {

          }
        } else {
          onFailure(dataModel.message.toString());
          print(dataModel.message.toString());
        }
        store.dispatch(new SetLoadingForMenuDetail(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForMenuDetail(false));
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<dynamic> updateCart(String id, String clearCart, int qty) async {
  ApiProvider apiProvider = ApiProvider();

  String userId = await PreferencesHelper.userId;

  Map body;

  body = {
    "cart_id": id,
    "clear_cart": clearCart,
    "quantity": qty.toString(),
    "user_id": userId
  };

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.UPDATE_MENU,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelAddToCart.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}
