import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/widgets/BarChartSample3.dart';
import 'package:amen_inch/widgets/DashboardCountWidget.dart';
import 'package:amen_inch/widgets/InitialHintAnimation.dart';
import 'package:amen_inch/widgets/LineChartSample.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:redux/redux.dart';

import 'HomeActions.dart';
import 'HomeViewModel.dart';

class HomeScreen extends StatefulWidget {
  @override
  State createState() {
    return Home();
  }
}

class Home extends State<HomeScreen> with SingleTickerProviderStateMixin {
  Store _store;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, HomeViewModel>(
      converter: (store) => HomeViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
        store.dispatch(
          dashboardAction(onFailure),
        ),
      },
    );
  }

  buildContent(HomeViewModel viewModel, BuildContext context) {
    TextEditingController codeController = TextEditingController();

    final children = <Widget>[];
    final tabs = <Widget>[];
    var size = MediaQuery.of(context).size;
    final double itemWidth = size.width / 2;

    int totalOrders = 0;
    int openOrders = 0;
    int completedOrders = 0;
    int failedOrders = 0;

    String revenue = "00";

    if (viewModel.dashboardDataModel != null) {
      if (viewModel.dashboardDataModel.data != null) {
        totalOrders = viewModel.dashboardDataModel.data.totalOrders;
        openOrders = viewModel.dashboardDataModel.data.openOrders;
        completedOrders =viewModel.dashboardDataModel.data.completeOrders;
        failedOrders = viewModel.dashboardDataModel.data.failedOrders;
        revenue = viewModel.dashboardDataModel.data.revenue;
      }
    }

    children.add(new Column(
      children: [
        new Row(
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, top: 8, bottom: 4, right: 4),
              width: itemWidth,
              height: 100,
              child: new DashboardCountWidget(
                  title: AppUtils.getLocalizedString("TOTAL_ORDERS"),
                  count: totalOrders.toString(),
                  color: Colors.orange),
            ),
            Container(
              padding: EdgeInsets.only(left: 4, top: 8, bottom: 4, right: 8),
              width: itemWidth,
              height: 100,
              child: new DashboardCountWidget(
                  title: AppUtils.getLocalizedString("OPEN_ORDERS"),
                  count: openOrders.toString(),
                  color: Colors.deepPurpleAccent),
            )
          ],
        ),
        new Row(
          children: [
            Container(
              padding: EdgeInsets.only(left: 8, top: 4, bottom: 8, right: 4),
              width: itemWidth,
              height: 100,
              child: new DashboardCountWidget(
                  title: AppUtils.getLocalizedString("COMPLETED_ORDERS"),
                  count: completedOrders.toString(),
                  color: Colors.green),
            ),
            Container(
              padding: EdgeInsets.only(left: 4, top: 4, bottom: 8, right: 8),
              width: itemWidth,
              height: 100,
              child: new DashboardCountWidget(
                  title: AppUtils.getLocalizedString("FAILED_ORDERS"),
                  count: failedOrders.toString(),
                  color: Colors.red),
            )
          ],
        )
      ],
    ));

    if (viewModel.dashboardDataModel != null) {
      if (viewModel.dashboardDataModel.data != null) {
        children.add(new InitialHintAnimation(viewModel.dashboardDataModel));

        children.add(
          new Card(
            color: Colors.white,
            elevation: 5,
            margin: EdgeInsets.all(8),
            shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.all(
                Radius.circular(5.0),
              ),
            ),
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(left: 10, right: 30, top: 10, bottom: 10),
              child: Column(
                children: <Widget>[
                  Text(
                    "Income (Revenue)",
                    style: TextStyle(
                        fontSize: 16,
                        color: Color(0xFF3a3a3b),
                        fontWeight: FontWeight.w400),
                    textAlign: TextAlign.left,
                  ),
                  new Padding(
                    padding: EdgeInsets.all(12),
                    child: new CircularPercentIndicator(
                      radius: 110.0,
                      lineWidth: 10.0,
                      percent: 1.0,
                      center: new Text(revenue),
                      progressColor: Colors.orange,
                    ),
                  )
                ],
              ),
            ),
          ),
        );

      }
    }



    return UIHelper.getScreen(
      scaffold: Scaffold(
          body: LoadingDialog(
        isLoading: viewModel.isLoading,
        child: SingleChildScrollView(
          child: Column(
            children: children,
          ),
        ),
      )),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
