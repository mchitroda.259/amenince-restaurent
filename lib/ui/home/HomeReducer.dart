import 'package:amen_inch/ui/home/Categories.dart';
import 'package:amen_inch/ui/home/HomeModel.dart';
import 'package:redux/redux.dart';

import 'HomeState.dart';

final homeReducer = combineReducers<HomeState>([
  TypedReducer<HomeState, SetLoadingForHome>(_setLoading),
  TypedReducer<HomeState, SetDashboardData>(_setDashboardData),
  TypedReducer<HomeState, SetCartCount>(_setCartCount),
  TypedReducer<HomeState, SetCategoryData>(_setCategoryData),

]);

HomeState _setLoading(HomeState state, SetLoadingForHome action) {
  return state.copyWith(isLoading: action.isLoading);
}

HomeState _setCartCount(HomeState state, SetCartCount action) {
  return state.copyWith(cartCount: action.cartCount);
}

class SetCartCount {
  String cartCount;

  SetCartCount(this.cartCount);
}

HomeState _setDashboardData(HomeState state, SetDashboardData action) {
  return state.copyWith(dashboardDataModel: action.dashboardDataModel);
}

HomeState _setCategoryData(HomeState state, SetCategoryData action) {
  return state.copyWith(categories: action.categories);
}


class SetLoadingForHome {
  bool isLoading;

  SetLoadingForHome(this.isLoading);
}

class SetDashboardData {
  HomeModel dashboardDataModel;

  SetDashboardData(this.dashboardDataModel);
}


class SetCategoryData {
  Categories categories;

  SetCategoryData(this.categories);
}
