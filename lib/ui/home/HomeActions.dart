import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/ui/home/HomeModel.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import 'HomeReducer.dart';

String selectedCatId = "";

ThunkAction dashboardAction(Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForHome(true));
    new Future(() async {
      performLoadDashboardData().then((dashboardDataModel) async {
        if (dashboardDataModel is HomeModel) {
          if (dashboardDataModel.success == "1") {
            store.dispatch(SetDashboardData(dashboardDataModel));
          } else {
            if (onFailure != null)
              onFailure(dashboardDataModel.message.toString());
            print(dashboardDataModel.message.toString());
          }
        } else {
          if (dashboardDataModel.status == "401") {
            if (onFailure != null)
              onFailure(dashboardDataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            if (onFailure != null)
              onFailure(dashboardDataModel.message.toString());
            print(dashboardDataModel.message.toString());
          }
        }
        store.dispatch(new SetLoadingForHome(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForHome(false));
        if (error.errorCode.toString() == "401") {
          if (onFailure != null)
            onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          if (onFailure != null) onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performLoadDashboardData() async {
  ApiProvider apiProvider = ApiProvider();

  final response = await apiProvider.doGet(
    APIEndpoints.DASHBOARD,
    APIEndpoints.BASE_URL,
  );

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return HomeModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}
