import 'package:amen_inch/ui/home/Categories.dart';
import 'package:amen_inch/ui/home/HomeModel.dart';
import 'package:flutter/material.dart';

class HomeState {
  bool isLoading = false;
  HomeModel dashboardDataModel;
  Categories categories;

  String cartCount;

  HomeState(
      {@required this.isLoading, this.dashboardDataModel, this.cartCount,this.categories});

  factory HomeState.initial() {
    return new HomeState(
        isLoading: false, dashboardDataModel: null, cartCount: "",categories: null);
  }

  HomeState copyWith(
      {bool isLoading, HomeModel dashboardDataModel, String cartCount,Categories categories}) {
    return new HomeState(
        isLoading: isLoading ?? this.isLoading,
        dashboardDataModel: dashboardDataModel ?? this.dashboardDataModel,
        cartCount: cartCount ?? this.cartCount,
        categories: categories ?? this.categories);
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'home_model': this.dashboardDataModel,
      'cartCount': this.cartCount,
      'categories': this.categories,
    };
  }

  factory HomeState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new HomeState(
        isLoading: map['isLoading'] as bool,
        dashboardDataModel: HomeModel.fromJson(map['home_model']) != null
            ? HomeModel.fromJson(map['home_model'])
            : null,
        cartCount: map['cartCount'] as String,
        categories: Categories.fromJson(map['categories'])  != null
            ? Categories.fromJson(map['categories'])
            : null,
      );
    } else
      return new HomeState.initial();
  }
}
