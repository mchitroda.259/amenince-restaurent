class HomeModel {
  String success;
  String status;
  String message;
  Data data;

  HomeModel({this.success, this.status, this.message, this.data});

  HomeModel.fromJson(Map<String, dynamic> json) {

    if(json != null){
      success = json['success'];
      status = json['status'];
      message = json['message'];
      data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    }else{
      success = null;
      status = null;
      message =null;
      data = null;
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int totalOrders;
  int openOrders;
  int completeOrders;
  int failedOrders;
  List<VisitorsAnalytics> visitorsAnalytics;
  String revenue;

  Data(
      {this.totalOrders,
        this.openOrders,
        this.completeOrders,
        this.failedOrders,
        this.visitorsAnalytics,
        this.revenue});

  Data.fromJson(Map<String, dynamic> json) {
    totalOrders = json['total_orders'];
    openOrders = json['open_orders'];
    completeOrders = json['complete_orders'];
    failedOrders = json['failed_orders'];
    if (json['visitors_analytics'] != null) {
      visitorsAnalytics = new List<VisitorsAnalytics>();
      json['visitors_analytics'].forEach((v) {
        visitorsAnalytics.add(new VisitorsAnalytics.fromJson(v));
      });
    }
    revenue = json['revenue'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_orders'] = this.totalOrders;
    data['open_orders'] = this.openOrders;
    data['complete_orders'] = this.completeOrders;
    data['failed_orders'] = this.failedOrders;
    if (this.visitorsAnalytics != null) {
      data['visitors_analytics'] =
          this.visitorsAnalytics.map((v) => v.toJson()).toList();
    }
    data['revenue'] = this.revenue;
    return data;
  }
}

class VisitorsAnalytics {
  String day;
  int visitors;

  VisitorsAnalytics({this.day, this.visitors});

  VisitorsAnalytics.fromJson(Map<String, dynamic> json) {
    day = json['day'];
    visitors = json['visitors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['day'] = this.day;
    data['visitors'] = this.visitors;
    return data;
  }
}
