import 'package:amen_inch/ui/home/Categories.dart';
import 'package:amen_inch/ui/home/HomeModel.dart';
import 'package:redux/redux.dart';

class HomeViewModel {
  bool isLoading;
  HomeModel dashboardDataModel;
  Categories categories;
  String cartCount;

  HomeViewModel(
      {this.isLoading,
      this.dashboardDataModel,
        this.categories,
        this.cartCount});

  static HomeViewModel fromStore(Store store) {
    return HomeViewModel(
      isLoading: store.state.homeState.isLoading,
      dashboardDataModel: store.state.homeState.dashboardDataModel,
      categories: store.state.homeState.categories,
      cartCount: store.state.homeState.cartCount,
    );
  }
}
