class Categories {
  String success;
  String status;
  String message;
  List<Data> data;

  Categories({this.success, this.status, this.message, this.data});

  Categories.fromJson(Map<String, dynamic> json) {

    if(json != null){
      success = json['success'];
      status = json['status'];
      message = json['message'];
      if (json['data'] != null) {
        data = new List<Data>();
        json['data'].forEach((v) {
          data.add(new Data.fromJson(v));
        });
    }else{
      success = null;
      status = null;
      message =null;
      data = null;
    }

    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String id;
  String title;
  String image;
  bool isSelected = false;

  Data({this.id, this.title, this.image});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    return data;
  }
}
