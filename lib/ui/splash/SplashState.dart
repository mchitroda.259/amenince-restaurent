
class SplashState {
  SplashState();

  factory SplashState.initial() {
    return new SplashState();
  }

  SplashState copyWith() {
    return new SplashState();
  }

  Map<String, dynamic> toJson() {
    return {};
  }

  factory SplashState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new SplashState();
    } else
      return new SplashState.initial();
  }
}
