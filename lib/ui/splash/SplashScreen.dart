import 'dart:async';

import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'SplashViewModel.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Splash();
  }
}

class Splash extends State<SplashScreen> {
  bool isFromNotification = false;

  @override
  Widget build(BuildContext context) {
    isFromNotification = ModalRoute.of(context).settings.arguments as bool;

    return new StoreConnector<AppState, SplashViewModel>(
      converter: (store) => SplashViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel),
      onInit: (store) {
        PreferencesHelper.userId.then((value) => {
              Constants.userId = value,
            });

        PreferencesHelper.languageCode.then((value) => {
              Constants.selectedLanCode = value,
            });
        PreferencesHelper.fcmToken.then((value) => {
          Constants.NOTIFICATION_TOKEN = value,
        });
        startTime();
      },
    );
  }

  buildContent(SplashViewModel viewModel) {
    return UIHelper.getScreen(
      scaffold: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/splash_screen.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationToLoginPage);
  }

  void navigationToLoginPage() {
    PreferencesHelper.introVisited.then((value) => {
          if (value)
            {
              PreferencesHelper.authenticated.then((value) => {
                    if (value)
                      {
                        if (isFromNotification != null)
                          {
                            if (isFromNotification)
                              {
                                Constants.isFromNotification = true,
                                NavigationHelper()
                                    .pushScreenOnTop(Routes.MAIN_SCREEN)
                              }
                            else
                              {
                                Constants.isFromNotification = false,
                                NavigationHelper()
                                    .pushScreenOnTop(Routes.MAIN_SCREEN),
                              }
                          }
                        else
                          {
                            NavigationHelper()
                                .pushScreenOnTop(Routes.MAIN_SCREEN)
                          }
                      }
                    else
                      NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN)
                  })
            }
          else
            {NavigationHelper().pushScreenOnTop(Routes.INTRO)}
        });
  }
}
