import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import 'OrderListModel.dart';
import 'OrderListReducer.dart';

ThunkAction getOrderListActions(Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForOrderList(true));
    new Future(() async {
      performGetOrderList().then((dataModel) async {
        store.dispatch(new SetLoadingForOrderList(false));
        if (dataModel is OrderListModel) {
          if (dataModel.success == "1") {
            store.dispatch(SetOrderListData(dataModel));
          } else {
            dataModel.data = null;
            store.dispatch(SetOrderListData(dataModel));
            onFailure("No result found!");
            print(dataModel.message.toString());
          }
        } else {
          if (dataModel.status == "401") {
            onFailure(dataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForOrderList(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performGetOrderList() async {
  ApiProvider apiProvider = ApiProvider();

  Map body;

  body = {};

  final response = await apiProvider.doGet(
    APIEndpoints.GET_ORDER_LIST,
    APIEndpoints.BASE_URL,
  );

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return OrderListModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}

ThunkAction getCompletedOrderListActions(Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForOrderList(true));
    new Future(() async {
      performGetCompletedOrderList().then((dataModel) async {
        store.dispatch(new SetLoadingForOrderList(false));
        if (dataModel is OrderListModel) {
          if (dataModel.success == "1") {
            store.dispatch(SetCompletedOrderListData(dataModel));
          } else {
            dataModel.data = null;
            store.dispatch(SetCompletedOrderListData(dataModel));
            onFailure("No result found!");
            print(dataModel.message.toString());
          }
        } else {
          if (dataModel.status == "401") {
            onFailure(dataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForOrderList(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performGetCompletedOrderList() async {
  ApiProvider apiProvider = ApiProvider();

  Map body;

  body = {};

  final response = await apiProvider.doGet(
    APIEndpoints.GET_COMPLETED_ORDER_LIST,
    APIEndpoints.BASE_URL,
  );

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return OrderListModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}


ThunkAction updateOrder(
String orderId, String status, String time,Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForOrderList(true));
    new Future(() async {
      performUpdateOrder(orderId, status, time).then((dataModel) async {
        store.dispatch(new SetLoadingForOrderList(false));
        if (dataModel is ResponseModel) {
          if (dataModel.success == "1") {
            store.dispatch(getOrderListActions(onFailure));
          } else {
            onFailure("Failed to update order! Please try again");
            print(dataModel.message.toString());
          }
        } else {
          if (dataModel.status == "401") {
            onFailure(dataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForOrderList(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performUpdateOrder(
    String orderId, String status, String time) async {
  ApiProvider apiProvider = ApiProvider();
  String userId = await PreferencesHelper.userId;

  Map body;

  body = {"order_id": orderId, "status": status, "time": time};

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.UPDATE_ORDER,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ResponseModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}
