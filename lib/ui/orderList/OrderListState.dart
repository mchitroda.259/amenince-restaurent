import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:flutter/material.dart';

import 'OrderListModel.dart';

class OrderListState {
  bool isLoading = false;
  OrderListModel orderListModel;
  OrderListModel completedOrderListModel;

  OrderListState(
      {@required this.isLoading,
      @required this.orderListModel,
      @required this.completedOrderListModel});

  factory OrderListState.initial() {
    return new OrderListState(
        isLoading: false, orderListModel: null, completedOrderListModel: null);
  }

  OrderListState copyWith({bool isLoading, OrderListModel orderListModel,OrderListModel completedOrderListModel}) {
    return new OrderListState(
      isLoading: isLoading ?? this.isLoading,
      orderListModel: orderListModel ?? this.orderListModel,
      completedOrderListModel: completedOrderListModel ?? this.completedOrderListModel
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'orderListModel': this.orderListModel,
      'completedOrderListModel':this.completedOrderListModel
    };
  }

  factory OrderListState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new OrderListState(
        isLoading: map['isLoading'] as bool,
        orderListModel: OrderListModel.fromJson(map['orderListModel']),
        completedOrderListModel: OrderListModel.fromJson(map['completedOrderListModel']),
      );
    } else
      return new OrderListState.initial();
  }
}
