import 'package:redux/redux.dart';

import 'OrderListModel.dart';

class OrderListViewModel {
  bool isLoading;

  OrderListModel orderListModel;
  OrderListModel completedOrderListModel;

  OrderListViewModel({this.isLoading,
    this.orderListModel,this.completedOrderListModel});

  static OrderListViewModel fromStore(Store store) {
    return OrderListViewModel(
      isLoading: store.state.orderListState.isLoading,
      orderListModel: store.state.orderListState.orderListModel,
      completedOrderListModel: store.state.orderListState.completedOrderListModel
    );
  }
}
