class OrderListModel {
  String success;
  String status;
  String message;
  List<Data> data;

  OrderListModel({this.success, this.status, this.message, this.data});

  OrderListModel.fromJson(Map<String, dynamic> json) {
    if(json == null){
      OrderListModel.initial();
    }else{
      success = json['success'];
      status = json['status'];
      message = json['message'];
      if (json['data'] != null) {
        data = new List<Data>();
        json['data'].forEach((v) {
          data.add(new Data.fromJson(v));
        });
      }
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
  factory OrderListModel.initial() {
    return new OrderListModel(
        status: null, data: null, message: null, success: null);
  }
}

class Data {
  String id;
  String date;
  String total;
  String grandTotal;
  String status;
  String isRated;
  String pickupTime;
  Review review;
  List<Items> items;

  Data(
      {this.id,
        this.date,
        this.total,
        this.grandTotal,
        this.status,
        this.isRated,
        this.pickupTime,
        this.review,
        this.items});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    date = json['date'];
    total = json['total'];
    grandTotal = json['grand_total'];
    status = json['status'];
    isRated = json['is_rated'];
    pickupTime = json['pickup_time'];
    review =
    json['review'] != null ? new Review.fromJson(json['review']) : null;
    if (json['items'] != null) {
      items = new List<Items>();
      json['items'].forEach((v) {
        items.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['date'] = this.date;
    data['total'] = this.total;
    data['grand_total'] = this.grandTotal;
    data['status'] = this.status;
    data['is_rated'] = this.isRated;
    data['pickup_time'] = this.pickupTime;
    if (this.review != null) {
      data['review'] = this.review.toJson();
    }
    if (this.items != null) {
      data['items'] = this.items.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Review {
  String id;
  String name;
  String rating;
  String review;
  String date;

  Review({this.id, this.name, this.rating, this.review, this.date});

  Review.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    rating = json['rating'];
    review = json['review'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['rating'] = this.rating;
    data['review'] = this.review;
    data['date'] = this.date;
    return data;
  }
}

class Items {
  String id;
  String title;
  String amount;

  Items({this.id, this.title, this.amount});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['amount'] = this.amount;
    return data;
  }
}
