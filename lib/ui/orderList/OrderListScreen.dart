import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/widgets/CompletedOrderListWidget.dart';
import 'package:amen_inch/widgets/OrderListWidget.dart';
import 'package:amen_inch/widgets/MenuListWidget.dart';
import 'package:amen_inch/widgets/SearchWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/src/store.dart';

import 'OrderListActions.dart';
import 'OrderListViewModel.dart';

class OrderListScreen extends StatefulWidget {
  @override
  State createState() {
    return OrderList();
  }
}

class OrderList extends State<OrderListScreen>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  Store<AppState> _store;

  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
    super.initState();

    _tabController.addListener(() {
      if(_tabController.index == 0)
        _store.dispatch(getOrderListActions(onFailure));
      else
        _store.dispatch(getCompletedOrderListActions(onFailure));
      print("Selected Index: " + _tabController.index.toString());
    });

  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, OrderListViewModel>(
      converter: (store) => OrderListViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
        store.state.orderListState.orderListModel = null,
        store.dispatch(getOrderListActions(onFailure))
      },
    );
  }

  buildContent(OrderListViewModel viewModel, BuildContext context) {
    final subView = <Widget>[];

    return UIHelper.getScreen(
      scaffold: Scaffold(
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                // give the tab bar a height [can change hheight to preferred height]
                Container(
                  height: 45,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                        color: AppUtils.hexToColor(AppConfig.COLOR_PRIMARY)),
                  ),
                  child: TabBar(
                    controller: _tabController,
                    // give the indicator a decoration (color and border radius)
                    indicator: BoxDecoration(
                      color: AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
                    ),
                    labelColor: Colors.white,
                    unselectedLabelColor: Colors.black,
                    tabs: [
                      // first tab [you can add an icon using the icon property]
                      Tab(
                        text: AppUtils.getLocalizedString("OPEN_ORDERS"),
                      ),

                      // second tab [you can add an icon using the icon property]
                      Tab(
                        text: AppUtils.getLocalizedString("COMPLETED_ORDERS"),
                      ),
                    ],
                  ),
                ),
                // tab bar view here
                Expanded(
                  child: TabBarView(
                    controller: _tabController,
                    children: [
                      // first tab bar view widget
                      new OrderListItems(
                          orderListModel: viewModel.orderListModel,store: _store,),
                      new CompletedOrderListItems(
                          orderListModel: viewModel.completedOrderListModel),
                      // second tab bar view widget
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
