import 'package:redux/redux.dart';

import 'OrderListModel.dart';
import 'OrderListState.dart';

final orderListReducer = combineReducers<OrderListState>([
  TypedReducer<OrderListState, SetLoadingForOrderList>(_setLoading),
  TypedReducer<OrderListState, SetOrderListData>(_setOrderListData),
  TypedReducer<OrderListState, SetCompletedOrderListData>(_setCompletedOrderListData),
]);

OrderListState _setLoading(OrderListState state, SetLoadingForOrderList action) {
  return state.copyWith(isLoading: action.isLoading);
}

OrderListState _setOrderListData(
    OrderListState state, SetOrderListData action) {
  return state.copyWith(orderListModel: action.orderListModel);
}

OrderListState _setCompletedOrderListData(
    OrderListState state, SetCompletedOrderListData action) {
  return state.copyWith(completedOrderListModel: action.orderListModel);
}

class SetLoadingForOrderList {
  bool isLoading;
  SetLoadingForOrderList(this.isLoading);
}


class SetOrderListData {
  OrderListModel orderListModel;
  SetOrderListData(this.orderListModel);
}

class SetCompletedOrderListData {
  OrderListModel orderListModel;
  SetCompletedOrderListData(this.orderListModel);
}

