import 'dart:io';

import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/ui/detail/FoodDetailActions.dart';
import 'package:amen_inch/ui/menuList/MenuListActionsModel.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/Validation.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:image_picker/image_picker.dart';
import 'package:redux/redux.dart';

import 'FoodDetailViewModel.dart';

class FoodDetailScreen extends StatefulWidget {
  MenuData menuData;

  FoodDetailScreen({Key key, @required this.menuData}) : super(key: key);

  @override
  State createState() {
    return FoodDetail();
  }
}

class FoodDetail extends State<FoodDetailScreen> with WidgetsBindingObserver {
  TextEditingController titleController = TextEditingController();
  TextEditingController decController = TextEditingController();
  TextEditingController priceController = TextEditingController();

  FocusNode titleNode = new FocusNode();
  FocusNode decNode = new FocusNode();
  FocusNode priceNode = new FocusNode();

  final _formKey = GlobalKey<FormState>();

  String selectedImage = "";

  PickedFile _imageFile;
  bool isFromSelection = false;

  final ImagePicker _picker = ImagePicker();

  Store _store;
  final children = <Widget>[];
  bool isInit = false;
  bool status = true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    isInit = true;
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      print("Detail resume");
    }
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
        maxWidth: 512,
        maxHeight: 512,
        imageQuality: 100,
      );

      setState(() {
        _imageFile = pickedFile;
        if (_imageFile != null)
          isFromSelection = true;
        else
          isFromSelection = false;
      });
    } catch (e) {
      setState(() {
        isFromSelection = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, FoodDetailViewModel>(
      converter: (store) => FoodDetailViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
        titleController.text = widget.menuData.title,
        decController.text = widget.menuData.description,
        priceController.text = widget.menuData.price,
        selectedImage = widget.menuData.image,
        status = widget.menuData.status == 'active',
      },
    );
  }

  buildContent(FoodDetailViewModel viewModel, BuildContext context) {

    return UIHelper.getScreen(
      scaffold: Scaffold(
        appBar: UIHelper.getAppBar(
            AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            AppUtils.getLocalizedString("DETAILS"),
            16,
            false,
            context,
            "0",
            ""),
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: SingleChildScrollView(
              child: LoadingDialog(
            isLoading: viewModel.isLoading,
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                       /* _onImageButtonPressed(ImageSource.gallery,
                            context: context);*/
                      },
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 20),
                            width: 150,
                            height: 150,
                            padding: EdgeInsets.all(5.0),
                            child: !isFromSelection
                                ? CachedNetworkImage(
                                    imageUrl: selectedImage,
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                      width: 150.0,
                                      height: 150.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                    placeholder: (context, url) =>
                                        CircularProgressIndicator(),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  )
                                : Container(
                                    width: 150,
                                    height: 150,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image:
                                              FileImage(File(_imageFile.path)),
                                          fit: BoxFit.fill),
                                    ),
                                  )),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: titleController,
                        keyboardType: TextInputType.text,
                        focusNode: titleNode,
                        validator: (t) {
                          return Validation()
                              .validateString(t, Constants.ENTER_TITLE);
                        },
                        onFieldSubmitted: (v) {
                          FocusScope.of(context).requestFocus(decNode);
                        },
                        decoration: InputDecoration(
                          labelText: AppUtils.getLocalizedString("TITLE"),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 1,
                      child: Container(
                        margin: EdgeInsets.all(10),
                        color: Colors.grey,
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: decController,
                        keyboardType: TextInputType.text,
                        focusNode: decNode,
                        maxLines: 4,
                        minLines: 4,
                        validator: (t) {
                          return Validation()
                              .validateString(t, Constants.ENTER_DEC);
                        },
                        onFieldSubmitted: (v) {
                          FocusScope.of(context).requestFocus(priceNode);
                        },
                        decoration: InputDecoration(
                          labelText: AppUtils.getLocalizedString("DESCRIPTION"),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: priceController,
                        keyboardType: TextInputType.number,
                        focusNode: priceNode,
                        validator: (t) {
                          return Validation()
                              .validateString(t, Constants.ENTER_PRICE);
                        },
                        onFieldSubmitted: (v) {},
                        decoration: InputDecoration(
                          labelText: AppUtils.getLocalizedString("PRICE"),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.centerRight,
                          child: Text(AppUtils.getLocalizedString("STATUS"),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400)),
                        ),
                        FlutterSwitch(
                          width: 60.0,
                          height: 30.0,
                          valueFontSize: 10.0,
                          toggleSize: 25.0,
                          value: status,
                          borderRadius: 30.0,
                          padding: 4.0,
                          activeToggleColor: Colors.white,
                          activeColor:  Colors.green,
                          inactiveToggleColor: Color(0xFF2F363D),
                          onToggle: (val) {
                            setState(() {
                              status = val;
                            });
                          },
                        ),
                      ],
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 10, right: 10, top: 20),
                        child: SizedBox(
                          height: 50,
                          child: UIHelper.getFlatButton(context,
                              backGroundColor: AppUtils.hexToColor(
                                  AppConfig.COLOR_SECONDARY),
                              buttonName: AppUtils.getLocalizedString("UPDATE"),
                              doOnPress: () {
                            if (_formKey.currentState.validate()) {
                              print(titleController.text);
                              print(decController.text);
                              print(priceController.text);
                              String statusValue = status ? 'active' : 'inactive';
                              _store.dispatch(
                                updateMenuActions(widget.menuData.id, titleController.text, decController.text, priceController.text, statusValue, onFailure),
                              );
                            }
                          }),
                        )),
                  ],
                ),
              ),
            ),
          )),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
