import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import 'FoodDetailReducer.dart';
import 'ModelFoodDetail.dart';

Map detailsBody;

ThunkAction updateMenuActions(
String id, String title, String description,String price,String status, Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForFoodDetail(true));
    new Future(() async {
      updateMenu(id, title,description,price,status).then((dataModel) async {
        if (dataModel.success == "1") {
          Constants.isFromMenuUpdate = true;
          NavigationHelper().pushScreenOnTop(Routes.MAIN_SCREEN);
        } else {
          onFailure(dataModel.message.toString());
          print(dataModel.message.toString());
        }
        store.dispatch(new SetLoadingForFoodDetail(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForFoodDetail(false));
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<dynamic> updateMenu(String id, String title, String description,String price,String status) async {
  ApiProvider apiProvider = ApiProvider();

  String userId = await PreferencesHelper.userId;

  Map body;

  body = {"product_id": id, "title": title, "description": description,"price" : price,"status" : status};

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.UPDATE_MENU,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelFoodDetail.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}

