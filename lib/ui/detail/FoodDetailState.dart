import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:flutter/material.dart';

import 'ModelFoodDetail.dart';

class FoodDetailState {
  bool isLoading = false;
  ModelFoodDetail modelFoodDetail;
  ModelAddToCart modelAddToCart;
  String cartCount;

  FoodDetailState(
      {@required this.isLoading,
      @required this.modelFoodDetail,
      @required this.modelAddToCart,
      this.cartCount});

  factory FoodDetailState.initial() {
    return new FoodDetailState(
        isLoading: false,
        modelFoodDetail: null,
        modelAddToCart: null,
        cartCount: "");
  }

  FoodDetailState copyWith(
      {bool isLoading,
      ModelFoodDetail modelFoodDetail,
      ModelAddToCart modelAddToCart,
      String cartCount}) {
    return new FoodDetailState(
      isLoading: isLoading ?? this.isLoading,
      modelFoodDetail: modelFoodDetail ?? this.modelFoodDetail,
      modelAddToCart: modelAddToCart ?? this.modelAddToCart,
      cartCount: cartCount ?? this.cartCount,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'modelFoodDetail': this.modelFoodDetail,
      'modelAddToCart': this.modelAddToCart,
      'cartCount': this.cartCount,
    };
  }

  factory FoodDetailState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new FoodDetailState(
        isLoading: map['isLoading'] as bool,
        modelFoodDetail: ModelFoodDetail.fromJson(map['modelFoodDetail']),
        modelAddToCart: ModelAddToCart.fromJson(map['modelAddToCart']),
        cartCount: map['cartCount'] as String,
      );
    } else
      return new FoodDetailState.initial();
  }
}
