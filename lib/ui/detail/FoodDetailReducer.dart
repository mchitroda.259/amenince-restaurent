import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:redux/redux.dart';

import 'FoodDetailState.dart';
import 'ModelFoodDetail.dart';

final foodDetailReducer = combineReducers<FoodDetailState>([
  TypedReducer<FoodDetailState, SetLoadingForFoodDetail>(_setLoading),
  TypedReducer<FoodDetailState, SetFoodDetailData>(_setFoodDetailData),
  TypedReducer<FoodDetailState, SetAddToCartData>(_setAddTOCartData),
  TypedReducer<FoodDetailState, SetCartCount>(_setCartCount),

]);

FoodDetailState _setLoading(FoodDetailState state, SetLoadingForFoodDetail action) {
  return state.copyWith(isLoading: action.isLoading);
}

FoodDetailState _setCartCount(FoodDetailState state, SetCartCount action) {
  return state.copyWith(cartCount: action.cartCount);
}


FoodDetailState _setFoodDetailData(
    FoodDetailState state, SetFoodDetailData action) {
  return state.copyWith(modelFoodDetail: action.modelFoodDetail);
}

FoodDetailState _setAddTOCartData(
    FoodDetailState state, SetAddToCartData action) {
  return state.copyWith(modelAddToCart: action.modelAddToCart);
}


class SetLoadingForFoodDetail {
  bool isLoading;

  SetLoadingForFoodDetail(this.isLoading);
}


class SetCartCount {
  String cartCount;

  SetCartCount(this.cartCount);
}

class SetFoodDetailData {
  ModelFoodDetail modelFoodDetail;

  SetFoodDetailData(this.modelFoodDetail);
}

class SetAddToCartData {
  ModelAddToCart modelAddToCart;

  SetAddToCartData(this.modelAddToCart);
}
