import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:redux/redux.dart';

import 'ModelFoodDetail.dart';

class FoodDetailViewModel {
  bool isLoading;
  ModelFoodDetail modelFoodDetail;
  String cartCount;

  FoodDetailViewModel(
      {this.isLoading,
        this.modelFoodDetail,
      this.cartCount});

    static FoodDetailViewModel fromStore(Store store) {
    return FoodDetailViewModel(
      isLoading: store.state.foodDetailState.isLoading,
      modelFoodDetail: store.state.foodDetailState.modelFoodDetail,
      cartCount: store.state.foodDetailState.cartCount,
    );
  }
}
