class ModelFoodDetail {
  String success;
  String status;
  String message;
  Data data;

  ModelFoodDetail({this.success, this.status, this.message, this.data});

  ModelFoodDetail.fromJson(Map<String, dynamic> json) {
    if(json == null){
      ModelFoodDetail.initial();
    }else{
      success = json['success'];
      status = json['status'];
      message = json['message'];
      data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  factory ModelFoodDetail.initial() {
    return new ModelFoodDetail(
        status: null, data: null, message: null, success: null);
  }
}

class Data {
  int id;
  String image;
  String title;
  String description;
  String tax;
  String price;
  String offerPrice;
  int userId;
  int ownerId;
  int categoryId;
  String status;
  String createdAt;
  String updatedAt;
  List<Translations> translations;

  Data(
      {this.id,
        this.image,
        this.title,
        this.description,
        this.tax,
        this.price,
        this.offerPrice,
        this.userId,
        this.ownerId,
        this.categoryId,
        this.status,
        this.createdAt,
        this.updatedAt,
        this.translations});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    title = json['title'];
    description = json['description'];
    tax = json['tax'];
    price = json['price'];
    offerPrice = json['offer_price'];
    userId = json['user_id'];
    ownerId = json['owner_id'];
    categoryId = json['category_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['translations'] != null) {
      translations = new List<Translations>();
      json['translations'].forEach((v) {
        translations.add(new Translations.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['title'] = this.title;
    data['description'] = this.description;
    data['tax'] = this.tax;
    data['price'] = this.price;
    data['offer_price'] = this.offerPrice;
    data['user_id'] = this.userId;
    data['owner_id'] = this.ownerId;
    data['category_id'] = this.categoryId;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.translations != null) {
      data['translations'] = this.translations.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Translations {
  int id;
  int productId;
  String title;
  String description;
  String locale;

  Translations(
      {this.id, this.productId, this.title, this.description, this.locale});

  Translations.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    title = json['title'];
    description = json['description'];
    locale = json['locale'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['title'] = this.title;
    data['description'] = this.description;
    data['locale'] = this.locale;
    return data;
  }
}
