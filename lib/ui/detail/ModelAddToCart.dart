class ModelAddToCart {
  String success;
  String status;
  String message;
  Data data;

  ModelAddToCart({this.success, this.status, this.message, this.data});

  ModelAddToCart.fromJson(Map<String, dynamic> json) {
    if(json == null){
      ModelAddToCart.initial();
    }else{
      success = json['success'];
      status = json['status'];
      message = json['message'];
      data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  factory ModelAddToCart.initial() {
    return new ModelAddToCart(
        status: null, data: null, message: null, success: null);
  }
}

class Data {
  String id;
  String title;
  String image;
  String description;
  String productId;
  String quantity;
  String price;
  String total;

  Data(
      {this.id,
        this.title,
        this.image,
        this.description,
        this.productId,
        this.quantity,
        this.price,
        this.total});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    description = json['description'];
    productId = json['product_id'];
    quantity = json['quantity'];
    price = json['price'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    data['description'] = this.description;
    data['product_id'] = this.productId;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    data['total'] = this.total;
    return data;
  }
}
