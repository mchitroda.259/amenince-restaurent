import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:redux/redux.dart';

import 'FavListModel.dart';
import 'FavState.dart';

final favReducer = combineReducers<FavState>([
  TypedReducer<FavState, SetLoadingForFavList>(_setLoading),
  TypedReducer<FavState, SetFavListData>(_setFavListData),
  TypedReducer<FavState, SetAddToCartData>(_setAddTOCartData),
  TypedReducer<FavState, SetCartCount>(_setCartCount),

]);

FavState _setLoading(FavState state, SetLoadingForFavList action) {
  return state.copyWith(isLoading: action.isLoading);
}

FavState _setFavListData(
    FavState state, SetFavListData action) {
  return state.copyWith(trendingListModel: action.trendingListModel);
}

FavState _setAddTOCartData(
    FavState state, SetAddToCartData action) {
  return state.copyWith(modelAddToCart: action.modelAddToCart);
}


class SetLoadingForFavList {
  bool isLoading;

  SetLoadingForFavList(this.isLoading);
}


class SetFavListData {
  FavListModel trendingListModel;

  SetFavListData(this.trendingListModel);
}

class SetAddToCartData {
  ModelAddToCart modelAddToCart;

  SetAddToCartData(this.modelAddToCart);
}


FavState _setCartCount(FavState state, SetCartCount action) {
  return state.copyWith(cartCount: action.cartCount);
}

class SetCartCount {
  String cartCount;

  SetCartCount(this.cartCount);
}
