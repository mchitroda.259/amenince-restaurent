import 'package:amen_inch/ui/menuItemDetails/ModelMenuDetail.dart';

class FavListModel {
  String success;
  String status;
  String message;
  List<Products> data;

  FavListModel({this.success, this.status, this.message, this.data});

  FavListModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<Products>();
      json['data'].forEach((v) {
        data.add(new Products.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
