import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/ui/favorites/FavActions.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/widgets/FavItemList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/src/store.dart';

import 'FavViewModel.dart';


class FavScreen extends StatefulWidget {
  @override
  State createState() {
    return Cart();
  }
}

class Cart extends State<FavScreen> {
  int _currentIndex = 0;
  Store<AppState> _store;

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, FavViewModel>(
      converter: (store) => FavViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
        store.dispatch(getFavListActions(onFailure))
      },
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  buildContent(FavViewModel viewModel, BuildContext context) {
    final subView = <Widget>[];
    if (viewModel.trendingListModel == null ||
        viewModel.trendingListModel.data == null) {
      if (!viewModel.isLoading) {
        subView.add(new Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              AppUtils.getLocalizedString("NO_RESULT_FOUND"),
              style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF3a3a3b),
                  fontWeight: FontWeight.bold),
            ),
          ),
        ));
      } else {
        subView.add(new Container(
          child: Text(
            "",
          ),
        ));
      }
    } else {
      subView.add(FavItemWidget(
        store: _store,
        products: viewModel.trendingListModel.data,
      ));
    }

    return UIHelper.getScreen(
      scaffold: Scaffold(
        appBar: UIHelper.getAppBar(AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            AppUtils.getLocalizedString("FAVORITE"), 16, true, context, viewModel.cartCount, ""),
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: SingleChildScrollView(
              child: new Stack(
            children: <Widget>[
              new Container(
                  alignment: Alignment.topCenter,
                  child: new Column(children: subView)),
            ],
          )),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
