import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:flutter/material.dart';

import 'FavListModel.dart';

class FavState {
  bool isLoading = false;
  FavListModel trendingListModel;
  ModelAddToCart modelAddToCart;
  String cartCount;

  FavState({@required this.isLoading,@required this.trendingListModel,@required this.modelAddToCart,@required this.cartCount});

  factory FavState.initial() {
    return new FavState(isLoading: false,trendingListModel: null,modelAddToCart:null,cartCount: "");
  }

  FavState copyWith({bool isLoading,FavListModel trendingListModel,ModelAddToCart modelAddToCart,String cartCount}) {
    return new FavState(
      isLoading: isLoading ?? this.isLoading,
      trendingListModel: trendingListModel ?? this.trendingListModel,
      modelAddToCart: modelAddToCart ?? this.modelAddToCart,
      cartCount: cartCount ?? this.cartCount,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'trendingListModel': this.trendingListModel,
      'modelAddToCart': this.modelAddToCart,
      'cartCount': this.cartCount,
    };
  }

  factory FavState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new FavState(isLoading: map['isLoading'] as bool,
        trendingListModel: FavListModel.fromJson(map['trendingListModel']),
        modelAddToCart: ModelAddToCart.fromJson(map['modelAddToCart']),
        cartCount:  map['cartCount'] as String,);
    } else
      return new FavState.initial();
  }
}
