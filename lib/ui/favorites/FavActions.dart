import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:amen_inch/ui/favorites/FavListModel.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/CartCountModel.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:amen_inch/widgets/models/ModelFav.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import 'FavReducer.dart';

ThunkAction getFavListActions(Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForFavList(true));
    new Future(() async {
      performGetFavList().then((dataModel) async {
        store.dispatch(new SetLoadingForFavList(false));
        if (dataModel is FavListModel) {
          if (dataModel.success == "1" ) {
            store.dispatch(SetFavListData(dataModel));
            store.dispatch(getCartCountAction());
          } else {
            dataModel.data = null;
            store.dispatch(SetFavListData(dataModel));
            onFailure(AppUtils.getLocalizedString("NO_RESULT_FOUND"));
            print(dataModel.message.toString());
            NavigationHelper().pushScreenOnTop(Routes.MAIN_SCREEN);
          }
        } else {
          if (dataModel.status == "401") {
            onFailure(dataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForFavList(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performGetFavList() async {
  ApiProvider apiProvider = ApiProvider();

  String userId = await PreferencesHelper.userId;

  Map body;

  body = {
    "user_id": userId,
  };


  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.GET_TRENDING_LIST,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return FavListModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}

ThunkAction updateToCartActions(
    String id, String clearCart, int qty, Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForFavList(true));
    new Future(() async {
      performUpdateCart(id, clearCart, qty).then((dataModel) async {
        store.dispatch(new SetLoadingForFavList(false));
        if (dataModel.success == "1") {
          store.dispatch(SetAddToCartData(dataModel));
        } else {
          onFailure(dataModel.message.toString());
          print(dataModel.message.toString());
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForFavList(false));
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<dynamic> performUpdateCart(String id, String clearCart, int qty) async {
  ApiProvider apiProvider = ApiProvider();
  String userId = await PreferencesHelper.userId;

  Map body;

  body = {
    "product_id": id,
    "user_id": userId,
    "clear_cart": clearCart,
    "quantity": qty.toString()
  };

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.ADD_CART,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelAddToCart.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}


ThunkAction addCartActions(
    String id, String clearCart, int qty, Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForFavList(true));
    new Future(() async {
      performAddToCart(id, clearCart, qty).then((dataModel) async {
        if (dataModel.success == "1") {
          PreferencesHelper.cartCount.then((value) => {
            value = value + 1,
            PreferencesHelper.setCartCount(value),
            store.dispatch(SetCartCount(value.toString())),
          });
          store.dispatch(getFavListActions(onFailure));
        } else {
          onFailure(dataModel.message.toString());
          print(dataModel.message.toString());
        }
        store.dispatch(new SetLoadingForFavList(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForFavList(false));
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<dynamic> performAddToCart(String id, String clearCart, int qty) async {
  ApiProvider apiProvider = ApiProvider();
  String userId = await PreferencesHelper.userId;

  Map body;

  body = {
    "product_id": id,
    "user_id": userId,
    "clear_cart": clearCart,
    "quantity": qty.toString()
  };

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.ADD_CART,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelAddToCart.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}

ThunkAction updateCartActions(
    String id, String clearCart, int qty, Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForFavList(true));
    new Future(() async {
      updateCart(id, clearCart, qty).then((dataModel) async {
        if (dataModel.success == "1") {
          PreferencesHelper.cartCount.then((value) => {
            value = value + 1,
            PreferencesHelper.setCartCount(value),
            store.dispatch(SetCartCount(value.toString())),
          });
          store.dispatch(getFavListActions(onFailure));
        } else {
          onFailure(dataModel.message.toString());
          print(dataModel.message.toString());
        }
        store.dispatch(new SetLoadingForFavList(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForFavList(false));
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<dynamic> updateCart(String id, String clearCart, int qty) async {
  ApiProvider apiProvider = ApiProvider();


  String userId = await PreferencesHelper.userId;

  Map body;

  body = {"cart_id": id, "clear_cart": clearCart, "quantity": qty.toString(),"user_id" : userId};


  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.UPDATE_MENU,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelAddToCart.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}


ThunkAction addToFavActions(String id, Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForFavList(true));
    new Future(() async {
      performAddToFav(id).then((dataModel) async {
        if (dataModel.success == "1") {
          onFailure(dataModel.message.toString());
          store.dispatch(getFavListActions(onFailure));
        } else {
          onFailure(dataModel.message.toString());
          print(dataModel.message.toString());
        }
        store.dispatch(new SetLoadingForFavList(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForFavList(false));
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<dynamic> performAddToFav(
    String id,
    ) async {
  ApiProvider apiProvider = ApiProvider();

  Map body;

  body = {
    "product_id": id,
  };

  final response = await apiProvider
      .doPostWithToken(APIEndpoints.BASE_URL, APIEndpoints.ADD_FAV, body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelFav.fromJson(response);
  else
    /*if (responseModel.status == "201")
    return updateCart(id, clearCart, qty,);
  else*/
    return ResponseModel.fromJson(response);
}



ThunkAction getCartCountAction() {
  return (Store store) async {
    new Future(() async {
      getCartCount().then((dataModel) async {
        if (dataModel is CartCountModel) {
          if (dataModel.success == "1") {
            store.dispatch(SetCartCount(dataModel.data.totalQuantity));
          } else {
            print(dataModel.message.toString());
          }
          store.dispatch(new SetLoadingForFavList(false));
        } else {
          if (dataModel.status == "401") {
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            print(dataModel.message.toString());
          }
        }
        store.dispatch(new SetLoadingForFavList(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForFavList(false));
        print(error.toString());
      });
    });
  };
}

Future<dynamic> getCartCount() async {
  ApiProvider apiProvider = ApiProvider();

  Map body;

  body = {};

  final response = await apiProvider.doGet(
    APIEndpoints.GET_CART_COUNT,
    APIEndpoints.BASE_URL,
  );
  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return CartCountModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}
