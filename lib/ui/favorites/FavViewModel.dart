import 'package:redux/redux.dart';

import 'FavListModel.dart';

class FavViewModel {
  bool isLoading;
  String cartCount;

  FavListModel trendingListModel;

  FavViewModel({this.isLoading,
    this.trendingListModel,this.cartCount});

  static FavViewModel fromStore(Store store) {
    return FavViewModel(
      isLoading: store.state.favState.isLoading,
      trendingListModel: store.state.favState.trendingListModel,
      cartCount: store.state.favState.cartCount,
    );
  }
}
