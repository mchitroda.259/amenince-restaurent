import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:redux/redux.dart';

import 'MenuListActionsModel.dart';
import 'MenuListState.dart';

final menuListReducer = combineReducers<MenuListState>([
  TypedReducer<MenuListState, SetLoadingForMenuList>(_setLoading),
  TypedReducer<MenuListState, SetMenuListData>(_setMenuListData),
  TypedReducer<MenuListState, SetAddToCartData>(_setAddTOCartData),
  TypedReducer<MenuListState, SetCartCount>(_setCartCount),

]);

MenuListState _setLoading(MenuListState state, SetLoadingForMenuList action) {
  return state.copyWith(isLoading: action.isLoading);
}

MenuListState _setMenuListData(
    MenuListState state, SetMenuListData action) {
  return state.copyWith(restaurantListModel: action.menuListModel);
}

MenuListState _setAddTOCartData(
    MenuListState state, SetAddToCartData action) {
  return state.copyWith(modelAddToCart: action.modelAddToCart);
}

MenuListState _setCartCount(MenuListState state, SetCartCount action) {
  return state.copyWith(cartCount: action.cartCount);
}

class SetCartCount {
  String cartCount;

  SetCartCount(this.cartCount);
}


class SetLoadingForMenuList {
  bool isLoading;

  SetLoadingForMenuList(this.isLoading);
}


class SetMenuListData {
  MenuListActionsModel menuListModel;

  SetMenuListData(this.menuListModel);
}

class SetAddToCartData {
  ModelAddToCart modelAddToCart;

  SetAddToCartData(this.modelAddToCart);
}
