import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/widgets/MenuListWidget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/src/store.dart';

import 'MenuListActions.dart';
import 'MenuListViewModel.dart';

class MenuListScreen extends StatefulWidget {
  String type = '', searchKey = '';

  MenuListScreen({Key key, @required this.type, @required this.searchKey})
      : super(key: key);

  @override
  State createState() {
    return MenuList();
  }
}

class MenuList extends State<MenuListScreen> {
  int _currentIndex = 0;
  Store<AppState> _store;
  TextEditingController codeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    codeController.text = widget.searchKey;

    return new StoreConnector<AppState, MenuListViewModel>(
      converter: (store) => MenuListViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
        store.state.menuListState.menuListModel = null,
        store.dispatch(
            getMenuListActions(onFailure))
      },
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  buildContent(MenuListViewModel viewModel, BuildContext context) {
    final subView = <Widget>[];
   /* if (viewModel.restaurantListModel == null ||
        viewModel.restaurantListModel.data == null) {
      if (!viewModel.isLoading) {
        subView.add(new Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              "No Data Found!",
              style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF3a3a3b),
                  fontWeight: FontWeight.bold),
            ),
          ),
        ));
      } else {
        subView.add(new Container(
          child: Text(
            "",
          ),
        ));
      }
    } else {
      subView.add(
        Container(
            height: 70,
            padding: EdgeInsets.all(10),
            alignment: Alignment.centerLeft,
            decoration: new BoxDecoration(
              color: AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            ),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: const BorderRadius.all(
                  Radius.circular(15.0),
                ),
              ),
              child: TextField(
                textAlignVertical: TextAlignVertical.center,
                textAlign: TextAlign.left,
                controller: codeController,
                onEditingComplete: () {
                  _store.dispatch(getRestaurantListActions(
                      widget.type, codeController.text, onFailure));
                },
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.zero,
                  filled: true,
                  prefixIcon: Icon(
                    Icons.search,
                  ),
                  suffixIcon: IconButton(
                    onPressed: () {
                      codeController.clear();
                      _store.dispatch(getRestaurantListActions(
                          widget.type, codeController.text, onFailure));
                    },
                    icon: Icon(Icons.clear),
                  ),
                  fillColor: Color(0xFFFAFAFA),
                  hintStyle:
                      new TextStyle(color: Color(0xFFd0cece), fontSize: 18),
                  hintText: AppUtils.getLocalizedString("SEARCH_HINT"),
                ),
              ),
            )),
      );
      subView.add(MenuListWidget(
        restaurantListModel: viewModel.restaurantListModel,
      ));
    }
*/
    subView.add(MenuListWidget(
      restaurantListModel: viewModel.menuListModel,
    ));

    return UIHelper.getScreen(
      scaffold: Scaffold(
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: SingleChildScrollView(
              child: new Stack(
            children: <Widget>[
              new Container(child: new Column(children: subView)),
            ],
          )),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
