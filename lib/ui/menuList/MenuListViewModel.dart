import 'package:redux/redux.dart';

import 'MenuListActionsModel.dart';

class MenuListViewModel {
  bool isLoading;
  String cartCount;

  MenuListActionsModel menuListModel;

  MenuListViewModel({this.isLoading,
    this.menuListModel,
    this.cartCount});

  static MenuListViewModel fromStore(Store store) {
    return MenuListViewModel(
      isLoading: store.state.menuListState.isLoading,
      menuListModel: store.state.menuListState.menuListModel,
      cartCount: store.state.menuListState.cartCount,
    );
  }
}
