import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:flutter/material.dart';

import 'MenuListActionsModel.dart';

class MenuListState {
  bool isLoading = false;
  MenuListActionsModel menuListModel;
  ModelAddToCart modelAddToCart;
  String cartCount;

  MenuListState({@required this.isLoading,@required this.menuListModel,@required this.modelAddToCart,this.cartCount});

  factory MenuListState.initial() {
    return new MenuListState(isLoading: false,menuListModel: null,modelAddToCart:null,cartCount: "");
  }

  MenuListState copyWith({bool isLoading,MenuListActionsModel restaurantListModel,ModelAddToCart modelAddToCart,String cartCount}) {
    return new MenuListState(
      isLoading: isLoading ?? this.isLoading,
      menuListModel: restaurantListModel ?? this.menuListModel,
      modelAddToCart: modelAddToCart ?? this.modelAddToCart,
        cartCount: cartCount ?? this.cartCount

    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'restaurantListModel': this.menuListModel,
      'modelAddToCart': this.modelAddToCart,
      'cartCount': this.cartCount,

    };
  }

  factory MenuListState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new MenuListState(isLoading: map['isLoading'] as bool,
        menuListModel: MenuListActionsModel.fromJson(map['restaurantListModel']),
        modelAddToCart: ModelAddToCart.fromJson(map['modelAddToCart']),
        cartCount: map['cartCount'] as String,);
    } else
      return new MenuListState.initial();
  }
}
