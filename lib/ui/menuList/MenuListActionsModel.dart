class MenuListActionsModel {
  String success;
  String status;
  String message;
  List<MenuData> data;

  MenuListActionsModel({this.success, this.status, this.message, this.data});

  MenuListActionsModel.fromJson(Map<String, dynamic> json) {
    if(json == null){
      MenuListActionsModel.initial();
    }else{
      success = json['success'];
      status = json['status'];
      message = json['message'];
      if (json['data'] != null) {
        data = new List<MenuData>();
        json['data'].forEach((v) {
          data.add(new MenuData.fromJson(v));
        });
      }
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
  factory MenuListActionsModel.initial() {
    return new MenuListActionsModel(
        status: null, data: null, message: null, success: null);
  }
}

class MenuData {
  String id;
  String title;
  String image;
  String ownerId;
  String cartId;
  String countInCart;
  String quantity;
  String price;
  String priceTxt;
  String description;
  String status;
  String isFavorite;

  MenuData(
      {this.id,
        this.title,
        this.image,
        this.ownerId,
        this.cartId,
        this.countInCart,
        this.quantity,
        this.price,
        this.priceTxt,
        this.description,
        this.status,
        this.isFavorite});

  MenuData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    ownerId = json['owner_id'];
    cartId = json['cart_id'];
    countInCart = json['count_in_cart'];
    quantity = json['quantity'];
    price = json['price'];
    priceTxt = json['price_txt'];
    description = json['description'];
    status = json['status'];
    isFavorite = json['is_favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    data['owner_id'] = this.ownerId;
    data['cart_id'] = this.cartId;
    data['count_in_cart'] = this.countInCart;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    data['price_txt'] = this.priceTxt;
    data['description'] = this.description;
    data['status'] = this.status;
    data['is_favorite'] = this.isFavorite;
    return data;
  }
}
