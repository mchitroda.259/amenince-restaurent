import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/CartCountModel.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import 'MenuListActionsModel.dart';
import 'MenuListReducer.dart';

ThunkAction getMenuListActions(Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForMenuList(true));
    new Future(() async {
      performGetMenuList().then((dataModel) async {
        if (dataModel is MenuListActionsModel) {
          if (dataModel.success == "1") {
            store.dispatch(SetMenuListData(dataModel));
          } else {
            dataModel.data = null;
            store.dispatch(SetMenuListData(dataModel));
            onFailure("No result found!");
            print(dataModel.message.toString());
            NavigationHelper().pushScreenOnTop(Routes.MAIN_SCREEN);
          }
        } else {
          if (dataModel.status == "401") {
            onFailure(dataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        }
        store.dispatch(new SetLoadingForMenuList(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForMenuList(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performGetMenuList() async {
  ApiProvider apiProvider = ApiProvider();

  Map body;

  body = {};


  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.GET_MENU_LIST,
      body: body);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return MenuListActionsModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}

