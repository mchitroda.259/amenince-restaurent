class LoginModel {
  String success;
  String status;
  String message;
  Data data;

  LoginModel({this.success, this.status, this.message, this.data});

  LoginModel.fromJson(Map<String, dynamic> json) {
    if(json != null){
      success = json['success'];
      status = json['status'];
      message = json['message'];
      data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    }else{
      success = null;
      status = null;
      message =null;
      data = null;
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String token;
  String id;
  String gender;
  String age;
  String dob;
  String name;
  String email;
  String countryCode;
  String phoneNumber;
  String status;
  String userType;

  Data(
      {this.token,
        this.id,
        this.gender,
        this.age,
        this.dob,
        this.name,
        this.email,
        this.countryCode,
        this.phoneNumber,
        this.status,
        this.userType});

  Data.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    id = json['id'];
    gender = json['gender'];
    age = json['age'];
    dob = json['dob'];
    name = json['name'];
    email = json['email'];
    countryCode = json['country_code'];
    phoneNumber = json['phone_number'];
    status = json['status'];
    userType = json['user_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['id'] = this.id;
    data['gender'] = this.gender;
    data['age'] = this.age;
    data['dob'] = this.dob;
    data['name'] = this.name;
    data['email'] = this.email;
    data['country_code'] = this.countryCode;
    data['phone_number'] = this.phoneNumber;
    data['status'] = this.status;
    data['user_type'] = this.userType;
    return data;
  }
}
