import 'package:amen_inch/ui/login/LoginModel.dart';
import 'package:redux/redux.dart';

import 'LoginActions.dart';

class LoginViewModel {
  bool isLoading, isLoggedIn;
  Function(String, String, String, Function) login;
  String phoneNumber, countryCode;
  LoginModel loginModel;

  LoginViewModel(
      {this.isLoading,
      this.isLoggedIn,
      this.login(String phoneNumber, String countryCode, String password,
          Function onFailure),
      this.phoneNumber,
      this.countryCode,
      this.loginModel});

  static LoginViewModel fromStore(Store store) {
    return LoginViewModel(
        isLoading: store.state.loginState.isLoading,
        isLoggedIn: store.state.loginState.isLoggedin,
        login: (String phoneNumber, String countryCode, String password,
            Function onFailure) {
          store.dispatch(loginAction(
              store, phoneNumber, countryCode, password, onFailure));
        },
        loginModel: store.state.loginState.loginModel);
  }
}
