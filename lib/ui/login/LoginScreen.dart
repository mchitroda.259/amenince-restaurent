
import 'package:amen_inch/ui/forgotpass/ForgotPassScreen.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:amen_inch/utils/Validation.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'LoginViewModel.dart';

// ignore: must_be_immutable
class LoginScreen extends StatefulWidget {
  LoginViewModel viewModel;

  @override
  State createState() {
    return Login();
  }
}

class Login extends State<LoginScreen> {
  TextEditingController phoneController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  FocusNode phoneNode = new FocusNode();
  FocusNode usernameNode = new FocusNode();
  FocusNode passwordNode = new FocusNode();

  final _formKey = GlobalKey<FormState>();
  bool isShow = true, isToggleShown = false, isRememberMe = true;

  final TextEditingController controller = TextEditingController();
  Country selectedCountry  = CountryPickerUtils.getCountryByIsoCode('AM');

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, LoginViewModel>(
      converter: (store) => LoginViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel),
    );
  }

  buildContent(LoginViewModel viewModel) {
    return UIHelper.getScreen(
      scaffold: Scaffold(
          // appBar: UIHelper.getAppBar('Login', 16),
          body: LoadingDialog(
        isLoading: viewModel.isLoading,
        child: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            child: Container(
              child: Padding(
                padding: EdgeInsets.fromLTRB(6, 50, 6, 6),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      new Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                            padding: EdgeInsets.only(left: 10),
                            child: Text.rich(
                              TextSpan(
                                text: AppUtils.getLocalizedString("WELCOME"), // default text style
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 30),
                              ),
                            )),
                      ),
                      Container(
                          padding: EdgeInsets.only(left: 10, bottom: 60),
                          child: Text.rich(
                            TextSpan(
                              text: AppUtils.getLocalizedString("SIGN_IN_TO_CONTINUE"),
                              // default text style
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            ),
                          )),
                      new Align(
                        alignment: Alignment.center,
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left: 10),
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(left: 5),
                                  margin: EdgeInsets.only(bottom: 10),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black),
                                      borderRadius:
                                          new BorderRadius.circular(10.0)),
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: CountryPickerDropdown(
                                          initialValue: 'AM',
                                          itemBuilder: _buildDropdownItem,
                                          onValuePicked: (Country country) {
                                            selectedCountry = country;
                                            print("${country.name}");
                                          },
                                        ),
                                      ),
                                      Expanded(
                                        child: TextFormField(
                                          focusNode: phoneNode,
                                          controller: phoneController,
                                          keyboardType: TextInputType.phone,
                                          validator: (t) {
                                            return Validation()
                                                .validatePhoneNumber(t);
                                          },
                                          decoration: InputDecoration(
                                            labelText: AppUtils.getLocalizedString("PHONE_NO"),
                                            border: InputBorder.none,
                                            hintText: '60868686'
                                          ),
                                          onFieldSubmitted: (v) {},
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 10),
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black),
                                      borderRadius:
                                          new BorderRadius.circular(10.0)),
                                  child: new TextFormField(
                                    autofocus: false,
                                    obscureText: isShow,
                                    keyboardType: TextInputType.text,
                                    controller: passwordController,
                                    decoration: InputDecoration(
                                      labelText: AppUtils.getLocalizedString("PASSWORD"),
                                      border: InputBorder.none,
                                    ),
                                    validator: (t) {
                                      return Validation().validatePassword(t);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    /*  new InkWell(
                        child: Container(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.only(
                                        right: 8, top: 10, bottom: 8),
                                    child: Text(AppUtils.getLocalizedString("FORGOT_PASSWORD"),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        onTap: () {
                          NavigationHelper().pushScreen(ForgotPassScreen());
                        },
                      ),*/
                      Container(
                          padding:
                              EdgeInsets.only(left: 10, right: 10, top: 10),
                          child: SizedBox(
                            height: 50,
                            child: UIHelper.getFlatButton(context,
                                backGroundColor: AppUtils.hexToColor(
                                    AppConfig.COLOR_SECONDARY),
                                buttonName: AppUtils.getLocalizedString("BTN_SIGN_IN"), doOnPress: () {
                              if (_formKey.currentState.validate()) {
                                print(usernameController.text);
                                print(passwordController.text);
                                _performLogin(viewModel);
                              }
                            }),
                          )),
                      /*Container(
                          padding:
                              EdgeInsets.only(left: 10, right: 10, top: 20),
                          child: SizedBox(
                            height: 50,
                            child: UIHelper.getFlatButton(context,
                                backGroundColor: AppUtils.hexToColor(
                                    AppConfig.COLOR_SECONDARY),
                                buttonName: AppUtils.getLocalizedString("DO_NOT_HAVE_ACCOUNT"),
                                doOnPress: () {
                              NavigationHelper().pushScreen(RegisterScreen(
                                isForEdit: false,
                              ));
                            }),
                          )),*/
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      )),
    );
  }

  Widget _buildDropdownItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}(${country.isoCode})"),
          ],
        ),
      );

  onFailure(String error) {
    _showMessage(error);
  }

  Future<void> _performLogin(LoginViewModel viewModel) async {
    if (selectedCountry == null) {
      _showMessage(AppUtils.getLocalizedString("PLEASE_SELECT_COUNTRY_CODE"));
      return;
    }
    String phoneNumber =
        phoneController.text.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
    phoneNumber.replaceAll(new RegExp(r"\s+\b|\b\s"), "");
    viewModel.login(phoneNumber.toString(), "+" + selectedCountry.phoneCode.toString(),
        passwordController.text.trim().toString(), onFailure);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
