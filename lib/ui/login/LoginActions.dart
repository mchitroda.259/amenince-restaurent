import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/ui/login/LoginModel.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'LoginReducer.dart';

ThunkAction loginAction(Store store, String phoneNumber, String countryCode,
    String password, Function onFailure) {
  store.dispatch(new SetLoadingForLogin(true));
  return (Store store) async {
    new Future(() async {
      performLogin(store, phoneNumber, countryCode, password).then(
          (loginModel) async {
        store.dispatch(new SetLoadingForLogin(false));
        if (loginModel.success == "1") {
          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken(loginModel.data.token);
          PreferencesHelper.setUserId(loginModel.data.id);
          PreferencesHelper.setUserName(loginModel.data.name);
          PreferencesHelper.setUserEmail(loginModel.data.email);

          Constants.userId = loginModel.data.id;
          // NavigationHelper().pushScreenOnTop(Routes.LOCATION);
          NavigationHelper().pushScreenOnTop(Routes.MAIN_SCREEN);
          // NavigationHelper().pushScreen(OTPScreen(phoneNumber: phoneNumber,countryCode: countryCode,));
        } else {
          onFailure(loginModel.message.toString());
          PreferencesHelper.setAuthenticated(false);
          print(loginModel.message.toString());
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForLogin(false));
        PreferencesHelper.setAuthenticated(false);
        onFailure(error.toString());
        print(error.toString());
      });
    });
  };
}

Future<LoginModel> performLogin(Store store, String phoneNumber,
    String countryCode, String password) async {
  ApiProvider apiProvider = ApiProvider();
  Map body;
  body = {
    'phone_number': phoneNumber,
    'country_code': countryCode,
    'password': password,
    'device_token': Constants.NOTIFICATION_TOKEN,
  };

  final response = await apiProvider
      .doPost(APIEndpoints.BASE_URL, APIEndpoints.LOGIN, body: body);
  return LoginModel.fromJson(response);
}
