import 'package:amen_inch/ui/login/LoginModel.dart';
import 'package:meta/meta.dart';

class LoginState {
  bool isLoading = false;
  bool isLoggedin = false;

  String phoneNumber = "";
  String countryCode = "";
  LoginModel loginModel;

  LoginState(
      {@required this.isLoggedin,
      @required this.isLoading,
      @required this.loginModel,
      this.phoneNumber,
      this.countryCode});

  factory LoginState.initial() {
    return new LoginState(
        isLoggedin: false,
        isLoading: false,
        loginModel: null,
        phoneNumber: null,
        countryCode: null);
  }

  LoginState copyWith(
      {bool isLoggedin,
      bool isLoading,
      LoginModel loginModel,
      String phoneNumber,
      String countryCode}) {
    return new LoginState(
        isLoggedin: isLoggedin ?? this.isLoggedin,
        isLoading: isLoading ?? this.isLoading,
        loginModel: loginModel ?? this.loginModel,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        countryCode: countryCode ?? this.countryCode);
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'isLoggedin': this.isLoggedin,
      'loginModel': this.loginModel,
      'phoneNumber': this.phoneNumber,
      'countryCode': this.countryCode,
    };
  }

  factory LoginState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new LoginState(
          isLoading: map['isLoading'] as bool,
          isLoggedin: map['isLoggedin'] as bool,
          loginModel: LoginModel.fromJson(map['loginModel']) != null
              ? LoginModel.fromJson(map['loginModel'])
              : null,
          phoneNumber: map['phoneNumber'] as String,
          countryCode: map['countryCode'] as String);
    } else
      return new LoginState.initial();
  }
}
