import 'package:redux/redux.dart';

import 'LoginState.dart';

final loginReducer = combineReducers<LoginState>([
  TypedReducer<LoginState, SetLogin>(_setLogin),
  TypedReducer<LoginState, SetLoadingForLogin>(_setLoading),
  TypedReducer<LoginState, SetPerformLogin>(_setPerformLogin),

]);

LoginState _setPerformLogin(LoginState state, SetPerformLogin action) {
  return state.copyWith(phoneNumber: action.phoneNumber,countryCode: action.countryCode);
}


LoginState _setLogin(LoginState state, SetLogin action) {
  return state.copyWith(isLoggedin: action.isLoggedin);
}

LoginState _setLoading(LoginState state, SetLoadingForLogin action) {
  return state.copyWith(isLoading: action.isLoading);
}

class SetLoadingForLogin {
  bool isLoading;

  SetLoadingForLogin(this.isLoading);
}

class SetRememberMe {
  bool isRememberMe;

  SetRememberMe(this.isRememberMe);
}

class SetLogin {
  bool isLoggedin;

  SetLogin(this.isLoggedin);
}

class SetPerformLogin {
  String  phoneNumber;
  String countryCode;

  SetPerformLogin(this.phoneNumber,this.countryCode);
}