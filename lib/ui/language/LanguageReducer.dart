import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:redux/redux.dart';

import 'LanguageListModel.dart';
import 'LanguageState.dart';

final languageReducer = combineReducers<LanguageState>([
  TypedReducer<LanguageState, SetLoadingForLanguageList>(_setLoading),
  TypedReducer<LanguageState, SetLanguageListData>(_setLanguageListData),
  TypedReducer<LanguageState, SetAddToCartData>(_setAddTOCartData),
  TypedReducer<LanguageState, SetLanguage>(_setCartCount),

]);

LanguageState _setLoading(LanguageState state, SetLoadingForLanguageList action) {
  return state.copyWith(isLoading: action.isLoading);
}

LanguageState _setLanguageListData(
    LanguageState state, SetLanguageListData action) {
  return state.copyWith(languageListModel:  action.languageListModel);
}

LanguageState _setAddTOCartData(
    LanguageState state, SetAddToCartData action) {
  return state.copyWith(modelAddToCart: action.modelAddToCart);
}

LanguageState _setCartCount(LanguageState state, SetLanguage action) {
  return state.copyWith(cartCount: action.cartCount);
}

class SetLanguage {
  String cartCount;

  SetLanguage(this.cartCount);
}


class SetLoadingForLanguageList {
  bool isLoading;

  SetLoadingForLanguageList(this.isLoading);
}


class SetLanguageListData {
  LanguageListModel languageListModel;

  SetLanguageListData(this.languageListModel);
}

class SetAddToCartData {
  ModelAddToCart modelAddToCart;

  SetAddToCartData(this.modelAddToCart);
}
