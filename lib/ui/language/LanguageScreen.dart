import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/ui/profile/ProfileActions.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/widgets/LanguageListItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/src/store.dart';

import 'LanguageActions.dart';
import 'LanguageViewModel.dart';

class LanguageScreen extends StatefulWidget {
  Store profileStore;
  LanguageScreen({Key key,@required this.profileStore})
      : super(key: key);

  @override
  State createState() {
    return Language();
  }
}

class Language extends State<LanguageScreen> {
  int _currentIndex = 0;
  Store<AppState> _store;
  TextEditingController codeController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    return new StoreConnector<AppState, LanguageViewModel>(
      converter: (store) => LanguageViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
        store.dispatch(
            getLanguageListActions(onFailure))
      },
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  buildContent(LanguageViewModel viewModel, BuildContext context) {
    final subView = <Widget>[];
    if (viewModel.languageListModel == null ||
        viewModel.languageListModel.data == null) {
      if (!viewModel.isLoading) {
        subView.add(new Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              AppUtils.getLocalizedString("NO_RESULT_FOUND"),
              style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF3a3a3b),
                  fontWeight: FontWeight.bold),
            ),
          ),
        ));
      } else {
        subView.add(new Container(
          child: Text(
            "",
          ),
        ));
      }
    } else {
      subView.add(new LanguageListItemWidget(
        languageListModel: viewModel.languageListModel,
        store: _store,
        selectedLan: viewModel.cartCount,
      ));
    }

    return new WillPopScope(
      child:  UIHelper.getScreen(
        scaffold: Scaffold(
          appBar: UIHelper.getAppBar(AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
              AppUtils.getLocalizedString("LANGUAGE"), 16, false, context,"",""),
          body: LoadingDialog(
            isLoading: viewModel.isLoading,
            child: SingleChildScrollView(
                child: new Stack(
                  children: <Widget>[
                    new Container(child: new Column(children: subView)),
                  ],
                )),
          ),
        ),
      ),
      onWillPop: () async {
        print("on Back");
        NavigationHelper().pushScreenOnTop(Routes.MAIN_SCREEN);
        return true;
      },
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
