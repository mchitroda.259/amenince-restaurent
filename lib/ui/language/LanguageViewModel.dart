import 'package:redux/redux.dart';

import 'LanguageListModel.dart';

class  LanguageViewModel {
  bool isLoading;
  String cartCount;

  LanguageListModel languageListModel;

  LanguageViewModel({this.isLoading,
    this.languageListModel,
    this.cartCount});

  static LanguageViewModel fromStore(Store store) {
    return LanguageViewModel(
      isLoading: store.state.languageState.isLoading,
      languageListModel: store.state.languageState.languageListModel,
      cartCount: store.state.languageState.cartCount,
    );
  }
}
