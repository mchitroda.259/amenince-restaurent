class LanguageListModel {
  String success;
  String status;
  String message;
  List<Data> data;

  LanguageListModel({this.success, this.status, this.message, this.data});

  LanguageListModel.fromJson(Map<String, dynamic> json) {
    if(json == null){
      LanguageListModel.initial();
    }else{
      success = json['success'];
      status = json['status'];
      message = json['message'];
      if (json['data'] != null) {
        data = new List<Data>();
        json['data'].forEach((v) {
          data.add(new Data.fromJson(v));
        });
      }
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }

  factory LanguageListModel.initial() {
    return new LanguageListModel(
        status: null, data: null, message: null, success: null);
  }
}

class Data {
  String id;
  String title;
  String code;
  bool isSelected = false;

  Data({this.id, this.title, this.code, this.isSelected});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    code = json['code'];
    isSelected = json['isSelected'];
    if (isSelected == null) isSelected = false;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['code'] = this.code;
    if (this.isSelected == null) this.isSelected = false;
    data['isSelected'] = this.isSelected;
    return data;
  }
}
