import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import 'LanguageListModel.dart';
import 'LanguageReducer.dart';

ThunkAction getLanguageListActions(Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForLanguageList(true));
    new Future(() async {
      performGetLanguageList().then((dataModel) async {
        if (dataModel is LanguageListModel) {
          if (dataModel.success == "1") {
            PreferencesHelper.language.then(
                    (value) => {store.dispatch(SetLanguage(value.toString()))});
            store.dispatch(SetLanguageListData(dataModel));
          } else {
            dataModel.data = null;
            store.dispatch(SetLanguageListData(dataModel));
            onFailure(AppUtils.getLocalizedString("NO_RESULT_FOUND"));
            print(dataModel.message.toString());
          }
        } else {
          if (dataModel.status == "401") {
            onFailure(dataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        }
        store.dispatch(new SetLoadingForLanguageList(false));
      }, onError: (error) async {
        store.dispatch(new SetLoadingForLanguageList(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performGetLanguageList() async {
  ApiProvider apiProvider = ApiProvider();

  Map body;

  body = {
  };

  final response = await apiProvider.doGet(
    APIEndpoints.GET_LANGUAGE,
    APIEndpoints.BASE_URL,
  );


  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return LanguageListModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}
