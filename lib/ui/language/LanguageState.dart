import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:flutter/material.dart';

import 'LanguageListModel.dart';

class LanguageState {
  bool isLoading = false;
  LanguageListModel languageListModel;
  ModelAddToCart modelAddToCart;
  String cartCount;

  LanguageState({@required this.isLoading,@required this.languageListModel,@required this.modelAddToCart,this.cartCount});

  factory LanguageState.initial() {
    return new LanguageState(isLoading: false,languageListModel: null,modelAddToCart:null,cartCount: "");
  }

  LanguageState copyWith({bool isLoading,LanguageListModel languageListModel,ModelAddToCart modelAddToCart,String cartCount}) {
    return new LanguageState(
      isLoading: isLoading ?? this.isLoading,
        languageListModel: languageListModel ?? this.languageListModel,
      modelAddToCart: modelAddToCart ?? this.modelAddToCart,
        cartCount: cartCount ?? this.cartCount

    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'languageListModel': this.languageListModel,
      'modelAddToCart': this.modelAddToCart,
      'cartCount': this.cartCount,

    };
  }

  factory LanguageState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new LanguageState(isLoading: map['isLoading'] as bool,
        languageListModel: LanguageListModel.fromJson(map['languageListModel']),
        modelAddToCart: ModelAddToCart.fromJson(map['modelAddToCart']),
        cartCount: map['cartCount'] as String,);
    } else
      return new LanguageState.initial();
  }
}
