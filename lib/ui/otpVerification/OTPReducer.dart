import 'package:redux/redux.dart';

import 'OTPPassState.dart';

final OTPReducer = combineReducers<OTPState>([
  TypedReducer<OTPState, SetLogin>(_setLogin),
  TypedReducer<OTPState, SetLoadingForLogin>(_setLoading),
  TypedReducer<OTPState, SetPerformOTPVerification>(_setPerformOTPVerification),
]);

OTPState _setLogin(OTPState state, SetLogin action) {
  return state.copyWith(isLoggedin: action.isLoggedin);
}

OTPState _setLoading(OTPState state, SetLoadingForLogin action) {
  return state.copyWith(isLoading: action.isLoading);
}

OTPState _setPerformOTPVerification(OTPState state, SetPerformOTPVerification action) {
  return state.copyWith(phoneNumber: action.phoneNumber,countryCode: action.countryCode,otp: action.otp);
}

class SetLoadingForLogin {
  bool isLoading;

  SetLoadingForLogin(this.isLoading);
}

class SetLogin {
  bool isLoggedin;

  SetLogin(this.isLoggedin);
}

class SetPerformOTPVerification {
  String  phoneNumber;
  String countryCode;
  String otp;

  SetPerformOTPVerification(this.phoneNumber,this.countryCode,this.otp);
}