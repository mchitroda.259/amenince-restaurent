import 'package:amen_inch/ui/otpVerification/OTPVerificationModel.dart';
import 'package:meta/meta.dart';

class OTPState {
  bool isLoading = false;
  bool isLoggedin = false;
  String phoneNumber = "";
  String countryCode = "";
  String otp = "";

  OTPVerificationModel otpVerificationModel;

  OTPState({
    @required this.isLoading,
    @required this.isLoggedin,
    @required this.otpVerificationModel,
    this.phoneNumber,
    this.countryCode,
    this.otp,
  });

  factory OTPState.initial() {
    return new OTPState(
        isLoading: false,
        isLoggedin: false,
        otpVerificationModel: null,
        phoneNumber: null,
        countryCode: null,
        otp: null);
  }

  OTPState copyWith(
      {bool isLoading,
        bool isLoggedin,
      OTPVerificationModel otpVerificationModel,
      String phoneNumber,
      String countryCode,
      String otp}) {
    return new OTPState(
      isLoading: isLoading ?? this.isLoading,
      isLoggedin: isLoggedin ?? this.isLoggedin,
      otpVerificationModel: otpVerificationModel ?? this.otpVerificationModel,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      countryCode: countryCode ?? this.countryCode,
      otp: otp ?? this.otp,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'isLoggedin': this.isLoggedin,
      'otpVerificationModel': this.otpVerificationModel,
      'phoneNumber': this.phoneNumber,
      'countryCode': this.countryCode,
      'otp': this.otp,
    };
  }

  factory OTPState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new OTPState(
        isLoading: map['isLoading'] as bool,
        isLoggedin: map['isLoggedin'] as bool,
        otpVerificationModel:
            OTPVerificationModel.fromJson(map['otpVerificationModel']),
        phoneNumber: map['phoneNumber'] as String,
        countryCode: map['countryCode'] as String,
        otp: map['otp'] as String,
      );
    } else
      return new OTPState.initial();
  }
}
