import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/ui/otpVerification/OTPVerificationModel.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'OTPReducer.dart';

ThunkAction setOTPPassAction(bool isLogin) {
  return (Store store) async {
    new Future(() async {
      store.dispatch(new SetLogin(isLogin));
      var dialogResult = await NavigationHelper()
          .dialogService
          .showDialog('double', 'title', 'message');
      if (dialogResult.confirmed) {
        print('User has confirmed');
      } else {
        print('User cancelled the dialog');
      }
    });
  };
}

ThunkAction sendOTPAction(
    Store store, String phoneNumber, String countryCode, Function onFailure) {
  store.dispatch(new SetLoadingForLogin(true));
  return (Store store) async {
    new Future(() async {
      performSendOTP(store, phoneNumber, countryCode).then((loginModel) async {
        store.dispatch(new SetLoadingForLogin(false));
        if (loginModel.success == "1") {
          onFailure(loginModel.message.toString());
        } else {
          onFailure(loginModel.message.toString());
          print(loginModel.message.toString());
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForLogin(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<OTPVerificationModel> performSendOTP(
    Store store, String phoneNumber, String countryCode) async {
  ApiProvider apiProvider = ApiProvider();
  Map body;
  body = {
    'phone_number': phoneNumber,
    'country_code': countryCode,
  };

  final response = await apiProvider
      .doPost(APIEndpoints.BASE_URL, APIEndpoints.SEND_OTP, body: body);
  return OTPVerificationModel.fromJson(response);
}

ThunkAction verifyOTPAction(Store store, String phoneNumber, String countryCode,
    String otp, Function onFailure) {
  store.dispatch(new SetLoadingForLogin(true));
  return (Store store) async {
    new Future(() async {
      performOTPVerification(store, phoneNumber, countryCode, otp).then(
          (loginModel) async {
        store.dispatch(new SetLoadingForLogin(false));
        if (loginModel.success == "1") {
          NavigationHelper().pushScreenOnTop(Routes.MAIN_SCREEN);
        } else {
          onFailure(loginModel.message.toString());
          print(loginModel.message.toString());
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForLogin(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<OTPVerificationModel> performOTPVerification(
    Store store, String phoneNumber, String countryCode, String otp) async {
  ApiProvider apiProvider = ApiProvider();
  Map body;
  body = {
    'otp': otp,
    'phone_number': phoneNumber,
    'country_code': countryCode,
  };

  final response = await apiProvider
      .doPost(APIEndpoints.BASE_URL, APIEndpoints.VERIFY_OTP, body: body);
  return OTPVerificationModel.fromJson(response);
}
