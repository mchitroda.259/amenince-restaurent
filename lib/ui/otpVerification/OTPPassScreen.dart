import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:redux/redux.dart';

import 'OTPActions.dart';
import 'OTPPassViewModel.dart';

// ignore: must_be_immutable
class OTPScreen extends StatefulWidget {
  String phoneNumber, countryCode;

  OTPScreen({Key key, @required this.phoneNumber, @required this.countryCode})
      : super(key: key);

  @override
  OTPScreenState createState() => OTPScreenState();
}

class OTPScreenState extends State<OTPScreen> {
  Store _store;
  String otp;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, OTPViewModel>(
      converter: (store) => OTPViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
      },
    );
  }

  void matchOtp() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(AppUtils.getLocalizedString("SUCCESSFULLY")),
            content: Text(AppUtils.getLocalizedString("OTP_MATCHED")),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.check),
                  onPressed: () {
                    Navigator.of(context).pop();
                  })
            ],
          );
        });
  }

  buildContent(OTPViewModel viewModel, BuildContext context) {
    return UIHelper.getScreen(
      scaffold: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: UIHelper.getAppBar(AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            AppUtils.getLocalizedString("VERIFY_OTP"), 16, false, context, "", ""),
        backgroundColor: Color(0xFFeaeaea),
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Divider(
                  color: Colors.black54,
                  height: 2,
                ),
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 20),
                  margin: EdgeInsets.only(bottom: 70),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, right: 16.0),
                        child: Text(
                          AppUtils.getLocalizedString("ENTER_YOUR_OTP_CODE_HERE"),
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 22.0,
                              fontWeight: FontWeight.normal),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                OTPTextField(
                  length: 4,
                  obscureText: true,
                  width: MediaQuery.of(context).size.width,
                  textFieldAlignment: MainAxisAlignment.spaceAround,
                  fieldWidth: 50,
                  fieldStyle: FieldStyle.underline,
                  style: TextStyle(fontSize: 17),
                  onChanged: (pin) {
                    print("Changed: " + pin);
                  },
                  onCompleted: (pin) {
                    print("Completed: " + pin);
                    otp = pin;
                  },
                ),
                Container(
                  child: Container(
                      margin: EdgeInsets.only(top: 30),
                      padding: EdgeInsets.only(left: 10, right: 10, top: 30),
                      child: SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: UIHelper.getFlatButton(context,
                            backGroundColor:
                                AppUtils.hexToColor(AppConfig.COLOR_SECONDARY),
                            buttonName: AppUtils.getLocalizedString("BTN_VERIFY"), doOnPress: () {
                          _performVerifyOTP(viewModel);
                        }),
                      )),
                ),
                InkWell(
                  onTap: () {
                    _store.dispatch(sendOTPAction(_store, widget.phoneNumber,
                        widget.countryCode, onFailure));
                  },
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text(
                      AppUtils.getLocalizedString("RESET_OTP"),
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  Future<void> _performVerifyOTP(OTPViewModel viewModel) async {
    if (otp == null) {
      _showMessage(AppUtils.getLocalizedString("PLEASE_ENTER_VALID_OTP"));
      return;
    } else if (otp.isEmpty) {
      _showMessage(AppUtils.getLocalizedString("PLEASE_ENTER_VALID_OTP"));
      return;
    }

    viewModel.verifyOTP(widget.phoneNumber, widget.countryCode, otp, onFailure);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
