import 'package:amen_inch/ui/otpVerification/OTPVerificationModel.dart';
import 'package:redux/redux.dart';

import 'OTPActions.dart';

class OTPViewModel {
  bool isLoading, isLoggedIn;
  Function setOTPVerification;
  Function(String, String, String, Function) verifyOTP;
  String phoneNumber, countryCode,otp;
  OTPVerificationModel otpVerificationModel;

  OTPViewModel({this.isLoading,
    this.isLoggedIn,
    this.setOTPVerification,
    this.verifyOTP(String phoneNumber, String countryCode,String otp, Function onFailure),
    this.phoneNumber,
    this.countryCode,
    this.otp,
    this.otpVerificationModel});

  static OTPViewModel fromStore(Store store) {
    return OTPViewModel(
        isLoading: store.state.otpState.isLoading,
        isLoggedIn: store.state.otpState.isLoggedin,
        setOTPVerification: () {
          store.dispatch(setOTPPassAction(true));
        },
        verifyOTP: (String phoneNumber, String countryCode,String otp, Function onFailure) {
          store.dispatch(
              verifyOTPAction(store, phoneNumber, countryCode,otp, onFailure));
        },
        otpVerificationModel: store.state.otpState.otpVerificationModel);
  }
}
