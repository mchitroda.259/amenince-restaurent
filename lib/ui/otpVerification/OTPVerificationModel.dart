class OTPVerificationModel {
  String _success;
  String _status;
  String _message;
  String _data;

  OTPVerificationModel({String success, String status, String message/*, String data*/}) {
    this._success = success;
    this._status = status;
    this._message = message;
  //  this._data = data;
  }

  String get success => _success;
  set success(String success) => _success = success;
  String get status => _status;
  set status(String status) => _status = status;
  String get message => _message;
  set message(String message) => _message = message;
 /* String get data => _data;
  set data(String data) => _data = data;*/

  OTPVerificationModel.fromJson(Map<String, dynamic> json) {
    if(json==null){
      OTPVerificationModel.initial();
    }else{
      _success = json['success'];
      _status = json['status'];
      _message = json['message'];
      // _data = json['data'];
    }
  }

  factory OTPVerificationModel.initial(){
    return new OTPVerificationModel(
      success: "",
      message: "",
      status: ""
    );
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this._success;
    data['status'] = this._status;
    data['message'] = this._message;
   // data['data'] = this._data;
    return data;
  }
}