import 'package:redux/redux.dart';

import 'MainState.dart';

final mainReducer = combineReducers<MainState>([
  TypedReducer<MainState, SetLoadingForHome>(_setLoading),
]);

MainState _setLoading(MainState state, SetLoadingForHome action) {
  return state.copyWith(isLoading: action.isLoading);
}

class SetLoadingForHome {
  bool isLoading;

  SetLoadingForHome(this.isLoading);
}
