import 'package:amen_inch/animation/ScaleRoute.dart';
import 'package:amen_inch/ui/favorites/FavScreen.dart';
import 'package:amen_inch/ui/home/HomeScreen.dart';
import 'package:amen_inch/ui/language/LanguageScreen.dart';
import 'package:amen_inch/ui/orderList/OrderListScreen.dart';
import 'package:amen_inch/ui/profile/ProfileScreen.dart';
import 'package:amen_inch/ui/menuList/MenuListScreen.dart';
import 'package:amen_inch/ui/review/ReviewScreen.dart';
import 'package:amen_inch/ui/webview/ContactUs.dart';
import 'package:amen_inch/ui/webview/TermsAndConditions.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'MainViewModel.dart';

class MainScreen extends StatefulWidget {
  @override
  State createState() {
    return Main();
  }
}

class Main extends State<MainScreen> {
  int _currentIndex = 0;
  String userName = "N/A", userEmail = "N/A";
  String profileImage = "";
  bool isFromNotification = false;

  final List<Widget> _children = [
    HomeScreen(),
    MenuListScreen(),
    OrderListScreen(),
    ReviewScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    isFromNotification = ModalRoute.of(context).settings.arguments as bool;

    return new StoreConnector<AppState, MainViewModel>(
      converter: (store) => MainViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    PreferencesHelper.userName.then((value) => {userName = value});
    PreferencesHelper.userEmail.then((value) => {userEmail = value});
    PreferencesHelper.userProfile.then((value) => {profileImage = value});

    if (Constants.isFromMenuUpdate) {
      _currentIndex = 1;
      Constants.isFromMenuUpdate = false;
    }
    if (Constants.isFromNotification != null) {
      if (Constants.isFromNotification) {
        _currentIndex = 2;
      }
    }

    PreferencesHelper.isFromNotification.then((value) => {
      if (value) _currentIndex = 2,
      PreferencesHelper.setFromNotification(false),
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  buildContent(MainViewModel viewModel) {
    return UIHelper.getScreen(
      scaffold: Scaffold(
        appBar: UIHelper.getAppBarWithImageTitle(
            Colors.white, 'Home Screen', 16, true, context, "0"),
        body: _children[_currentIndex],
        drawer: Drawer(
          // Add a ListView to the drawer. This ensures the user can scroll
          // through the options in the drawer if there isn't enough vertical
          // space to fit everything.
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: 200,
                child: DrawerHeader(
                  child: Column(
                    children: [
                      InkWell(
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 8, left: 10),
                            width: 70,
                            height: 70,
                            padding: EdgeInsets.all(5.0),
                            child: CachedNetworkImage(
                              imageUrl: profileImage,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: 70.0,
                                height: 70.0,
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(color: Colors.black),
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.perm_identity),
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                              context, ScaleRoute(page: ProfileScreen()));
                        },
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.bottomLeft,
                            width: MediaQuery.of(context).size.width - 170,
                            padding: EdgeInsets.only(left: 10, top: 10),
                            child: Text(userName,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                style: TextStyle(
                                    color: AppUtils.hexToColor(
                                        AppConfig.COLOR_PRIMARY),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500)),
                          ),
                          Container(
                            alignment: Alignment.bottomLeft,
                            width: 250,
                            padding: EdgeInsets.only(
                              left: 10,
                            ),
                            child: Text(userEmail,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white24,
                  ),
                ),
              ),
              new InkWell(
                child: Container(
                  margin: EdgeInsets.only(top: 5, bottom: 5),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Row(
                        children: [
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              padding: EdgeInsets.all(20),
                              margin:
                                  EdgeInsets.only(top: 10, left: 8, right: 8),
                              alignment: Alignment.centerRight,
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage(
                                      "assets/icon/ic_my_orders.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 10),
                                child: Text(
                                    AppUtils.getLocalizedString("MY_ORDERS"),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500)),
                              ),
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                    AppUtils.getLocalizedString(
                                        "ALL_ORDER_LIST"),
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Color(0xFF6e6e71),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500)),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          padding: EdgeInsets.all(15),
                          margin: EdgeInsets.only(right: 10),
                          alignment: Alignment.centerRight,
                          width: 40,
                          height: 40,
                          child: new Icon(Icons.arrow_forward_ios_outlined),
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  onTabTapped(2);
                  Navigator.pop(context);
                },
              ),
              Divider(), //
              new InkWell(
                child: Container(
                  margin: EdgeInsets.only(top: 5, bottom: 5),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Row(
                        children: [
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              padding: EdgeInsets.all(20),
                              margin:
                                  EdgeInsets.only(top: 10, left: 8, right: 8),
                              alignment: Alignment.centerRight,
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image:
                                      AssetImage("assets/icon/ic_language.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 10),
                                child: Text(
                                    AppUtils.getLocalizedString("LANGUAGE"),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500)),
                              ),
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                    AppUtils.getLocalizedString(
                                        "SELECT_LANGUAGE"),
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Color(0xFF6e6e71),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500)),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          padding: EdgeInsets.all(15),
                          margin: EdgeInsets.only(right: 10),
                          alignment: Alignment.centerRight,
                          width: 40,
                          height: 40,
                          child: new Icon(Icons.arrow_forward_ios_outlined),
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.push(context, ScaleRoute(page: LanguageScreen()));
                },
              ),
              Divider(), //
              new InkWell(
                child: Container(
                  margin: EdgeInsets.only(top: 5, bottom: 5),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Row(
                        children: [
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              padding: EdgeInsets.all(20),
                              margin:
                                  EdgeInsets.only(top: 10, left: 8, right: 5),
                              alignment: Alignment.centerRight,
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image:
                                      AssetImage("assets/icon/ic_t_and_c.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 10),
                                width: MediaQuery.of(context).size.width -
                                    (165 + 65),
                                child: Text(
                                    AppUtils.getLocalizedString(
                                        "TERMS_AND_CONDITIONS"),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500)),
                              ),
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 5),
                                child: Text(
                                    AppUtils.getLocalizedString("READ_MORE"),
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Color(0xFF6e6e71),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500)),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          padding: EdgeInsets.all(15),
                          margin: EdgeInsets.only(right: 10),
                          alignment: Alignment.centerRight,
                          width: 40,
                          height: 40,
                          child: new Icon(Icons.arrow_forward_ios_outlined),
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.push(
                      context, ScaleRoute(page: TermsAndConditions()));
                },
              ),
              Divider(), //
              new InkWell(
                child: Container(
                  margin: EdgeInsets.only(top: 5, bottom: 5),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Row(
                        children: [
                          Align(
                            alignment: Alignment.centerRight,
                            child: Container(
                              padding: EdgeInsets.all(20),
                              margin:
                                  EdgeInsets.only(top: 10, left: 8, right: 5),
                              alignment: Alignment.centerRight,
                              width: 40,
                              height: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage(
                                      "assets/icon/ic_contact_us.png"),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 10),
                                child: Text(
                                    AppUtils.getLocalizedString("CONTACT_US"),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500)),
                              ),
                              Container(
                                alignment: Alignment.bottomLeft,
                                padding: EdgeInsets.only(top: 5),
                                width: MediaQuery.of(context).size.width -
                                    (165 + 65),
                                child: Text(
                                    AppUtils.getLocalizedString(
                                        "FOR_ANY_KIND_OF_SUPPORT"),
                                    maxLines: 2,
                                    style: TextStyle(
                                        color: Color(0xFF6e6e71),
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500)),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: Container(
                          padding: EdgeInsets.all(15),
                          margin: EdgeInsets.only(right: 10),
                          alignment: Alignment.centerRight,
                          width: 40,
                          height: 40,
                          child: new Icon(Icons.arrow_forward_ios_outlined),
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.push(context, ScaleRoute(page: ContactUs()));
                },
              ),
              Divider(), //
              new InkWell(
                child: Container(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        children: <Widget>[
                          Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(
                              left: 10,
                            ),
                            child: Text(AppUtils.getLocalizedString("LOGOUT"),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500)),
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          padding: EdgeInsets.all(15),
                          margin: EdgeInsets.only(left: 10, right: 10),
                          alignment: Alignment.bottomLeft,
                          width: 50,
                          height: 50,
                          child: new Image.asset("assets/icon/ic_logout.png"),
                        ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  _showDialog(context);
                },
              ),
            ],
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          onTap: onTabTapped,
          // new
          currentIndex: _currentIndex,
          // new
          unselectedItemColor: Colors.black,
          items: [
            BottomNavigationBarItem(
                icon: new Icon(Icons.home_outlined),
                title: new Text(AppUtils.getLocalizedString("HOME"))),
            BottomNavigationBarItem(
              icon: new Icon(Icons.menu_book),
              title: new Text(AppUtils.getLocalizedString("MENU")),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.local_shipping_outlined),
              title: new Text(AppUtils.getLocalizedString("ORDERS")),
            ),
            BottomNavigationBarItem(
                icon: new Icon(Icons.star_border),
                title: new Text(AppUtils.getLocalizedString("REVIEWS")))
          ],
        ),
      ),
    );
  }
}

void _showDialog(BuildContext context) async {
  await showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(AppUtils.getLocalizedString("DO_YOU_WANT_TO_LOGOUT")),
        content: Text('We hate to see you leave...'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              print("you choose no");
              Navigator.of(context).pop(false);
            },
            child: Text(AppUtils.getLocalizedString("NO"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
          FlatButton(
            onPressed: () {
              PreferencesHelper.setAuthenticated(false);
              PreferencesHelper.setToken("");
              Navigator.of(context, rootNavigator: true).pop();
              NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
            },
            child: Text(AppUtils.getLocalizedString("YES"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
        ],
      );
    },
  );
}
