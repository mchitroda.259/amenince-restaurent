import 'package:flutter/material.dart';

class MainState {
  bool isLoading = false;

  MainState({@required this.isLoading});

  factory MainState.initial() {
    return new MainState(isLoading: false);
  }

  MainState copyWith({bool isLoading}) {
    return new MainState(
      isLoading: isLoading ?? this.isLoading,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
    };
  }

  factory MainState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new MainState(isLoading: map['isLoading'] as bool);
    } else
      return new MainState.initial();
  }
}
