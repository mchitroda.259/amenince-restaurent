import 'package:amen_inch/ui/resetPassword/ResetPassScreen.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:amen_inch/utils/Validation.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'ForgotPassActions.dart';
import 'ForgotPassViewModel.dart';

// Integrated Facebook Login
// Android :- Added activity in Manifest File.
//            Add Facebook app id in string.xml file
// App is created as Flutter base in Facebook console(Test App)
// Command for debug hash key :- keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64

class ForgotPassScreen extends StatefulWidget {
  @override
  State createState() {
    return ForgotPass();
  }
}

class ForgotPass extends State<ForgotPassScreen> {
  TextEditingController codeController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  FocusNode phoneNode = new FocusNode();
  FocusNode codeNode = new FocusNode();

  final _formKey = GlobalKey<FormState>();
  bool isShow = true, isToggleShown = false, isRememberMe = true;

  final TextEditingController controller = TextEditingController();

  Store _store;
  Country selectedCountry  = CountryPickerUtils.getCountryByIsoCode('AM');

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, ForgotPassViewModel>(
      converter: (store) => ForgotPassViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
      },
    );
  }

  buildContent(ForgotPassViewModel viewModel, BuildContext context) {
    return UIHelper.getScreen(
      scaffold: Scaffold(
          appBar: UIHelper.getAppBar(
              AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
              AppUtils.getLocalizedString("FORGOT_PASS"),
              0,
              false,
              context,
              "",
              ""),
          body: LoadingDialog(
              isLoading: viewModel.isLoading,
              child: SingleChildScrollView(
                child: Container(
                  decoration: BoxDecoration(color: Colors.white),
                  child: Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Divider(
                            color: Colors.black54,
                            height: 2,
                          ),
                          new Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                                padding: EdgeInsets.only(
                                    left: 10, top: 20, right: 10),
                                child: Text.rich(
                                  TextSpan(
                                    text: AppUtils.getLocalizedString("FORGOT_PASS"),
                                    // default text style
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 30),
                                  ),
                                )),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                  left: 10, top: 10, bottom: 60, right: 10),
                              child: Text.rich(
                                TextSpan(
                                  text:
                                  AppUtils.getLocalizedString("OTP_SCREEN_TITLE"),
                                  // default text style
                                  style: TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 16),
                                ),
                              )),
                          Container(
                            padding: EdgeInsets.only(left: 10),
                            margin: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.black),
                                borderRadius: new BorderRadius.circular(10.0)),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: CountryPickerDropdown(
                                    initialValue: 'am',
                                    itemBuilder: _buildDropdownItem,
                                    onValuePicked: (Country country) {
                                      selectedCountry = country;
                                      print("${country.name}");
                                    },
                                  ),
                                ),
                                Expanded(
                                  child: TextFormField(
                                    focusNode: phoneNode,
                                    controller: phoneNumberController,
                                    keyboardType: TextInputType.phone,
                                    validator: (t) {
                                      return Validation()
                                          .validatePhoneNumber(t);
                                    },
                                    decoration: InputDecoration(
                                      labelText: AppUtils.getLocalizedString("LBL_PHONE_NO"),
                                    ),
                                    onFieldSubmitted: (v) {},
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                              padding:
                                  EdgeInsets.only(left: 10, right: 10, top: 20),
                              child: SizedBox(
                                height: 50,
                                child: UIHelper.getFlatButton(context,
                                    backGroundColor: AppUtils.hexToColor(
                                        AppConfig.COLOR_SECONDARY),
                                    buttonName: AppUtils.getLocalizedString("BTN_SEND"), doOnPress: () {
                                  if (selectedCountry == null) {
                                    _showMessage(AppUtils.getLocalizedString("PLEASE_SELECT_COUNTRY_CODE"));
                                    return;
                                  }

                                  _store.dispatch(sendOTPAction(
                                      _store,
                                      phoneNumberController.text.toString(),
                                      "+" + selectedCountry.phoneCode,
                                      context,
                                      onFailure));
                                }),
                              )),
                        ],
                      ),
                    ),
                  ),
                ),
              ))),
    );
  }

  Widget _buildDropdownItem(Country country) => Container(
        child: Row(
          children: <Widget>[
            CountryPickerUtils.getDefaultFlagImage(country),
            SizedBox(
              width: 8.0,
            ),
            Text("+${country.phoneCode}(${country.isoCode})"),
          ],
        ),
      );

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
