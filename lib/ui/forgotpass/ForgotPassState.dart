import 'package:meta/meta.dart';

class ForgotPassState {
  bool isLoading = false;

  ForgotPassState(
      {@required@required this.isLoading});

  factory ForgotPassState.initial() {
    return new ForgotPassState(
      isLoading: false,);
  }

  ForgotPassState copyWith({ bool isLoading}) {
    return new ForgotPassState(
        isLoading: isLoading ?? this.isLoading,);
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
    };
  }

  factory ForgotPassState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new ForgotPassState(
          isLoading: map['isLoading'] as bool,);
    } else
      return new ForgotPassState.initial();
  }
}
