import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/ui/login/LoginModel.dart';
import 'package:amen_inch/ui/otpVerification/OTPVerificationModel.dart';
import 'package:amen_inch/ui/resetPassword/ResetPassScreen.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'ForgotPassReducer.dart';

ThunkAction sendOTPAction(
    Store store, String phoneNumber, String countryCode, BuildContext context,Function onFailure) {
  store.dispatch(new SetLoadingForForgotPass(true));
  return (Store store) async {
    new Future(() async {
      performSendOTP(store, phoneNumber, countryCode).then((loginModel) async {
        store.dispatch(new SetLoadingForForgotPass(false));
        if (loginModel.success == "1") {
          onFailure(loginModel.message.toString());
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ResetPassScreen(
                countryCode: countryCode,
                phoneNumber: phoneNumber,
              ),
            ),
          );
        } else {
          onFailure(loginModel.message.toString());
          print(loginModel.message.toString());
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForForgotPass(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<OTPVerificationModel> performSendOTP(
    Store store, String phoneNumber, String countryCode) async {
  ApiProvider apiProvider = ApiProvider();
  Map body;
  body = {
    'phone_number': phoneNumber,
    'country_code': countryCode,
  };

  final response = await apiProvider
      .doPost(APIEndpoints.BASE_URL, APIEndpoints.SEND_OTP, body: body);
  return OTPVerificationModel.fromJson(response);
}
