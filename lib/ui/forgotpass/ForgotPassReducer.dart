import 'package:redux/redux.dart';

import 'ForgotPassState.dart';

final forgotPassReducer = combineReducers<ForgotPassState>([
  TypedReducer<ForgotPassState, SetLoadingForForgotPass>(_setLoading),
]);


ForgotPassState _setLoading(ForgotPassState state, SetLoadingForForgotPass action) {
  return state.copyWith(isLoading: action.isLoading);
}

class SetLoadingForForgotPass {
  bool isLoading;

  SetLoadingForForgotPass(this.isLoading);
}
