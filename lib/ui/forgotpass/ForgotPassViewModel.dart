import 'package:redux/redux.dart';


class ForgotPassViewModel {
  bool isLoading;

  ForgotPassViewModel({
    this.isLoading,
  });

  static ForgotPassViewModel fromStore(Store store) {
    return ForgotPassViewModel(
      isLoading: store.state.forgotPassState.isLoading,
    );
  }
}
