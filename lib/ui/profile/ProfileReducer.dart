import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:redux/redux.dart';

import 'ProfileState.dart';

final profileReducer = combineReducers<ProfileState>([
  TypedReducer<ProfileState, SetLoadingForProfile>(_setLoading),
  TypedReducer<ProfileState, SetProfileData>(_setProfileData),
]);

ProfileState _setLoading(ProfileState state, SetLoadingForProfile action) {
  return state.copyWith(isLoading: action.isLoading);
}

ProfileState _setProfileData(
    ProfileState state, SetProfileData action) {
  return state.copyWith(modelUserProfile: action.modelUserProfile);
}


class SetLoadingForProfile {
  bool isLoading;

  SetLoadingForProfile(this.isLoading);
}

class SetProfileData {
  ModelUserProfile modelUserProfile;

  SetProfileData(this.modelUserProfile);
}
