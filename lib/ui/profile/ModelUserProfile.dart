class ModelUserProfile {
  String success;
  String status;
  String message;
  Data data;

  ModelUserProfile({this.success, this.status, this.message, this.data});

  ModelUserProfile.fromJson(Map<String, dynamic> json) {
    if(json == null){
      ModelUserProfile.initial();
    }else{
      success = json['success'];
      status = json['status'];
      message = json['message'];
      data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }

  factory ModelUserProfile.initial() {
    return new ModelUserProfile(
        status: null, data: null, message: null, success: null);
  }
}

class Data {
  String id;
  String profileImage;
  String name;
  String email;
  String startTime;
  String endTime;
  String phoneNumber;
  bool status;

  Data(
      {this.id,
        this.profileImage,
        this.name,
        this.email,
        this.startTime,
        this.endTime,
        this.phoneNumber,
        this.status});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    profileImage = json['profile_image'];
    name = json['name'];
    email = json['email'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    phoneNumber = json['phone_number'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['profile_image'] = this.profileImage;
    data['name'] = this.name;
    data['email'] = this.email;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['phone_number'] = this.phoneNumber;
    data['status'] = this.status;
    return data;
  }
}
