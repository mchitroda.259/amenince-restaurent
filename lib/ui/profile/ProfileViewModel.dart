import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:redux/redux.dart';

class ProfileViewModel {
  bool isLoading;
  ModelUserProfile modelUserProfile;

  ProfileViewModel(
      {this.isLoading,
        this.modelUserProfile});

    static ProfileViewModel fromStore(Store store) {
    return ProfileViewModel(
      isLoading: store.state.profileState.isLoading,
      modelUserProfile: store.state.profileState.modelUserProfile,
    );
  }
}
