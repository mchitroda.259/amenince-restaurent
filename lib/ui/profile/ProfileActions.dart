import 'dart:io';

import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:amen_inch/ui/registration/ModelUpdateProfile.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';
import 'package:http/http.dart' as http;

import 'ProfileReducer.dart';

ThunkAction getUserProfileAction(Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForProfile(true));
    new Future(() async {
      performGetUserProfile().then((dataModel) async {
        store.dispatch(new SetLoadingForProfile(false));
        if (dataModel.success == "1") {
          store.dispatch(SetProfileData(dataModel));
          PreferencesHelper.setUserProfile(dataModel.data.profileImage);
        } else {
          onFailure(dataModel.message.toString());
          print(dataModel.message.toString());
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForProfile(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<ModelUserProfile> performGetUserProfile() async {
  ApiProvider apiProvider = ApiProvider();

  Map body;

  body = {};

  final response = await apiProvider.doGet(
    APIEndpoints.GET_PROFILE,
    APIEndpoints.BASE_URL,
  );
  return ModelUserProfile.fromJson(response);
}

Future<dynamic> performUpdateWithImage(Store store, File file) async {
  ApiProvider apiProvider = ApiProvider();
  String token = await PreferencesHelper.tokenString;
  print('TOKEN :- ' + token);
  var uri = Uri.parse(APIEndpoints.BASE_URL + APIEndpoints.UPDATE_PROFILE_PIC);
  var request = http.MultipartRequest('POST', uri);

  Map<String, String> headers = {
    'Content-Type': 'multipart/form-data',
    'Authorization': 'Bearer $token',
    'Accept-Language': 'en',
  };

  request.headers.addAll(headers);

  await http.MultipartFile.fromPath('profile_image', file.path)
      .then((value) => request.files.add(value));

  final response = await apiProvider.doMultipartRequest(request);

  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ModelUpdateProfile.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}

ThunkAction updateAction(
    Store store,
    String title,
    String email,
    String phoneNumber,
    String startTime,
    String endTime,
    int status,
    Function onFailure) {
  return (Store store) async {
    new Future(() async {
      store.dispatch(new SetLoadingForProfile(true));
      performUpdate(
              store, title, email, phoneNumber, startTime, endTime, status)
          .then((loginModel) async {
        store.dispatch(new SetLoadingForProfile(false));
        if (loginModel is ModelUpdateProfile) {
          if (loginModel.success == "1") {
            onFailure(loginModel.message);
            PreferencesHelper.setUserName(loginModel.data.title);
            PreferencesHelper.setUserEmail(loginModel.data.email);

            NavigationHelper().pushScreenOnTop(Routes.MAIN_SCREEN);
          } else {
            onFailure(AppUtils.getLocalizedString("FAILED_TO_UPDATE_PROFILE"));
            print(loginModel.message.toString());
            store.dispatch(new SetLoadingForProfile(false));
          }
        } else {
          store.dispatch(new SetLoadingForProfile(false));
          if (loginModel.status == "401") {
            onFailure(loginModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(loginModel.message.toString());
            print(loginModel.message.toString());
          }
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForProfile(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performUpdate(Store store, String title, String email,
    String phoneNumber, String startTime, String endTime, int status) async {
  ApiProvider apiProvider = ApiProvider();
  Map body;
  body = {
    'title': title,
    'email': email,
    'phone_number': phoneNumber,
    'start_time': startTime,
    'end_time': endTime,
    'status': status.toString(),
  };

  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.UPDATE_PROFILE,
      body: body);
  ResponseModel responseModel = ResponseModel.fromJson(response);

  if (responseModel.status == "200")
    return ModelUpdateProfile.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}
