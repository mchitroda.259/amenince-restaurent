import 'dart:io';

import 'package:amen_inch/components/LoadingDialog.dart';

import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:amen_inch/utils/Validation.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dropdown.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:redux/redux.dart';

import 'ProfileActions.dart';
import 'ProfileViewModel.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State createState() {
    return Profile();
  }
}

class Profile extends State<ProfileScreen> {
  int _currentIndex = 0;
  String selectedImage = "";
  Store _store;

  TextEditingController resName = TextEditingController();

  TextEditingController startTimeController = TextEditingController();
  TextEditingController endTimeController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  FocusNode resNode = new FocusNode();
  FocusNode startNode = new FocusNode();
  FocusNode endNode = new FocusNode();
  FocusNode emailNode = new FocusNode();
  FocusNode phoneNode = new FocusNode();

  DateTime selectedDate = DateTime.now();

  Country selectedCountry = CountryPickerUtils.getCountryByIsoCode('IN');

  final _formKey = GlobalKey<FormState>();

  PickedFile _imageFile;
  bool isFromSelection = false;

  final ImagePicker _picker = ImagePicker();
  bool status = true;
  bool isToggle = false;
  bool isInit = false;

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
        maxWidth: 512,
        maxHeight: 512,
        imageQuality: 100,
      );

      _store.dispatch(performUpdateWithImage(_store, File(pickedFile.path)));

      setState(() {
        _imageFile = pickedFile;
        if (_imageFile != null)
          isFromSelection = true;
        else
          isFromSelection = false;
      });
    } catch (e) {
      setState(() {
        isFromSelection = false;
      });
    }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void didUpdateWidget(covariant ProfileScreen oldWidget) {
    // TODO: implement didUpdateWidget
    isInit = true;
    super.didUpdateWidget(oldWidget);
  }
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, ProfileViewModel>(
      converter: (store) => ProfileViewModel.fromStore(store),
      onInit: (store) => {
        this._store = store,
        isInit = true,
        store.dispatch(getUserProfileAction(onFailure)),
      },
      builder: (_, viewModel) => buildContent(viewModel, context),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  buildContent(ProfileViewModel viewModel, BuildContext context) {
    String title = "N/A";
    String startTime = "N/A";
    String endTime = "N/A";
    String emailAddress = "N/A";
    String phoneNumber = "N/A";

    if(isInit){
      if (viewModel != null) {
        if (viewModel.modelUserProfile != null) {
          if (viewModel.modelUserProfile.data != null) {
            resName.text = viewModel.modelUserProfile.data.name;
            startTimeController.text = viewModel.modelUserProfile.data.startTime;
            endTimeController.text = viewModel.modelUserProfile.data.endTime;
            emailController.text = viewModel.modelUserProfile.data.email;
            phoneController.text = viewModel.modelUserProfile.data.phoneNumber;
            selectedImage = viewModel.modelUserProfile.data.profileImage;
            if (!isToggle) status = viewModel.modelUserProfile.data.status;
          }
        }
      }
      isInit = false;
    }else{
      if (!isToggle) status = viewModel.modelUserProfile.data.status;
    }

    return UIHelper.getScreen(
      scaffold: Scaffold(
        appBar: UIHelper.getAppBar(
            AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            AppUtils.getLocalizedString("DETAILS"),
            16,
            false,
            context,
            "0",
            ""),
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: SingleChildScrollView(
              child: LoadingDialog(
            isLoading: viewModel.isLoading,
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                      /*  _onImageButtonPressed(ImageSource.gallery,
                            context: context);*/
                      },
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(top: 20),
                            width: 150,
                            height: 150,
                            padding: EdgeInsets.all(5.0),
                            child: !isFromSelection
                                ? CachedNetworkImage(
                                    imageUrl: selectedImage,
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                      width: 150.0,
                                      height: 150.0,
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                    placeholder: (context, url) =>
                                        CircularProgressIndicator(),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  )
                                : Container(
                                    width: 150,
                                    height: 150,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                          image:
                                              FileImage(File(_imageFile.path)),
                                          fit: BoxFit.fill),
                                    ),
                                  )),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: resName,
                        keyboardType: TextInputType.text,
                        focusNode: resNode,
                        enabled: true,
                        validator: (t) {
                          return Validation()
                              .validateString(t, Constants.ENTER_TITLE);
                        },
                        onFieldSubmitted: (v) {
                          FocusScope.of(context).requestFocus(phoneNode);
                        },
                        decoration: InputDecoration(
                          labelText: AppUtils.getLocalizedString("RES_NAME"),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(10),
                          alignment: Alignment.centerRight,
                          child: Text(AppUtils.getLocalizedString("STATUS"),
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w400)),
                        ),
                        FlutterSwitch(
                          width: 60.0,
                          height: 30.0,
                          valueFontSize: 10.0,
                          toggleSize: 25.0,
                          value: status,
                          borderRadius: 30.0,
                          activeToggleColor: Colors.white,
                          activeColor: Colors.green,
                          inactiveToggleColor: Color(0xFF2F363D),
                          padding: 4.0,
                          onToggle: (val) {
                            setState(() {
                              isToggle = true;
                              status = val;
                            });
                          },
                        ),
                      ],
                    ),
                    new InkWell(
                      onTap: () {
                        DatePicker.showTime12hPicker(context,
                            showTitleActions: true, onChanged: (date) {
                          print('change $date in time zone ' +
                              date.timeZoneOffset.inHours.toString());
                        }, onConfirm: (date) {
                          String formattedTime = DateFormat.jm().format(date);
                          print('confirm $formattedTime');
                          startTimeController.text = formattedTime;
                        }, currentTime: DateTime.now());
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          controller: startTimeController,
                          keyboardType: TextInputType.text,
                          focusNode: startNode,
                          enabled: false,
                          validator: (t) {},
                          onFieldSubmitted: (v) {
                            FocusScope.of(context).requestFocus(endNode);
                          },
                          decoration: InputDecoration(
                            labelText:
                                AppUtils.getLocalizedString("RES_START_TIME"),
                          ),
                        ),
                      ),
                    ),
                    new InkWell(
                      onTap: () {
                        DatePicker.showTime12hPicker(context,
                            showTitleActions: true, onChanged: (date) {
                          print('change $date in time zone ' +
                              date.timeZoneOffset.inHours.toString());
                        }, onConfirm: (date) {
                          String formattedTime = DateFormat.jm().format(date);
                          print('confirm $formattedTime');
                          endTimeController.text = formattedTime;
                        }, currentTime: DateTime.now());
                      },
                      child: Container(
                        padding: EdgeInsets.all(10),
                        child: TextFormField(
                          controller: endTimeController,
                          keyboardType: TextInputType.text,
                          focusNode: endNode,
                          enabled: false,
                          validator: (t) {},
                          onFieldSubmitted: (v) {
                            FocusScope.of(context).requestFocus(phoneNode);
                          },
                          decoration: InputDecoration(
                            labelText:
                                AppUtils.getLocalizedString("RES_END_TIME"),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: emailController,
                        keyboardType: TextInputType.text,
                        focusNode: emailNode,
                        enabled: true,
                        validator: (t) {
                          return Validation().validateEmail(t);
                        },
                        onFieldSubmitted: (v) {},
                        decoration: InputDecoration(
                          labelText:
                              AppUtils.getLocalizedString("EMAIL_ADDRESS"),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: TextFormField(
                        controller: phoneController,
                        keyboardType: TextInputType.text,
                        focusNode: phoneNode,
                        enabled: true,
                        validator: (t) {
                          return Validation().validatePhoneNumber(t);
                        },
                        onFieldSubmitted: (v) {},
                        decoration: InputDecoration(
                          labelText:
                              AppUtils.getLocalizedString("PHONE_NUMBER"),
                        ),
                      ),
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 10, right: 10, top: 20),
                        child: SizedBox(
                          height: 50,
                          child: UIHelper.getFlatButton(context,
                              backGroundColor: AppUtils.hexToColor(
                                  AppConfig.COLOR_SECONDARY),
                              buttonName: AppUtils.getLocalizedString("UPDATE"),
                              doOnPress: () {
                            if (_formKey.currentState.validate()) {
                              print(resName.text);
                              print(emailController.text);

                              int statusValue = status ? 1 : 0;
                              _store.dispatch(
                                updateAction(_store,resName.text, emailController.text, phoneController.text, startTimeController.text, endTimeController.text,statusValue, onFailure),
                              );
                            }
                          }),
                        )),
                  ],
                ),
              ),
            ),
          )),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
