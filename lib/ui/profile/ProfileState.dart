import 'package:amen_inch/ui/profile/ModelUserProfile.dart';
import 'package:flutter/material.dart';

class ProfileState {
  bool isLoading = false;
  ModelUserProfile modelUserProfile;

  ProfileState({@required this.isLoading,@required this.modelUserProfile});

  factory ProfileState.initial() {
    return new ProfileState(isLoading: false,modelUserProfile: null);
  }

  ProfileState copyWith({bool isLoading, ModelUserProfile modelUserProfile}) {
    return new ProfileState(
      isLoading: isLoading ?? this.isLoading,
      modelUserProfile: modelUserProfile ?? this.modelUserProfile,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'modelUserProfile': this.modelUserProfile,
    };
  }

  factory ProfileState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new ProfileState(isLoading: map['isLoading'] as bool,
      modelUserProfile: ModelUserProfile.fromJson(map['modelUserProfile']));
    } else
      return new ProfileState.initial();
  }
}
