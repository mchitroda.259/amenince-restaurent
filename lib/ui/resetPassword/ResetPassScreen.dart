import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:amen_inch/utils/Validation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:redux/redux.dart';

import 'ResetPassActions.dart';
import 'ResetPassViewModel.dart';

// ignore: must_be_immutable
class ResetPassScreen extends StatefulWidget {
  String phoneNumber, countryCode;

  ResetPassScreen(
      {Key key, @required this.phoneNumber, @required this.countryCode})
      : super(key: key);

  @override
  ResetPassScreenState createState() => ResetPassScreenState();
}

class ResetPassScreenState extends State<ResetPassScreen> {

  TextEditingController passwordController = TextEditingController();
  TextEditingController conPasswordController = TextEditingController();

  FocusNode passwordNode = new FocusNode();



  Store _store;

  TextEditingController currController = new TextEditingController();
  String otp;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, ResetPassViewModel>(
      converter: (store) => ResetPassViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
      },
    );
  }

  void matchOtp() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(AppUtils.getLocalizedString("PASS_RESET_SUCCESSFULLY")),
            content: Text(AppUtils.getLocalizedString("OTP_MATCHED_SUCCESS")),
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.check),
                  onPressed: () {
                    Navigator.of(context).pop();
                  })
            ],
          );
        });
  }

  buildContent(ResetPassViewModel viewModel, BuildContext context) {

    return UIHelper.getScreen(
      scaffold: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: UIHelper.getAppBar(AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            AppUtils.getLocalizedString("RESET_PASS"), 16, false, context, "", ""),
        backgroundColor: Color(0xFFeaeaea),
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: Container(
            color: Colors.white,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Divider(
                  color: Colors.black54,
                  height: 2,
                ),
                Container(
                  padding:
                      EdgeInsets.only(left: 10, right: 10, top: 20, bottom: 50),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 4.0, right: 16.0),
                        child: Text(
                          AppUtils.getLocalizedString("ENTER_YOUR_OTP_HERE"),
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 22.0,
                              fontWeight: FontWeight.normal),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                OTPTextField(
                  length: 4,
                  width: MediaQuery.of(context).size.width,
                  textFieldAlignment: MainAxisAlignment.spaceAround,
                  fieldWidth: 50,
                   obscureText: true,
                  fieldStyle: FieldStyle.underline,
                  style: TextStyle(fontSize: 22),
                  onChanged: (pin) {
                    print("Changed: " + pin);
                  },
                  onCompleted: (pin) {
                    print("Completed: " + pin);
                    otp = pin;
                  },
                ),

                Container(
                  margin: EdgeInsets.only(top: 20, bottom: 10,left: 10,right: 10),
                  padding: EdgeInsets.only(
                    left: 10,
                  ),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: new BorderRadius.circular(10.0)),
                  child: new TextFormField(
                    autofocus: false,
                    obscureText: true,
                    keyboardType: TextInputType.text,
                    controller: passwordController,
                    decoration: InputDecoration(
                      labelText: AppUtils.getLocalizedString("NEW_PASS"),
                      border: InputBorder.none,
                    ),
                    validator: (t) {
                      return Validation().validatePassword(t);
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10,left: 10,right: 10),
                  padding: EdgeInsets.only(
                    left: 10,
                  ),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      borderRadius: new BorderRadius.circular(10.0)),
                  child: new TextFormField(
                    autofocus: false,
                    obscureText: true,
                    keyboardType: TextInputType.text,
                    controller: conPasswordController,
                    decoration: InputDecoration(
                      labelText: AppUtils.getLocalizedString("CONFIRM_PASS"),
                      border: InputBorder.none,
                    ),
                    validator: (t) {
                      return Validation().validatePassword(t);
                    },
                  ),
                ),
                Container(
                  child: Container(
                      padding: EdgeInsets.only(left: 10, right: 10, top: 30),
                      child: SizedBox(
                        width: double.infinity,
                        height: 50,
                        child: UIHelper.getFlatButton(context,
                            backGroundColor:
                                AppUtils.hexToColor(AppConfig.COLOR_SECONDARY),
                            buttonName: AppUtils.getLocalizedString("VERIFY"), doOnPress: () {
                          _performVerifyOTP(viewModel);
                        }),
                      )),
                ),
                InkWell(
                  onTap: () {
                    _store.dispatch(sendOTPAction(_store, widget.phoneNumber,
                        widget.countryCode, onFailure));
                  },
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Text(
                      AppUtils.getLocalizedString("RESENT_OTP"),
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  Future<void> _performVerifyOTP(ResetPassViewModel viewModel) async {
    if (otp == null) {
      _showMessage(AppUtils.getLocalizedString("PLEASE_ENTER_VALID_OTP"));
      return;
    } else if (otp.isEmpty) {
      _showMessage(AppUtils.getLocalizedString("PLEASE_ENTER_VALID_OTP"));
      return;
    } else if (passwordController.text.toString() !=
        conPasswordController.text.toString()) {
      _showMessage(AppUtils.getLocalizedString("PASS_AND_CONFIRM_PASS_DOES_NOT_MATCH"));
      return;
    }

    viewModel.verifyOTP(widget.phoneNumber, widget.countryCode, otp,
        passwordController.text.trim(), onFailure);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
