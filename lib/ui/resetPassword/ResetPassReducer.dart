import 'package:redux/redux.dart';

import 'ResetPassState.dart';

final ResetPassReducer = combineReducers<ResetPassState>([
  TypedReducer<ResetPassState, SetLogin>(_setLogin),
  TypedReducer<ResetPassState, SetLoadingForLogin>(_setLoading),
  TypedReducer<ResetPassState, SetPerformOTPVerification>(_setPerformOTPVerification),
]);

ResetPassState _setLogin(ResetPassState state, SetLogin action) {
  return state.copyWith(isLoggedin: action.isLoggedin);
}

ResetPassState _setLoading(ResetPassState state, SetLoadingForLogin action) {
  return state.copyWith(isLoading: action.isLoading);
}

ResetPassState _setPerformOTPVerification(ResetPassState state, SetPerformOTPVerification action) {
  return state.copyWith(phoneNumber: action.phoneNumber,countryCode: action.countryCode,otp: action.otp);
}

class SetLoadingForLogin {
  bool isLoading;

  SetLoadingForLogin(this.isLoading);
}

class SetLogin {
  bool isLoggedin;

  SetLogin(this.isLoggedin);
}

class SetPerformOTPVerification {
  String  phoneNumber;
  String countryCode;
  String otp;

  SetPerformOTPVerification(this.phoneNumber,this.countryCode,this.otp);
}