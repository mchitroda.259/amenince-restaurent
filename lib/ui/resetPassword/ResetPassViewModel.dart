import 'package:amen_inch/ui/otpVerification/OTPVerificationModel.dart';
import 'package:redux/redux.dart';

import 'ResetPassActions.dart';

class ResetPassViewModel {
  bool isLoading, isLoggedIn;
  Function setOTPVerification;
  Function(String, String, String, String,Function) verifyOTP;
  String phoneNumber, countryCode,otp;
  OTPVerificationModel otpVerificationModel;

  ResetPassViewModel({this.isLoading,
    this.isLoggedIn,
    this.setOTPVerification,
    this.verifyOTP(String phoneNumber, String countryCode,String otp, String password,Function onFailure),
    this.phoneNumber,
    this.countryCode,
    this.otp,
    this.otpVerificationModel});

  static ResetPassViewModel fromStore(Store store) {
    return ResetPassViewModel(
        isLoading: store.state.resetPassState.isLoading,
        isLoggedIn: store.state.resetPassState.isLoggedin,
        setOTPVerification: () {
          store.dispatch(setOTPPassAction(true));
        },
        verifyOTP: (String phoneNumber, String countryCode,String otp, String password,Function onFailure) {
          store.dispatch(
              verifyOTPAction(store, phoneNumber, countryCode,otp,password, onFailure));
        },
        otpVerificationModel: store.state.resetPassState.otpVerificationModel);
  }
}
