import 'package:amen_inch/ui/otpVerification/OTPVerificationModel.dart';
import 'package:meta/meta.dart';

class ResetPassState {
  bool isLoading = false;
  bool isLoggedin = false;
  String phoneNumber = "";
  String countryCode = "";
  String otp = "";

  OTPVerificationModel otpVerificationModel;

  ResetPassState({
    @required this.isLoading,
    @required this.isLoggedin,
    @required this.otpVerificationModel,
    this.phoneNumber,
    this.countryCode,
    this.otp,
  });

  factory ResetPassState.initial() {
    return new ResetPassState(
        isLoading: false,
        isLoggedin: false,
        otpVerificationModel: null,
        phoneNumber: null,
        countryCode: null,
        otp: null);
  }

  ResetPassState copyWith(
      {bool isLoading,
        bool isLoggedin,
      OTPVerificationModel otpVerificationModel,
      String phoneNumber,
      String countryCode,
      String otp}) {
    return new ResetPassState(
      isLoading: isLoading ?? this.isLoading,
      isLoggedin: isLoggedin ?? this.isLoggedin,
      otpVerificationModel: otpVerificationModel ?? this.otpVerificationModel,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      countryCode: countryCode ?? this.countryCode,
      otp: otp ?? this.otp,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'isLoggedin': this.isLoggedin,
      'otpVerificationModel': this.otpVerificationModel,
      'phoneNumber': this.phoneNumber,
      'countryCode': this.countryCode,
      'otp': this.otp,
    };
  }

  factory ResetPassState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new ResetPassState(
        isLoading: map['isLoading'] as bool,
        isLoggedin: map['isLoggedin'] as bool,
        otpVerificationModel:
            OTPVerificationModel.fromJson(map['otpVerificationModel']),
        phoneNumber: map['phoneNumber'] as String,
        countryCode: map['countryCode'] as String,
        otp: map['otp'] as String,
      );
    } else
      return new ResetPassState.initial();
  }
}
