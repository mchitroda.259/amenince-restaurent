
class IntroState {
  IntroState();

  factory IntroState.initial() {
    return new IntroState();
  }

  IntroState copyWith() {
    return new IntroState();
  }

  Map<String, dynamic> toJson() {
    return {};
  }

  factory IntroState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new IntroState();
    } else
      return new IntroState.initial();
  }
}
