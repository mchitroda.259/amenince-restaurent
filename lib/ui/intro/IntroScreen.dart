
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:flutter/material.dart';

import 'package:intro_slider/dot_animation_enum.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';

class IntroScreen extends StatefulWidget {
  IntroScreen({Key key}) : super(key: key);

  @override
  IntroScreenState createState() => new IntroScreenState();
}

class IntroScreenState extends State<IntroScreen> {
  List<Slide> slides = new List();

  Function goToTab;

  @override
  void initState() {
    super.initState();
    slides.add(
      new Slide(
        title: "Discover 100+ restaurants near you \n Բացահայտեք Ձեզ մոտ գտնվող ավելի քան 100 ռեստորաններ:",
        styleTitle: TextStyle(
            color: Colors.black87,
            fontSize: 15.0,
            fontWeight: FontWeight.normal,
            fontFamily: 'RobotoMono'),
        description:
            "We make it simple to find the food you crave for. Satisfying your cravings, is our hobby. \n Մենք հեշտացրել ենք Ձեր սիրելի ուտելիքը գտնելու գործընթացը: Ձեր քաղցը հագեցնելը մեր հոբբին է:",
        styleDescription: TextStyle(
            color: Colors.black54,
            fontSize: 15.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Raleway'),
        pathImage: "assets/images/receive_the_order.jpg",
      ),
    );
    slides.add(
      new Slide(
        title: "Choose your favourite dishes \n Ընտրեք Ձեր սիրելի ուտեստը:",
        styleTitle: TextStyle(
            color: Colors.black87,
            fontSize: 15.0,
            fontWeight: FontWeight.normal,
            fontFamily: 'RobotoMono'),
        description: "Make your taste buds sizzle. Your favourite delicacies are just few clicks away. \n Արթնացրեք Ձեր համի զգայարանները: Ձեր նախընտրած ուտեստները՝ մի քանի քլիքով:",
        styleDescription: TextStyle(
            color: Colors.black54,
            fontSize: 15.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Raleway'),
        pathImage: "assets/images/choose_a_testi_dish.jpg",
      ),
    );
    slides.add(
      new Slide(
        title: "On-time delivery. \n 3. Առաքում ժամանակին:",
        styleTitle: TextStyle(
            color: Colors.black87,
            fontSize: 15.0,
            fontWeight: FontWeight.normal,
            fontFamily: 'RobotoMono'),
        description:
            "Fastest delivery of your favourite food at your doorstep, guaranteed. We value your time and money. \n Ձեր նախընտրած սննդի երաշխավորված ամենաարագ առաքումը Ձեր ընտրած հասցեով: Մենք գնահատում ենք Ձեր ժամանակն ու գումարը:",
        styleDescription: TextStyle(
            color: Colors.black54,
            fontSize: 15.0,
            fontStyle: FontStyle.normal,
            fontFamily: 'Raleway'),
        pathImage: "assets/images/pickup_or_delevery.jpg",
      ),
    );
  }

  void onDonePress() {
    PreferencesHelper.setIntroVisited(true);
    // Do what you want
    NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
  }

  void onTabChangeCompleted(index) {
    // Index of current tab is focused
    // Do what you want
  }

  Widget renderDoneBtn() {
    return new OutlineButton(
      child: new Text("Done",
          style: TextStyle(
              color: Colors.green,
              fontSize: 10.0,
              fontWeight: FontWeight.normal,
              fontFamily: 'RobotoMono')),
      onPressed: onDonePress,
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(10.0)),
      color: Colors.green,
    );
  }

  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = new List();
    for (int i = 0; i < slides.length; i++) {
      Slide currentSlide = slides[i];
      tabs.add(Container(
        width: double.infinity,
        height: double.infinity,
        child: Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(bottom: 10.0, top: 100.0),
          child: ListView(
            children: <Widget>[
              GestureDetector(
                  child: Image.asset(
                currentSlide.pathImage,
                width: 200.0,
                height: 300.0,
                fit: BoxFit.contain,
              )),
              Container(
                child: Text(
                  currentSlide.title,
                  style: currentSlide.styleTitle,
                  textAlign: TextAlign.center,
                ),
                margin: EdgeInsets.only(top: 50.0),
              ),
              Container(
                child: Text(
                  currentSlide.description,
                  style: currentSlide.styleDescription,
                  textAlign: TextAlign.center,
                  maxLines: 5,
                  overflow: TextOverflow.ellipsis,
                ),
                margin: EdgeInsets.only(top: 20.0, left: 20, right: 20),
              ),
            ],
          ),
        ),
      ));
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      // Done button
      renderDoneBtn: this.renderDoneBtn(),
      showDoneBtn: true,

      // List slides
      slides: this.slides,
      // Dot indicator
      colorDot: Colors.black87,
      sizeDot: 8.0,
      typeDotAnimation: dotSliderAnimation.DOT_MOVEMENT,

      // Tabs
      listCustomTabs: this.renderListCustomTabs(),
      backgroundColorAllSlides: Colors.white,
      refFuncGoToTab: (refFunc) {
        this.goToTab = refFunc;
      },

      // Show or hide status bar
      hideStatusBar: true,

      // On tab change completed
      onTabChangeCompleted: this.onTabChangeCompleted,
    );
  }
}
