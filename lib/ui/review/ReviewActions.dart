import 'package:amen_inch/apiservices/APIEndpoints.dart';
import 'package:amen_inch/apiservices/ApiProvider.dart';
import 'package:amen_inch/apiservices/ResponseModel.dart';
import 'package:amen_inch/ui/review/ReviewModel.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'package:redux/redux.dart';

import 'ReviewReducer.dart';


ThunkAction getReviewList(Function onFailure) {
  return (Store store) async {
    store.dispatch(new SetLoadingForReview(true));
    new Future(() async {
      performGetReviewList().then((dataModel) async {
        store.dispatch(new SetLoadingForReview(false));
        if (dataModel is ReviewModel) {
          if (dataModel.success == "1") {
            store.dispatch(SetReviewModel(dataModel));
          } else {
            dataModel.data = null;
            store.dispatch(SetReviewModel(dataModel));
            onFailure("No result found!");
            print(dataModel.message.toString());
          }
        } else {
          if (dataModel.status == "401") {
            onFailure(dataModel.message.toString());
            PreferencesHelper.setAuthenticated(true);
            PreferencesHelper.setToken("");
            NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
          } else {
            onFailure(dataModel.message.toString());
            print(dataModel.message.toString());
          }
        }
      }, onError: (error) async {
        store.dispatch(new SetLoadingForReview(false));
        if (error.errorCode.toString() == "401") {
          onFailure(AppUtils.getLocalizedString("SESSION_EXPIRED"));

          PreferencesHelper.setAuthenticated(true);
          PreferencesHelper.setToken("");
          NavigationHelper().pushScreenOnTop(Routes.LOGIN_SCREEN);
        } else {
          onFailure(error.toString());
          print(error.toString());
        }
      });
    });
  };
}

Future<dynamic> performGetReviewList() async {
  ApiProvider apiProvider = ApiProvider();


  Map body;

  body = {};


  final response = await apiProvider.doPostWithToken(
      APIEndpoints.BASE_URL, APIEndpoints.GET_REVIEWS,
      body: body);


  ResponseModel responseModel = ResponseModel.fromJson(response);
  if (responseModel.status == "200")
    return ReviewModel.fromJson(response);
  else
    return ResponseModel.fromJson(response);
}