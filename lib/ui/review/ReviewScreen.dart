import 'package:amen_inch/components/LoadingDialog.dart';
import 'package:amen_inch/ui/orderList/OrderListModel.dart';
import 'package:amen_inch/ui/review/ReviewActions.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/widgets/ReviewList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/utils/UIHelpers.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/src/store.dart';

import 'ReviewViewModel.dart';

class ReviewScreen extends StatefulWidget {
  String orderId = '', searchKey = '';
  Review review;

  ReviewScreen(
      {Key key,
      @required this.orderId,
      @required this.searchKey,
      @required this.review})
      : super(key: key);

  @override
  State createState() {
    return Reviews();
  }
}

class Reviews extends State<ReviewScreen> {

  Store _store;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, ReviewViewModel>(
      converter: (store) => ReviewViewModel.fromStore(store),
      builder: (_, viewModel) => buildContent(viewModel, context),
      onInit: (store) => {
        _store = store,
        _store.dispatch(getReviewList(onFailure)),
      },
    );
  }

  buildContent(ReviewViewModel viewModel, BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    final subView = <Widget>[];

    subView.add(new ReviewList(
      reviews: viewModel.reviewModel,
      store: _store,
    ));

    return UIHelper.getScreen(
      scaffold: Scaffold(
        body: LoadingDialog(
          isLoading: viewModel.isLoading,
          child: SingleChildScrollView(
              child: new Stack(
                children: <Widget>[
                  new Container(child: new Column(children: subView)),
                ],
              )),
        ),
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
