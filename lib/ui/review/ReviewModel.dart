class ReviewModel {
  String success;
  String status;
  String message;
  List<Data> data;

  ReviewModel({this.success, this.status, this.message, this.data});

  ReviewModel.fromJson(Map<String, dynamic> json) {
    if(json == null){
      ReviewModel.initial();
    }else{
      success = json['success'];
      status = json['status'];
      message = json['message'];
      if (json['data'] != null) {
        data = new List<Data>();
        json['data'].forEach((v) {
          data.add(new Data.fromJson(v));
        });
      }
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }

  factory ReviewModel.initial() {
    return new ReviewModel(
        status: null, data: null, message: null, success: null);
  }
}

class Data {
  String id;
  String name;
  String rating;
  String review;
  String date;

  Data({this.id, this.name, this.rating, this.review, this.date});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    rating = json['rating'];
    review = json['review'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['rating'] = this.rating;
    data['review'] = this.review;
    data['date'] = this.date;
    return data;
  }
}
