import 'package:amen_inch/ui/detail/ModelAddToCart.dart';
import 'package:flutter/material.dart';

import 'ReviewModel.dart';

class ReviewState {
  bool isLoading = false;
  ReviewModel reviewModel;

  ReviewState({@required this.isLoading,@required this.reviewModel});

  factory ReviewState.initial() {
    return new ReviewState(isLoading: false,reviewModel: null);
  }

  ReviewState copyWith({bool isLoading,ReviewModel reviewModel,ModelAddToCart modelAddToCart,String cartCount}) {
    return new ReviewState(
      isLoading: isLoading ?? this.isLoading,
        reviewModel: reviewModel ?? this.reviewModel
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'isLoading': this.isLoading,
      'reviewModel': this.reviewModel,
    };
  }

  factory ReviewState.fromJson(Map<String, dynamic> map) {
    if (map != null) {
      return new ReviewState(isLoading: map['isLoading'] as bool,
        reviewModel: ReviewModel.fromJson(map['reviewModel']),);
    } else
      return new ReviewState.initial();
  }
}
