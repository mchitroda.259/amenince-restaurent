import 'package:amen_inch/ui/review/ReviewModel.dart';
import 'package:redux/redux.dart';

import 'ReviewState.dart';

final reviewReducer = combineReducers<ReviewState>([
  TypedReducer<ReviewState, SetLoadingForReview>(_setLoading),
  TypedReducer<ReviewState, SetReviewModel>(_setReviewList),
]);

ReviewState _setLoading(ReviewState state, SetLoadingForReview action) {
  return state.copyWith(isLoading: action.isLoading);
}

ReviewState _setReviewList(ReviewState state, SetReviewModel action) {
  return state.copyWith(reviewModel: action.reviewModel);
}

class SetReviewModel {
  ReviewModel reviewModel;

  SetReviewModel(this.reviewModel);
}

class SetLoadingForReview {
  bool isLoading;

  SetLoadingForReview(this.isLoading);
}
