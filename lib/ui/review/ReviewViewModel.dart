import 'package:redux/redux.dart';

import 'ReviewModel.dart';

class ReviewViewModel {
  bool isLoading;
  ReviewModel reviewModel;

  ReviewViewModel({this.isLoading,
    this.reviewModel,});

  static ReviewViewModel fromStore(Store store) {
    return ReviewViewModel(
      isLoading: store.state.reviewState.isLoading,
      reviewModel: store.state.reviewState.reviewModel,
    );
  }
}
