import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class LoadingDialog extends StatelessWidget {
  final Widget child;
  final bool isLoading;

  LoadingDialog({this.child, this.isLoading});

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
        color: Colors.white,
        opacity: 0.6,
        progressIndicator: CircularProgressIndicator(),
        inAsyncCall: isLoading,
        child: child);
  }
}
