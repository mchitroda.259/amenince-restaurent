import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:amen_inch/utils/AppUtils.dart';

class CustomTextView extends StatelessWidget {
  final String text;
  final dynamic color;
  final FontWeight fontWeight;
  final TextAlign textAlign;
  final double fontSize;
  final TextOverflow overflow;
  final int maxLine;

  CustomTextView(
      {this.text,
      this.color,
      this.fontWeight,
      this.textAlign,
      this.fontSize,
      this.overflow,
      this.maxLine});

  @override
  Widget build(BuildContext context) {
    return new Text(text,
        textAlign: textAlign,
        overflow: overflow,
        maxLines: maxLine == null ? 1 : maxLine,
        style: TextStyle(
          fontSize: fontSize == null ? 16.0 : fontSize,
          fontWeight: fontWeight,
          color: color == null
              ? null
              : color is Color ? color : AppUtils.hexToColor(color) ?? null,
        ));
  }
}
