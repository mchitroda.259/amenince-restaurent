
class ModelNotification {
  String id, msg, type, title, name, currentQty;

  ModelNotification(
      {this.id, this.msg, this.type, this.title, this.name, this.currentQty});

  Map<String, dynamic> toMap() {
    return {
      'entity_id': this.id,
      'message': this.msg,
      'type': this.type,
      'title': this.title,
      'Name': this.name,
      'Current Qty': this.name,
    };
  }

  factory ModelNotification.fromMap(Map<dynamic, dynamic> map) {
    return new ModelNotification(
      id: map['entity_id'] as String,
      msg: map['message'] as String,
      type: map['type'] as String,
      title: map['title'] as String,
      name: map['Name'] as String,
      currentQty: map['Current Qty'] as String,
    );
  }
}
