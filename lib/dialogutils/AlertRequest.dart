import 'package:amen_inch/apiservices/APIException.dart';

class AlertRequest {
  final String type;
  final String title;
  final String message;
  final String okButton;
  final String cancelButton;
  final APIException exception;

  AlertRequest(this.type, this.title, this.message,
      {this.okButton, this.cancelButton, this.exception});
}
