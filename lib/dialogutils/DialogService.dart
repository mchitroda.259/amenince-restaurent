import 'dart:async';

import 'package:amen_inch/apiservices/APIException.dart';

import 'AlertRequest.dart';
import 'AlertResponse.dart';

class DialogService {
  Function(AlertRequest) _showDialogListener;
  Completer<AlertResponse> _dialogCompleter;

  /// Registers a callback function. Typically to show the dialog
  void registerDialogListener(Function(AlertRequest) showDialogListener) {
    _showDialogListener = showDialogListener;
  }

  /// Calls the dialog listener and returns a Future that will wait for dialogComplete.
  Future<AlertResponse> showDialog(String type, String title, String message,
      {String okButton, String cancelButton, APIException e}) {
    _dialogCompleter = Completer<AlertResponse>();
    _showDialogListener(AlertRequest(
      type,
      title,
      message,
      okButton: okButton,
      cancelButton: cancelButton,
    ));
    return _dialogCompleter.future;
  }

  Future<AlertResponse> showErrorDialog(String type, APIException e,
      {String okButton, String cancelButton}) {
    _dialogCompleter = Completer<AlertResponse>();
    _showDialogListener(AlertRequest(type, '', '',
        okButton: okButton, cancelButton: cancelButton, exception: e));
    return _dialogCompleter.future;
  }

  /// Completes the _dialogCompleter to resume the Future's execution call
  void dialogComplete(AlertResponse response) {
    _dialogCompleter.complete(response);
//    _dialogCompleter = null;
  }
}
