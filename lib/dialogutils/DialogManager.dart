import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';

import 'AlertRequest.dart';
import 'AlertResponse.dart';
import 'DialogService.dart';

class DialogManager extends StatefulWidget {
  final Widget child;

  DialogManager({Key key, this.child}) : super(key: key);

  _DialogManagerState createState() => _DialogManagerState();
}

class _DialogManagerState extends State<DialogManager> {
  DialogService _dialogService = NavigationHelper.locator<DialogService>();

  @override
  void initState() {
    super.initState();
    _dialogService.registerDialogListener(_showDialog);
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  Future<void> _showDialog(AlertRequest alertRequest) async {
    switch (alertRequest.type) {
      case 'single':
        {
          if (Platform.isIOS) {
            await showCupertinoDialog(
              context: context,
              builder: (context) => new CupertinoAlertDialog(
                title: new Text(alertRequest.title),
                content: new Text(alertRequest.message),
                actions: <Widget>[
                  new CupertinoDialogAction(
                    onPressed: () {
                      Navigator.of(context).pop(false);
                      _dialogService
                          .dialogComplete(AlertResponse(confirmed: true));
                    },
                    child: new Text(
                      alertRequest.okButton ?? Constants.OK.toUpperCase(),
                      style: TextStyle(
                          color: AppUtils.hexToColor(
                              AppConfig.COLOR_SECONDARY_DARK)),
                    ),
                    isDefaultAction: true,
                  ),
                ],
              ),
            );
          } else {
            await showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                title: new Text(alertRequest.title),
                content: new Text(alertRequest.message),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(false);
                      _dialogService
                          .dialogComplete(AlertResponse(confirmed: false));
                    },
                    child: new Text(
                      alertRequest.okButton ?? Constants.OK.toUpperCase(),
                      style: TextStyle(
                          color: AppUtils.hexToColor(
                              AppConfig.COLOR_SECONDARY_DARK)),
                    ),
                  ),
                ],
              ),
            );
          }
          break;
        }
      case 'double':
        {
          if (Platform.isIOS) {
            await showCupertinoDialog(
              context: context,
              builder: (context) => new CupertinoAlertDialog(
                title: new Text(alertRequest.title),
                content: new Text(alertRequest.message),
                actions: <Widget>[
                  new CupertinoDialogAction(
                    isDefaultAction: true,
                    onPressed: () {
                      Navigator.of(context).pop(false);
                      _dialogService
                          .dialogComplete(AlertResponse(confirmed: false));
                    },
                    child: new Text(
                      alertRequest.cancelButton ??
                          Constants.CANCEL.toUpperCase(),
                      /*style: TextStyle(
                    color:
                        AppUtils.hexToColor(AppConfig.COLOR_SECONDARY_DARK)),*/
                    ),
                  ),
                  new CupertinoDialogAction(
                    onPressed: () {
                      Navigator.of(context).pop(false);
                      _dialogService
                          .dialogComplete(AlertResponse(confirmed: true));
                    },
                    child: new Text(
                      alertRequest.okButton ?? Constants.OK.toUpperCase(),
                      /*style: TextStyle(
                    color:
                        AppUtils.hexToColor(AppConfig.COLOR_SECONDARY_DARK)),*/
                    ),
                  ),
                ],
              ),
            );
          } else {
            await showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                title: new Text(alertRequest.title),
                content: new Text(alertRequest.message),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: new Text(
                      alertRequest.cancelButton ??
                          Constants.CANCEL.toUpperCase(),
                      style: TextStyle(
                          color: AppUtils.hexToColor(
                              AppConfig.COLOR_SECONDARY_DARK)),
                    ),
                  ),
                  new FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(false);
                    },
                    child: new Text(
                      alertRequest.okButton ?? Constants.OK.toUpperCase(),
                      style: TextStyle(
                          color: AppUtils.hexToColor(
                              AppConfig.COLOR_SECONDARY_DARK)),
                    ),
                  ),
                ],
              ),
            );
          }
          break;
        }
      case 'server_error':
        {
          String title = 'Alert!';
          String message = 'Oops! Something went wrong, please try again.';
          switch (alertRequest.exception.errorCode) {
            case 408:
              {
                title = 'Connection Timeout';
                message =
                    'Oops, It seems your internet is too slow to communicate with application server. Please try again after sometime.';
                break;
              }
            case 12163:
              {
                title = 'No Internet Connection';
                message =
                    'Something\'s not right with your internet connection.';
                break;
              }
            case 400:
              {
                title = 'No Internet Connection';
                message =
                    'Something\'s not right with your internet connection.';
                break;
              }
            case 404:
              {
                message = 'Oops! Something went wrong, please try again.';
                break;
              }
            case 500:
              {
                message =
                    'The server encountered an unexpected condition which prevented it from fulfilling the request.';
                break;
              }
            case 503:
              {
                message =
                    'Service Unavailable. The server is currently unable to handle the request due to a temporary overloading or maintenance of the server.';
                break;
              }
          }
          if (Platform.isIOS) {
            await showCupertinoDialog(
              context: context,
              builder: (context) => new CupertinoAlertDialog(
                title: new Text(title),
                content: new Text(message),
                actions: <Widget>[
                  new CupertinoDialogAction(
                    onPressed: () {
                      Navigator.of(context).pop(false);
                      _dialogService
                          .dialogComplete(AlertResponse(confirmed: true));
                    },
                    child: new Text(
                      alertRequest.okButton ?? Constants.OK.toUpperCase(),
                      /*style: TextStyle(
                    color:
                        AppUtils.hexToColor(AppConfig.COLOR_SECONDARY_DARK)),*/
                    ),
                  ),
                ],
              ),
            );
          } else {
            await showDialog(
              context: context,
              builder: (context) => new AlertDialog(
                title: new Text(title),
                content: new Text(message),
                actions: <Widget>[
                  new FlatButton(
                    onPressed: () {
                      Navigator.of(context).pop(false);
                      _dialogService
                          .dialogComplete(AlertResponse(confirmed: true));
                    },
                    child: new Text(
                      alertRequest.okButton ?? Constants.OK.toUpperCase(),
                      style: TextStyle(
                          color: AppUtils.hexToColor(
                              AppConfig.COLOR_SECONDARY_DARK)),
                    ),
                  ),
                ],
              ),
            );
          }
          break;
        }
    }
  }
}
