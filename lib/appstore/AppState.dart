
import 'package:amen_inch/ui/detail/FoodDetailState.dart';
import 'package:amen_inch/ui/forgotpass/ForgotPassState.dart';
import 'package:amen_inch/ui/home/HomeState.dart';
import 'package:amen_inch/ui/language/LanguageState.dart';
import 'package:amen_inch/ui/login/LoginState.dart';
import 'package:amen_inch/ui/menuItemDetails/MenuDetailState.dart';
import 'package:amen_inch/ui/orderList/OrderListState.dart';
import 'package:amen_inch/ui/otpVerification/OTPPassState.dart';
import 'package:amen_inch/ui/profile/ProfileState.dart';
import 'package:amen_inch/ui/registration/RegisterState.dart';
import 'package:amen_inch/ui/resetPassword/ResetPassState.dart';
import 'package:amen_inch/ui/menuList/MenuListState.dart';
import 'package:amen_inch/ui/review/ReviewState.dart';
import 'package:amen_inch/ui/splash/SplashState.dart';

class AppState {
  LoginState loginState;
  SplashState splashState;
  HomeState homeState;
  OTPState otpState;
  RegisterState registerState;
  ProfileState profileState;
  FoodDetailState foodDetailState;
  MenuDetailState menuDetailState;
  MenuListState menuListState;
  OrderListState orderListState;
  ForgotPassState forgotPassState;
  LanguageState languageState;
  ResetPassState resetPassState;
  ReviewState reviewState;

  AppState(
      {this.loginState,
      this.splashState,
      this.homeState,
      this.otpState,
      this.registerState,
      this.profileState,
      this.foodDetailState,
      this.menuDetailState,
      this.menuListState,
      this.orderListState,
      this.forgotPassState,
      this.languageState,
      this.reviewState,
      this.resetPassState});

  static AppState fromJson(dynamic json) {
    return new AppState(
      loginState: json == null
          ? LoginState.initial()
          : LoginState.fromJson(
              json['loginState'],
            ),
      homeState: json == null
          ? HomeState.initial()
          : HomeState.fromJson(json['homeState']),
      otpState: json == null
          ? OTPState.initial()
          : OTPState.fromJson(json['otpState']),
      registerState: json == null
          ? RegisterState.initial()
          : RegisterState.fromJson(json['registerState']),
      profileState: json == null
          ? ProfileState.initial()
          : ProfileState.fromJson(json['profileState']),
      foodDetailState: json == null
          ? FoodDetailState.initial()
          : FoodDetailState.fromJson(json['foodDetailState']),
      menuDetailState: json == null
          ? MenuDetailState.initial()
          : MenuDetailState.fromJson(json['menuDetailState']),
      menuListState: json == null
          ? MenuListState.initial()
          : MenuListState.fromJson(json['restaurantState']),
      splashState: json == null
          ? SplashState.initial()
          : SplashState.fromJson(json['splashState']),
      orderListState: json == null
          ? OrderListState.initial()
          : OrderListState.fromJson(json['orderListState']),
      forgotPassState: json == null
          ? ForgotPassState.initial()
          : ForgotPassState.fromJson(json['forgotPassState']),
      languageState: json == null
          ? LanguageState.initial()
          : LanguageState.fromJson(json['languageState']),
      reviewState: json == null
          ? ReviewState.initial()
          : ReviewState.fromJson(json['reviewState']),
      resetPassState: json == null
          ? ResetPassState.initial()
          : ResetPassState.fromJson(json['resetPassState']),
    );
  }

  dynamic toJson() => {
        'loginState': this.loginState.toJson(),
        'splashState': this.splashState.toJson(),
        'homeState': this.homeState.toJson(),
        'otpState': this.otpState.toJson(),
        'registerState': this.registerState.toJson(),
        'profileState': this.profileState.toJson(),
        'foodDetailState': this.foodDetailState.toJson(),
        'menuDetailState': this.menuDetailState.toJson(),
        'restaurantState': this.menuListState.toJson(),
        'orderListState': this.orderListState.toJson(),
        'forgotPassState': this.forgotPassState.toJson(),
        'languageState': this.languageState.toJson(),
        'reviewState': this.reviewState.toJson(),
        'resetPassState': this.resetPassState.toJson(),
      };

  factory AppState.initial() {
    return new AppState(
      loginState: LoginState.initial(),
      splashState: SplashState.initial(),
      homeState: HomeState.initial(),
      otpState: OTPState.initial(),
      registerState: RegisterState.initial(),
      profileState: ProfileState.initial(),
      foodDetailState: FoodDetailState.initial(),
      menuDetailState: MenuDetailState.initial(),
      menuListState: MenuListState.initial(),
      orderListState: OrderListState.initial(),
      forgotPassState: ForgotPassState.initial(),
      languageState: LanguageState.initial(),
      reviewState: ReviewState.initial(),
      resetPassState: ResetPassState.initial(),
    );
  }

  AppState copyWith(
      {LoginState loginState,
      SplashState splashState,
      HomeState homeState,
      OTPState otpState,
      RegisterState registerState,
      FoodDetailState foodDetailState,
      MenuDetailState menuDetailState,
      MenuListState restaurantState,
      OrderListState orderListState,
      ForgotPassState forgotPassState,
      LanguageState languageState,
      ReviewState reviewState,
      ResetPassState resetPassState}) {
    return new AppState(
      loginState: loginState ?? this.loginState,
      splashState: splashState ?? this.splashState,
      homeState: homeState ?? this.homeState,
      otpState: otpState ?? this.otpState,
      registerState: registerState ?? this.registerState,
      foodDetailState: foodDetailState ?? this.foodDetailState,
      menuDetailState: menuDetailState ?? this.menuDetailState,
      menuListState: restaurantState ?? this.menuListState,
      orderListState: orderListState ?? this.orderListState,
      forgotPassState: forgotPassState ?? this.forgotPassState,
      languageState: languageState ?? this.languageState,
      reviewState: reviewState ?? this.reviewState,
      resetPassState: resetPassState ?? this.resetPassState,
    );
  }
}
