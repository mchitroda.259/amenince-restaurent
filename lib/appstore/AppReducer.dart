
import 'package:amen_inch/ui/detail/FoodDetailReducer.dart';
import 'package:amen_inch/ui/favorites/FavReducer.dart';
import 'package:amen_inch/ui/forgotpass/ForgotPassReducer.dart';
import 'package:amen_inch/ui/home/HomeReducer.dart';
import 'package:amen_inch/ui/language/LanguageReducer.dart';
import 'package:amen_inch/ui/login/LoginReducer.dart';
import 'package:amen_inch/ui/menuItemDetails/MenuDetailReducer.dart';
import 'package:amen_inch/ui/orderList/OrderListReducer.dart';
import 'package:amen_inch/ui/otpVerification/OTPReducer.dart';
import 'package:amen_inch/ui/profile/ProfileReducer.dart';
import 'package:amen_inch/ui/registration/RegistrerReducer.dart';
import 'package:amen_inch/ui/resetPassword/ResetPassReducer.dart';
import 'package:amen_inch/ui/menuList/MenuListReducer.dart';
import 'package:amen_inch/ui/review/ReviewReducer.dart';
import 'package:amen_inch/ui/splash/SplashReducer.dart';

import 'AppState.dart';

AppState reducer(AppState previousState, dynamic action) {
  return AppState(
      loginState: loginReducer(previousState.loginState, action),
      splashState: splashReducer(previousState.splashState, action),
      homeState: homeReducer(previousState.homeState, action),
      otpState: OTPReducer(previousState.otpState, action),
      registerState: registerReducer(previousState.registerState, action),
      profileState: profileReducer(previousState.profileState, action),
      foodDetailState: foodDetailReducer(previousState.foodDetailState, action),
      menuDetailState: menuDetailReducer(previousState.menuDetailState, action),
      menuListState: menuListReducer(previousState.menuListState, action),
      orderListState: orderListReducer(previousState.orderListState, action),
      forgotPassState: forgotPassReducer(previousState.forgotPassState, action),
      languageState: languageReducer(previousState.languageState, action),
      reviewState: reviewReducer(previousState.reviewState, action),
      resetPassState: ResetPassReducer(previousState.resetPassState, action));
}
