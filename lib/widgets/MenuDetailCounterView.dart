import 'package:amen_inch/ui/menuItemDetails/MenuDetailReducer.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class MenuDetailCounterView extends StatefulWidget {
  int counter;
  Store store;
  String productId;
  bool isFromState = false;

  MenuDetailCounterView(
      {Key key,
      @required this.counter,
      @required this.productId,
      @required this.store,
      @required isFromState})
      : super(key: key);

  @override
  _MenuDetailCounterViewState createState() => _MenuDetailCounterViewState();
}

class _MenuDetailCounterViewState extends State<MenuDetailCounterView> {
  int _n = 0;

  void add() {
    setState(() {
      widget.isFromState = true;
      _n++;
      widget.store.dispatch(UpdateCartCount(_n));
    });
  }

  void minus() {
    setState(() {
      widget.isFromState = true;
      if (_n != 0) _n--;
      widget.store.dispatch(UpdateCartCount(_n));
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    widget.isFromState = false;
    _n = widget.counter;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Wrap(
      children: [
        new Container(
          width: 105,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black),
              borderRadius: new BorderRadius.circular(10.0)),
          child: new Center(
            child: new Row(
              children: <Widget>[
                InkWell(
                  splashColor: Colors.white, // inkwell color
                  child: SizedBox(
                      width: 30,
                      height: 25,
                      child: Icon(
                        Icons.remove,
                        size: 20,
                      )),
                  onTap: minus,
                ),
                new Container(
                  color: Colors.white,
                  alignment: Alignment.center,
                  height: 25,
                  width: 40,
                  padding: EdgeInsets.only(left: 2, right: 2),
                  child: new Text('$_n', style: new TextStyle(fontSize: 14.0)),
                ),
                InkWell(
                  splashColor: Colors.white, // inkwell color
                  child: SizedBox(
                      width: 30,
                      height: 25,
                      child: Icon(
                        Icons.add,
                        size: 20,
                      )),
                  onTap: add,
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
