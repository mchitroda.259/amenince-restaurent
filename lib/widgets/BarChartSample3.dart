import 'package:amen_inch/ui/home/HomeModel.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class BarChartSample3 extends StatefulWidget {
  HomeModel homeModel;

  BarChartSample3({Key key, @required this.homeModel}) : super(key: key);

  @override
  State<StatefulWidget> createState() => BarChartSample3State();
}

class BarChartSample3State extends State<BarChartSample3> {
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.7,
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        color: const Color(0xff2c4260),
        child: BarChart(
          BarChartData(
            alignment: BarChartAlignment.spaceAround,
            maxY: 20,
            barTouchData: BarTouchData(
              enabled: false,
              touchTooltipData: BarTouchTooltipData(
                tooltipBgColor: Colors.transparent,
                tooltipPadding: const EdgeInsets.all(0),
                tooltipMargin: 8,
                getTooltipItem: (
                  BarChartGroupData group,
                  int groupIndex,
                  BarChartRodData rod,
                  int rodIndex,
                ) {
                  return BarTooltipItem(
                    rod.y.round().toString(),
                    TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  );
                },
              ),
            ),
            titlesData: FlTitlesData(
              show: true,
              bottomTitles: SideTitles(
                showTitles: true,
                getTextStyles: (value) => const TextStyle(
                    color: Color(0xff7589a2),
                    fontWeight: FontWeight.bold,
                    fontSize: 14),
                margin: 20,
                getTitles: (double value) {
                  for (int i = 0;
                      i < widget.homeModel.data.visitorsAnalytics.length;
                      i++) {
                    if (value.toInt() == i) {
                      return widget.homeModel.data.visitorsAnalytics[i].day;
                    }
                  }
                  /*switch (value.toInt()) {
                    case 0:
                      return 'Mn';
                    case 1:
                      return 'Te';
                    case 2:
                      return 'Wd';
                    case 3:
                      return 'Tu';
                    case 4:
                      return 'Fr';
                    case 5:
                      return 'St';
                    case 6:
                      return 'Sn';
                    default:
                      return '';
                  }*/
                },
              ),
              leftTitles: SideTitles(showTitles: false),
            ),
            borderData: FlBorderData(
              show: false,
            ),
            barGroups: getBarDate(widget.homeModel),
          ),
        ),
      ),
    );
  }
}

List<BarChartGroupData> getBarDate(HomeModel homeModel) {
  final barGroups = <BarChartGroupData>[];
  for (int i = 0; i < homeModel.data.visitorsAnalytics.length; i++) {
    barGroups.add(BarChartGroupData(
      x: 3,
      barRods: [
        BarChartRodData(
            y: /*homeModel.data.visitorsAnalytics[i].visitors.toDouble()*/5,
            colors: [Colors.lightBlueAccent, Colors.greenAccent])
      ],
      showingTooltipIndicators: [0],
    ));
  }
  return barGroups;
}
