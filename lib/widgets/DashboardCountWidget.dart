import 'package:amen_inch/animation/ScaleRoute.dart';
import 'package:amen_inch/widgets/FoodOrderPage.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

class DashboardCountWidget extends StatefulWidget {
  String title, count;

  Color color;

  DashboardCountWidget(
      {Key key,
      @required this.title,
      @required this.count,
      @required this.color})
      : super(key: key);

  @override
  _DashboardCountWidgetState createState() => _DashboardCountWidgetState();
}

class _DashboardCountWidgetState extends State<DashboardCountWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration:   BoxDecoration(
          color: widget.color,
            borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFfae3e2),
              blurRadius: 95.0,
              offset: Offset(0.0, 0.90),
            ),
          ]),
        child:  Center(
          child: Column(
            mainAxisAlignment:
            MainAxisAlignment.center,
            crossAxisAlignment:
            CrossAxisAlignment.center,
            children: [
              Text(widget.title,
                  maxLines: 1,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.bold)),
              Text(widget.count,
                  maxLines: 1,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold)),
            ],
          ),
        ),
      )
    );
  }
}
