import 'package:amen_inch/ui/home/HomeModel.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class InitialHintAnimation extends StatelessWidget {
  final bool animate;
  final HomeModel homeModel;

  InitialHintAnimation(this.homeModel, {this.animate});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300.0,
      child: new charts.BarChart(
        _createSampleData(homeModel),
        animate: animate,
        // Optionally turn off the animation that animates values up from the
        // bottom of the domain axis. If animation is on, the bars will animate up
        // and then animate to the final viewport.
        animationDuration: Duration.zero,
        // Set the initial viewport by providing a new AxisSpec with the
        // desired viewport: a starting domain and the data size.
        domainAxis: new charts.OrdinalAxisSpec(
            viewport: new charts.OrdinalViewport('2018', 5)),
        behaviors: [
          // Add this behavior to show initial hint animation that will pan to the
          // final desired viewport.
          // The duration of the animation can be adjusted by pass in
          // [hintDuration]. By default this is 3000ms.
          new charts.InitialHintBehavior(maxHintTranslate: 4.0),
          // Optionally add a pan or pan and zoom behavior.
          // If pan/zoom is not added, the viewport specified remains the viewport
          new charts.PanAndZoomBehavior(),
        ],
      ),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<OrdinalSales, String>> _createSampleData(
      HomeModel homeModel) {
    final data = <OrdinalSales>[];

    for (int i = 0; i < homeModel.data.visitorsAnalytics.length; i++) {
      data.add(
        new OrdinalSales(homeModel.data.visitorsAnalytics[i].day,
            homeModel.data.visitorsAnalytics[i].visitors),
      );
    }

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}

/// Sample ordinal data type.
class OrdinalSales {
  final String year;
  final int sales;

  OrdinalSales(this.year, this.sales);
}
