import 'package:amen_inch/animation/ScaleRoute.dart';
import 'package:amen_inch/ui/orderList/OrderListModel.dart';
import 'package:amen_inch/ui/review/ReviewScreen.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

// ignore: must_be_immutable
class OrderListTiles extends StatelessWidget {
  String id;
  String title;
  String address;
  String image;
  String date;
  String total;
  String status;
  String isRated;
  Review review;
  List<Items> items;

  OrderListTiles({Key key,
    @required this.id,
    @required this.title,
    @required this.address,
    @required this.image,
    @required this.date,
    @required this.total,
    @required this.status,
    @required this.isRated,
    @required this.review,
    @required this.items})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isRatedOrder;
    String time;
    Color statusColor;
    if (isRated == '0')
      isRatedOrder = false;
    else
      isRatedOrder = true;

    final itemsList = <Widget>[];

    for (int i = 0; i < items.length; i++) {
      itemsList.add(new Container(
        padding: EdgeInsets.only(left: 10, bottom: 5, top: 5),
        child: Align(
          alignment: Alignment.topLeft,
          child: new Text("${i + 1}" + ". " + items[i].title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 16,
                  fontWeight: FontWeight.bold)),
        ),
      ));
    }
    time = date;

    return InkWell(
      onTap: () {},
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
            child: Card(
              color: Colors.white,
              elevation: 5,
              child: Wrap(
                children: [
              Column(
              children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment:
                      MainAxisAlignment.spaceBetween,
                      children: [
                      Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 5, top: 5),
                      child: Text("# ${id}",
                          maxLines: 1,
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold)),
                    ),
                    Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(right: 5, top: 5),
                        child: Text(time,
                        maxLines: 1,
                        style: TextStyle(
                        color: Colors.red,
                        fontSize: 12,
                        fontWeight: FontWeight.bold)),
                  ),
                ],
              ),
            )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Container(
                  alignment: Alignment.topCenter,
                  child: new Column(
                    children: itemsList,
                  )),
              new Container(
                padding: EdgeInsets.only(right: 5, top: 5),
                child: Align(
                  alignment: Alignment.topRight,
                  child: new Text(total,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold)),
                ),
              )
            ],
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  new Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(
                        left: 5, right: 5, top: 3, bottom: 3),

                    child: Align(
                      alignment: Alignment.topLeft,
                      child: new Text(
                          AppUtils.getLocalizedString(
                              "ORDER_STATUS"),
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontWeight: FontWeight.bold)),
                    ),
                  )
                ],
              ),
              InkWell(
                onTap: () {

                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(
                        left: 10, right: 5, bottom: 5,),
                      padding: EdgeInsets.only(
                          left: 8, right: 8, top: 3, bottom: 3),
                      decoration: BoxDecoration(
                        color: getStatusColor(status),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: new Text(
                            status,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ),
      ],
    )),
    ),
    ],
    ),
    );
    }

  Color getStatusColor(String status) {
    if (status == "Received") {
      return Colors.deepPurple;
    } else if (status == "In Progress") {
      return Colors.yellow;
    } else if(status == 'Delivered'){
      return Colors.green;
    }
  }
}

class RestaurantTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            AppUtils.getLocalizedString("TRENDING_THIS_WEEK"),
            style: TextStyle(
                fontSize: 20,
                color: Color(0xFF3a3a3b),
                fontWeight: FontWeight.bold),
          ),
          Text(
            AppUtils.getLocalizedString("VIEW_ALL"),
            style: TextStyle(
                fontSize: 16, color: Colors.red, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class CompletedOrderListItems extends StatelessWidget {
  OrderListModel orderListModel;

  CompletedOrderListItems({Key key, @required this.orderListModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    if (orderListModel == null || orderListModel.data == null) {
    /*  for (var i = 0; i < 2; i++) {
        children.add(new OrderListTiles(
          id: "",
          date: '00',
          total: '00',
          status: 'New Order',
          items: [],
        ));
      }*/

      children.add(new Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(20),
        child: Align(
          alignment: Alignment.center,
          child: Text(
            AppUtils.getLocalizedString("NO_DATA_FOUND"),
            style: TextStyle(
                fontSize: 20,
                color: Color(0xFF3a3a3b),
                fontWeight: FontWeight.bold),
          ),
        )));
    } else {
      for (var i = 0; i < orderListModel.data.length; i++) {
        children.add(new OrderListTiles(
          id: orderListModel.data[i].id,
          date: orderListModel.data[i].date,
          total: orderListModel.data[i].total,
          status: orderListModel.data[i].status,
          items: orderListModel.data[i].items,
        ));
      }
    }

    return ListView(
        scrollDirection: Axis.vertical,
        primary: false,
        shrinkWrap: true,
        children: children);
  }
}


void _showAcceptDialog(BuildContext context) async {
  await showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(AppUtils.getLocalizedString("DO_YOU_WANT_TO_LOGOUT")),
        content: Text('We hate to see you leave...'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              print("you choose no");
              Navigator.of(context).pop(false);
            },
            child: Text(AppUtils.getLocalizedString("NO"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
          FlatButton(
            onPressed: () {

            },
            child: Text(AppUtils.getLocalizedString("YES"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
        ],
      );
    },
  );
}

void _showRejectDialog(BuildContext context) async {
  await showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(AppUtils.getLocalizedString("REJECT_TITLE")),
        content: Text(AppUtils.getLocalizedString("REJECT_MSG")),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              print("you choose no");
              Navigator.of(context).pop(false);
            },
            child: Text(AppUtils.getLocalizedString("CANCEL"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
          FlatButton(
            onPressed: () {
              print("you choose yes");
              Navigator.of(context).pop(false);
            },
            child: Text(AppUtils.getLocalizedString("YES"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
        ],
      );
    },
  );
}
