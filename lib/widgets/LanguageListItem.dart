import 'package:amen_inch/ui/language/LanguageListModel.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

// ignore: must_be_immutable
class LanguageListItemWidget extends StatefulWidget {
  LanguageListModel languageListModel;
  Store store;
  String selectedLan;

  LanguageListItemWidget(
      {Key key,
      @required this.languageListModel,
      @required this.store,
      @required this.selectedLan})
      : super(key: key);

  @override
  _LanguageItemState createState() => _LanguageItemState();
}

class _LanguageItemState extends State<LanguageListItemWidget> {
  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    if (widget.languageListModel == null ||
        widget.languageListModel.data == null) {
      for (var i = 0; i < 1; i++) {
        children.add(InkWell(
          onTap: () {},
          child: Column(
            children: <Widget>[
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
                child: Container(
                  width: double.infinity,
                  height: 50,
                  child: Row(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Container(
                          alignment: Alignment.topLeft,
                          width: 30,
                          height: 30,
                          padding: EdgeInsets.all(5.0),
                          child: Icon(Icons.error),
                        ),
                      ),
                      Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    alignment: Alignment.topLeft,
                                    padding: EdgeInsets.only(left: 5, top: 5),
                                    child: Text("N/A",
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  Container(
                                    alignment: Alignment.topLeft,
                                    padding: EdgeInsets.only(left: 5, top: 5),
                                    child: Text("N/A",
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.normal)),
                                  ),
                                ],
                              ),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
              Container(
                color: Colors.black26,
                height: 1,
              )
            ],
          ),
        ));
      }
    } else {
      for (var i = 0; i < widget.languageListModel.data.length; i++) {
        if (widget.languageListModel.data[i].title == widget.selectedLan) {
          widget.languageListModel.data[i].isSelected = true;
        } else {
          widget.languageListModel.data[i].isSelected = false;
        }

        children.add(InkWell(
          onTap: () {},
          child: Column(
            children: <Widget>[
              new Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
                width: double.infinity,
                height: 50,
                child: Row  (
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left: 5, top: 5),
                              child: Text(
                                  widget.languageListModel.data[i].title,
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ],
                        ),
                      ],
                    ),
                    IconButton(
                      icon: _iconControl(
                          widget.languageListModel.data[i].isSelected),
                      onPressed: () {
                        setState(() {
                          for (int j = 0;
                          j < widget.languageListModel.data.length;
                          j++) {
                            if (j == i) {
                              widget.languageListModel.data[j]
                                  .isSelected = true;
                              PreferencesHelper.setLanguage(
                                  widget.languageListModel.data[j].title);
                              PreferencesHelper.setLanguageCode(
                                  widget.languageListModel.data[j].code);
                              widget.selectedLan =
                                  widget.languageListModel.data[j].title;
                              Constants.selectedLanCode =
                                  widget.languageListModel.data[j].code;
                            } else {
                              widget.languageListModel.data[j]
                                  .isSelected = false;
                            }
                          }
                        });
                      },
                    ),
                  ],
                ),
              ),
              new Container(
                color: Colors.black26,
                height: 1,
              )
            ],
          ),
        ));
      }
    }

    return Wrap(
      children: [
        new ListView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: children,
        ),
      ],
    );
  }

  _iconControl(bool like) {
    if (like == false) {
      return Icon(Icons.radio_button_off);
    } else {
      return Icon(
        Icons.radio_button_on,
        color: Colors.red,
      );
    }
  }
}
