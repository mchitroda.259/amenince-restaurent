import 'package:amen_inch/animation/ScaleRoute.dart';
import 'package:amen_inch/ui/detail/FoodDetailScreen.dart';
import 'package:amen_inch/ui/menuList/MenuListActionsModel.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MenuListWidget extends StatefulWidget {
  MenuListActionsModel restaurantListModel;

  MenuListWidget({Key key, @required this.restaurantListModel})
      : super(key: key);

  @override
  _MenuListWidgetState createState() => _MenuListWidgetState();
}

class _MenuListWidgetState extends State<MenuListWidget> {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        new RestaurantListItems(
          restaurantListModel: widget.restaurantListModel,
        ),
      ],
    );
  }
}

// ignore: must_be_immutable
class RestaurantTiles extends StatelessWidget {
  String id;
  String title;
  String image;
  String description;
  String price;
  MenuData menuData;

  RestaurantTiles(
      {Key key,
      @required this.id,
      @required this.title,
      @required this.image,
      @required this.description,
      @required this.price,
      @required this.menuData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemWidth = size.width;

    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            ScaleRoute(
                page: FoodDetailScreen(
              menuData: menuData,
            )));
      },
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 5, right: 2, top: 5, bottom: 5),
            child: Card(
                color: Colors.white,
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                ),
                child: Container(
                  padding: EdgeInsets.all(5),
                  width: itemWidth,
                  height: 100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            alignment: Alignment.center,
                            height: 90,
                            width: 90,
                            child: CachedNetworkImage(
                              imageUrl: image,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                height: 130.0,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                          ),
                          Expanded(child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    width:
                                    MediaQuery.of(context).size.width -
                                        220,
                                    padding: EdgeInsets.only(left: 5, top: 5),
                                    child: Text(title.isEmpty ? "N/A" : title,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                  Container(
                                    width:
                                  100,
                                    padding: EdgeInsets.only(right: 5, top: 5),
                                    child: Text(price.isEmpty ? "N/A" : price,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500)),
                                  )
                                ],
                              ),

                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.topLeft,
                                        padding: EdgeInsets.only(
                                            left: 5, top: 5, right: 5),
                                        width:
                                        MediaQuery.of(context).size.width -
                                            120,
                                        child: Text(
                                            description.isEmpty
                                                ? "N/A"
                                                : description,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: Color(0xFF6e6e71),
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500)),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),flex: 3,),
                 /*         Expanded(child: Container(
                            width: MediaQuery.of(context).size.width - 310,
                            child: Text(price.isEmpty ? "N/A" : price,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500)),
                          ),flex: 1,),*/

                        ],
                      ),
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }
}

class RestaurantTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "Trending this week",
            style: TextStyle(
                fontSize: 20,
                color: Color(0xFF3a3a3b),
                fontWeight: FontWeight.bold),
          ),
          Text(
            "View all",
            style: TextStyle(
                fontSize: 16, color: Colors.red, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class RestaurantListItems extends StatelessWidget {
  MenuListActionsModel restaurantListModel;

  RestaurantListItems({Key key, @required this.restaurantListModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    if (restaurantListModel == null || restaurantListModel.data == null) {
      children.add(new Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: Align(
            alignment: Alignment.center,
            child: Text(
                AppUtils.getLocalizedString("NO_DATA_FOUND"),
                style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF3a3a3b),
                  fontWeight: FontWeight.bold),
            ),
          )));
    } else {
      for (var i = 0; i < restaurantListModel.data.length; i++) {
        children.add(new RestaurantTiles(
          id: restaurantListModel.data[i].id,
          title: restaurantListModel.data[i].title,
          image: restaurantListModel.data[i].image,
          description: restaurantListModel.data[i].description,
          price: restaurantListModel.data[i].priceTxt,
          menuData: restaurantListModel.data[i],
        ));
      }
    }

    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = 220;
    final double itemWidth = size.width / 2;

    return ListView(
        scrollDirection: Axis.vertical,
        primary: false,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        children: children);
  }
}
