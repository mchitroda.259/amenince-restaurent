import 'package:amen_inch/appstore/AppState.dart';
import 'package:amen_inch/ui/orderList/OrderListActions.dart';
import 'package:amen_inch/ui/orderList/OrderListModel.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:redux/redux.dart';

// ignore: must_be_immutable
class OrderListTiles extends StatelessWidget {
  String id;
  String date;
  String total;
  String status;
  String pickUpTime;
  List<Items> items;
  Store<AppState> store;

  OrderListTiles({
    Key key,
    @required this.id,
    @required this.date,
    @required this.total,
    @required this.status,
    @required this.pickUpTime,
    @required this.items,
    @required this.store,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String time;

    String totalString = AppUtils.getLocalizedString("TOTAL") + " " + total;
    final actionView = <Widget>[];
    final timerView = <Widget>[];

    final itemsList = <Widget>[];

    for (int i = 0; i < items.length; i++) {
      itemsList.add(new Padding(
        padding: EdgeInsets.all(8),
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            new Text(/*"${i + 1}" + ". " +*/ items[i].title,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
            new Text(items[i].amount,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
          ],
        ),
      ));
    }

    int endTime = getTimeDiff(pickUpTime);
    //int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 30;

    print("${endTime}"); // prints 7:40

    if (status == "Received") {
      time = date;
    } else {
      time = AppUtils.getLocalizedString("PICK_UP_TIME") + " " + pickUpTime;
    }

    if (status == "Received") {
      actionView.add(
        Container(
          width: 235,
          child: TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.grey[300]),
              overlayColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.hovered))
                    return Colors.black.withOpacity(0.04);
                  if (states.contains(MaterialState.focused) ||
                      states.contains(MaterialState.pressed))
                    return Colors.black.withOpacity(0.12);
                  return null; // Defer to the widget's default.
                },
              ),
            ),
            onPressed: () {
              DatePicker.showTime12hPicker(context, showTitleActions: true,
                  onChanged: (date) {
                print('change $date in time zone ' +
                    date.timeZoneOffset.inHours.toString());
              }, onConfirm: (date) {
                String formattedTime = DateFormat.jm().format(date);
                print('confirm $formattedTime');

                store.dispatch(
                    updateOrder(id, "Accepted", formattedTime, onFailure));
              }, currentTime: DateTime.now());
            },
            child: Text(AppUtils.getLocalizedString("CHANGE_PICK_UP_TIME"),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.normal)),
          ),
        ),
      );
      actionView.add(new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.green[300]),
              overlayColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.hovered))
                    return Colors.white.withOpacity(0.04);
                  if (states.contains(MaterialState.focused) ||
                      states.contains(MaterialState.pressed))
                    return Colors.white.withOpacity(0.12);
                  return null; // Defer to the widget's default.
                },
              ),
            ),
            onPressed: () {
              store.dispatch(updateOrder(id, "Accepted", "", onFailure));
            },
            child: RichText(
              text: TextSpan(
                children: [
                  WidgetSpan(
                    child: Icon(Icons.check_circle, size: 20),
                  ),
                  TextSpan(
                      text: " " + AppUtils.getLocalizedString("ACCEPT"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.normal)),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.red[300]),
              overlayColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.hovered))
                    return Colors.white.withOpacity(0.04);
                  if (states.contains(MaterialState.focused) ||
                      states.contains(MaterialState.pressed))
                    return Colors.white.withOpacity(0.12);
                  return null; // Defer to the widget's default.
                },
              ),
            ),
            onPressed: () {
              _showRejectDialog(context, store, id);
            },
            child: RichText(
              text: TextSpan(
                children: [
                  WidgetSpan(
                    child: Icon(Icons.cancel, size: 20),
                  ),
                  TextSpan(
                      text: " " + AppUtils.getLocalizedString("REJECT"),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.bold)),
                ],
              ),
            ),
          )
        ],
      ));
    } else if (status == 'Accepted') {
      /*actionView.add(
        Container(
          width: 218,
          child: TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.grey[300]),
              overlayColor: MaterialStateProperty.resolveWith<Color>(
                (Set<MaterialState> states) {
                  if (states.contains(MaterialState.hovered))
                    return Colors.black.withOpacity(0.04);
                  if (states.contains(MaterialState.focused) ||
                      states.contains(MaterialState.pressed))
                    return Colors.black.withOpacity(0.12);
                  return null; // Defer to the widget's default.
                },
              ),
            ),
            onPressed: () {
              DatePicker.showTime12hPicker(context, showTitleActions: true,
                  onChanged: (date) {
                print('change $date in time zone ' +
                    date.timeZoneOffset.inHours.toString());
              }, onConfirm: (date) {
                String formattedTime = DateFormat.jm().format(date);
                print('confirm $formattedTime');

                store.dispatch(
                    updateOrder(id, "Accepted", formattedTime, onFailure));
              }, currentTime: DateTime.now());
            },
            child: Text(AppUtils.getLocalizedString("CHANGE_PICK_UP_TIME")),
          ),
        ),
      );*/
      timerView.add(new Padding(
        padding: EdgeInsets.all(8),
        child: new Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            new Text(AppUtils.getLocalizedString("TIME_LEFT_MSG"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),
            new CountdownTimer(
              endTime: endTime,
              widgetBuilder: (_, CurrentRemainingTime time) {
                if (time == null) {
                  return Text(AppUtils.getLocalizedString("TIMES_UP"));
                }
                return Text(
                    '${time.hours == null ? 00 : time.hours} : ${time.min == null ? 00 : time.min} : ${time.sec == null ? 00 : time.sec}');
              },
            )
            /* new Text(getTimeDiff(pickUpTime) + ' Minutes',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontWeight: FontWeight.bold)),*/
          ],
        ),
      ));
    }

    return InkWell(
      onTap: () {},
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
            child: Card(
                color: Colors.white,
                elevation: 5,
                child: Wrap(
                  children: [
                    Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(left: 5, top: 5),
                                    child: Text("#${id}",
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  Container(
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(right: 5, top: 5),
                                    child: Text(time,
                                        maxLines: 1,
                                        style: TextStyle(
                                            color: Colors.red,
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Container(
                                alignment: Alignment.topCenter,
                                child: new Column(
                                  children: itemsList,
                                )),
                            new Container(
                              padding: EdgeInsets.only(right: 5, top: 5),
                              child: Align(
                                alignment: Alignment.topRight,
                                child: new Text(totalString,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold)),
                              ),
                            )
                          ],
                        ),
                        new Container(
                          width: 240,
                          child: new Column(
                            children: actionView,
                          ),
                        ),
                        Divider(),
                        new Container(
                          child: new Column(
                            children: timerView,
                          ),
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                new Container(
                                  alignment: Alignment.center,
                                  padding: EdgeInsets.only(
                                      left: 5, right: 5, top: 3, bottom: 3),
                                  child: Align(
                                    alignment: Alignment.topLeft,
                                    child: new Text(
                                        AppUtils.getLocalizedString(
                                            "ORDER_STATUS"),
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                )
                              ],
                            ),
                            InkWell(
                              onTap: () {},
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  new Container(
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.only(
                                      left: 10,
                                      right: 5,
                                      bottom: 5,
                                    ),
                                    padding: EdgeInsets.only(
                                        left: 8, right: 8, top: 3, bottom: 3),
                                    decoration: BoxDecoration(
                                      color: getStatusColor(status),
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: new Text(status,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold)),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }

  Color getStatusColor(String status) {
    if (status == "Received") {
      return Colors.deepPurple;
    } else if (status == "Preparing") {
      return Colors.yellow;
    } else if (status == 'Accepted') {
      return Colors.green;
    } else if (status == 'Dispatched') {
      return Colors.orange;
    }
  }
}

// ignore: must_be_immutable
class OrderListItems extends StatelessWidget {
  OrderListModel orderListModel;

  Store<AppState> store;

  OrderListItems({Key key, @required this.orderListModel, @required this.store})
      : super(key: key);

  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    store.state.orderListState.orderListModel = null;
    store.dispatch(getOrderListActions(onFailure));
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    if (orderListModel == null || orderListModel.data == null) {
      children.add(new Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              AppUtils.getLocalizedString("NO_DATA_FOUND"),
              style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF3a3a3b),
                  fontWeight: FontWeight.bold),
            ),
          )));
    } else {
      for (var i = 0; i < orderListModel.data.length; i++) {
        children.add(new OrderListTiles(
          id: orderListModel.data[i].id,
          date: orderListModel.data[i].date,
          total: orderListModel.data[i].total,
          status: orderListModel.data[i].status,
          items: orderListModel.data[i].items,
          pickUpTime: orderListModel.data[i].pickupTime,
          store: store,
        ));
      }
    }

    return SmartRefresher(
      enablePullDown: true,
      enablePullUp: true,
      header: WaterDropHeader(),
      footer: CustomFooter(
        builder: (BuildContext context, LoadStatus mode) {
          Widget body;
          if (mode == LoadStatus.idle) {
            body = Text("pull up load");
          } else if (mode == LoadStatus.loading) {
            body = CupertinoActivityIndicator();
          } else if (mode == LoadStatus.failed) {
            body = Text("Load Failed!Click retry!");
          } else if (mode == LoadStatus.canLoading) {
            body = Text("release to load more");
          } else {
            body = Text("No more Data");
          }
          return Container(
            height: 55.0,
            child: Center(child: body),
          );
        },
      ),
      controller: _refreshController,
      onRefresh: _onRefresh,
      onLoading: _onLoading,
      child: ListView(
          scrollDirection: Axis.vertical,
          primary: false,
          shrinkWrap: true,
          children: children),
    );
  }
}

void _showAcceptDialog(BuildContext context) async {
  await showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(AppUtils.getLocalizedString("DO_YOU_WANT_TO_LOGOUT")),
        content: Text('We hate to see you leave...'),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              print("you choose no");
              Navigator.of(context).pop(false);
            },
            child: Text(AppUtils.getLocalizedString("NO"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
          FlatButton(
            onPressed: () {},
            child: Text(AppUtils.getLocalizedString("YES"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
        ],
      );
    },
  );
}

void _showRejectDialog(
    BuildContext context, Store<AppState> store, String id) async {
  await showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(AppUtils.getLocalizedString("REJECT_TITLE")),
        content: Text(AppUtils.getLocalizedString("REJECT_MSG")),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              print("you choose no");
              Navigator.of(context).pop(false);
            },
            child: Text(AppUtils.getLocalizedString("CANCEL"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
          FlatButton(
            onPressed: () {
              print("you choose yes");
              Navigator.of(context).pop(false);
              store.dispatch(updateOrder(id, "Rejected", "", onFailure));
            },
            child: Text(AppUtils.getLocalizedString("YES"),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
          ),
        ],
      );
    },
  );
}

onFailure(String error) {
  _showMessage(error);
}

void _showMessage(String message) {}

int getTimeDiff(String time) {
  var format = DateFormat("HH:mm a");
  var one = format.parse(DateFormat('HH:mm a').format(format.parse(time)));
  var two = format.parse(DateFormat('HH:mm a').format(DateTime.now()));
  print("${one}"); // prints 7:40

  print("${two}"); // prints 7:40

  print("${one.difference(two).inMinutes}"); // prints 7:40
  int endTime = one.difference(two).inSeconds;
  return DateTime.now().add(Duration(seconds: endTime)).millisecondsSinceEpoch;
}
