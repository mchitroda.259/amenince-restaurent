import 'package:amen_inch/animation/ScaleRoute.dart';
import 'package:amen_inch/ui/favorites/FavActions.dart';
import 'package:amen_inch/ui/menuItemDetails/MenuDetailScreen.dart';
import 'package:amen_inch/ui/menuItemDetails/ModelMenuDetail.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

// ignore: must_be_immutable
class FavItemWidget extends StatefulWidget {
  List<Products> products;
  Store store;

  FavItemWidget({Key key, @required this.products, @required this.store})
      : super(key: key);

  @override
  _MenuItemState createState() => _MenuItemState();
}

class _MenuItemState extends State<FavItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: [
        new TitleItems(
          products: widget.products,
          store: widget.store,
        ),
      ],
    );
  }
}

class Title extends StatefulWidget {
  Menu menu;

  Title({Key key, @required this.menu}) : super(key: key);

  @override
  _TitleState createState() => _TitleState();
}

class _TitleState extends State<Title> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 20, 10, 15),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                widget.menu.title,
                style: TextStyle(
                    fontSize: 16,
                    color: Color(0xFF3a3a3b),
                    fontWeight: FontWeight.bold),
              ),
              Text(
                "  ${widget.menu.products.length} " + " ITEMS",
                style: TextStyle(
                    fontSize: 12,
                    color: Color(0xFF3a3a3b),
                    fontWeight: FontWeight.normal),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            color: Colors.black26,
            height: 1,
          )
        ],
      ),
    );
  }
}

class TitleItems extends StatefulWidget {
  List<Products> products;
  Store store;

  TitleItems({Key key, @required this.products, @required this.store})
      : super(key: key);

  @override
  _TitleItemState createState() => _TitleItemState();
}

class _TitleItemState extends State<TitleItems> {
  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    if (widget.products == null) {
      for (var i = 0; i < 2; i++) {
        children.add(new MenuItemViewTiles(
          id: "",
          name: '----',
          imageUrl: "",
          rating: '0.0',
          numberOfRating: '0',
          price: '00',
          store: widget.store,
          products: widget.products[i],
        ));
      }
    } else {
      for (var i = 0; i < widget.products.length; i++) {
        children.add(new MenuItemViewTiles(
            id: widget.products[i].id,
            name: widget.products[i].title,
            imageUrl: widget.products[i].image,
            rating: '0.0',
            numberOfRating: '0',
            price: widget.products[i].priceTxt,
            store: widget.store,
            products: widget.products[i]));
      }
    }

    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = 200;
    final double itemWidth = size.width / 2;

    return GridView.count(
      // Create a grid with 2 columns. If you change the scrollDirection to
      // horizontal, this produces 2 rows.
      crossAxisCount: 2,
      childAspectRatio: (itemWidth / itemHeight),
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      controller: new ScrollController(keepScrollOffset: false),

      physics: NeverScrollableScrollPhysics(),
      // Generate 100 widgets that display their index in the List.
      children: children,
    );
  }
}

// ignore: must_be_immutable
class MenuItemViewTiles extends StatefulWidget {
  String id;
  String ownerId;
  String name;
  String imageUrl;
  String rating;
  String numberOfRating;
  String price;
  String slug;
  Products products;
  Store store;

  MenuItemViewTiles(
      {Key key,
      @required this.id,
      @required this.ownerId,
      @required this.name,
      @required this.imageUrl,
      @required this.rating,
      @required this.numberOfRating,
      @required this.price,
      @required this.slug,
      @required this.products,
      @required this.store})
      : super(key: key);

  @override
  _MenuItemViewState createState() => _MenuItemViewState();
}

class _MenuItemViewState extends State<MenuItemViewTiles> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemWidth = size.width / 2;
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            ScaleRoute(
                page: MenuDetailScreen(
              restaurantId: widget.ownerId,
              products: widget.products,
              restaurantImage: widget.imageUrl,
              restaurantName: widget.name,
              isFromFav: true,
            )));
      },
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
            child: Card(
                color: Colors.white,
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                child: Container(
                  width: itemWidth,
                  height: 182,
                  child: Column(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              alignment: Alignment.center,
                              width: itemWidth,
                              height: 130,
                              child: CachedNetworkImage(
                                imageUrl: widget.imageUrl,
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  width: itemWidth,
                                  height: 130.0,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    image: DecorationImage(
                                        image: imageProvider,
                                        fit: BoxFit.cover),
                                  ),
                                ),
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              alignment: Alignment.topRight,
                              child: Container(
                                height: 30,
                                width: 30,
                                // decoration: BoxDecoration(
                                //     shape: BoxShape.circle,
                                //     color: Colors.white70,
                                //     boxShadow: [
                                //       BoxShadow(
                                //         color: Color(0xFFfae3e2),
                                //         blurRadius: 25.0,
                                //         offset: Offset(0.0, 0.75),
                                //       ),
                                //     ]),
                                child: IconButton(
                                    onPressed: () {
                                      if (widget.products.cartId != "0" &&
                                          widget.products.cartId.isNotEmpty) {
                                        widget.store.dispatch(addToFavActions(
                                            widget.id, onFailure));
                                      } else {
                                        widget.store.dispatch(addToFavActions(
                                            widget.id, onFailure));
                                      }
                                    },
                                    iconSize: 20,
                                    icon: Icon(
                                      widget.products.isFav == "1"
                                          ? Icons.favorite
                                          : Icons.favorite_outline,
                                      color: AppUtils.hexToColor(
                                          AppConfig.COLOR_PRIMARY),
                                      size: 20,
                                    )),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: 180,
                            alignment: Alignment.bottomLeft,
                            padding: EdgeInsets.only(left: 5, top: 5),
                            child: Text(widget.name,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 14,
                                )),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.bottomLeft,
                            width: 134,
                            padding: EdgeInsets.only(left: 5, top: 10),
                            child: Text(widget.price,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold)),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 8, bottom: 8),
                            height: 20,
                            width: 20,
                            child: IconButton(
                                onPressed: () {
                                  if (widget.products.cartId != "0" &&
                                      widget.products.cartId.isNotEmpty) {
                                    widget.store.dispatch(updateCartActions(
                                        widget.products.cartId,
                                        "0",
                                        int.parse(widget.products.countInCart) +
                                            1,
                                        onFailure));
                                  } else {
                                    widget.store.dispatch(addCartActions(
                                        widget.id,
                                        "0",
                                        int.parse(widget.products.countInCart) +
                                            1,
                                        onFailure));
                                  }
                                },
                                iconSize: 20,
                                icon: Icon(
                                  Icons.add_shopping_cart,
                                  color: AppUtils.hexToColor(
                                      AppConfig.COLOR_PRIMARY),
                                )),
                          ),
                        ],
                      ),
                    ],
                  ),
                )),
          ),
        ],
      ),
    );
  }

  onFailure(String error) {
    _showMessage(error);
  }

  void _showMessage(String message) {
    setState(() {
      AppUtils.showToast(message);
    });
  }
}
