import 'package:amen_inch/ui/review/ReviewModel.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';

// ignore: must_be_immutable
class ReviewList extends StatefulWidget {
  ReviewModel reviews;
  Store store;

  ReviewList({Key key, @required this.reviews, @required this.store})
      : super(key: key);

  @override
  _ReviewListState createState() => _ReviewListState();
}

class _ReviewListState extends State<ReviewList> {
  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    if (widget.reviews == null) {
      children.add(new ReviewItems(
        reviewsList: null,
      ));
    } else if (widget.reviews.data == null) {
      children.add(new ReviewItems(
        reviewsList: null,
      ));
    } else {
      children.add(new ReviewItems(
        reviewsList: widget.reviews.data,
      ));
    }

    return Card(
      margin: EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
      child: Wrap(children: children),
    );
  }
}

class ReviewListItemTiles extends StatelessWidget {
  Store store;
  String id;
  String name;
  String rating;
  String review;
  String date;

  ReviewListItemTiles(
      {Key key,
      @required this.store,
      @required this.id,
      @required this.name,
      @required this.rating,
      @required this.review,
      @required this.date})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
            child: Container(
              width: double.infinity,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              alignment: Alignment.topLeft,
                              padding: EdgeInsets.only(left: 5, top: 5),
                              child: Text(name,
                                  maxLines: 1,
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500)),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            width: MediaQuery.of(context).size.width * 0.8,
            padding: EdgeInsets.only(top: 5),
            child: Text(review,
                style: TextStyle(
                    color: Color(0xFF6e6e71),
                    fontSize: 16,
                    fontWeight: FontWeight.w500)),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            padding: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
            child: Container(
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(left: 5, top: 5),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(rating,
                              maxLines: 1,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500)),
                          Icon(
                            Icons.star,
                            size: 17,
                            color: AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
                          )
                        ],
                      )),
                  Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(left: 5, top: 5),
                    child: Text(date,
                        maxLines: 1,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w500)),
                  ),
                ],
              ),
            ),
          ),
          Divider()
        ],
      ),
    );
  }
}

class ReviewItems extends StatelessWidget {
  List<Data> reviewsList;
  Store store;

  ReviewItems({Key key, @required this.reviewsList, @required this.store})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final children = <Widget>[];

    if (reviewsList == null || reviewsList == null) {
      children.add(new Container(
          alignment: Alignment.center,
          padding: EdgeInsets.all(20),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              AppUtils.getLocalizedString("NO_DATA_FOUND"),
              style: TextStyle(
                  fontSize: 20,
                  color: Color(0xFF3a3a3b),
                  fontWeight: FontWeight.bold),
            ),
          )));
    } else {
      for (var i = 0; i < reviewsList.length; i++) {
        children.add(new ReviewListItemTiles(
          store: store,
          id: reviewsList[i].id,
          name: reviewsList[i].name,
          rating: reviewsList[i].rating,
          review: reviewsList[i].review,
          date: reviewsList[i].date,
        ));
      }
    }

    return ListView(
      scrollDirection: Axis.vertical,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      children: children,
    );
  }
}
