import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:flutter/material.dart';

class SearchWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    TextEditingController codeController = TextEditingController();

    return Container(
        height: 60,
        padding: EdgeInsets.all(10),
        alignment: Alignment.centerLeft,

        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: const BorderRadius.all(
              Radius.circular(15.0),
            ),
          ),
          child: TextField(
            textAlignVertical: TextAlignVertical.center,
            textAlign: TextAlign.left,
            controller: codeController,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.zero,
              filled: true,
              prefixIcon: Icon(
                Icons.search,
              ),
              suffixIcon: IconButton(
                onPressed: () => codeController.clear(),
                icon: Icon(Icons.clear),
              ),
              fillColor: Color(0xFFFAFAFA),
              hintStyle: new TextStyle(color: Color(0xFFd0cece), fontSize: 18),
              hintText: "Search for restaurant or dishes",
            ),
          ),
        ));
  }
}
