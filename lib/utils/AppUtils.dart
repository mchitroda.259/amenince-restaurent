import 'dart:io';
import 'dart:ui';

import 'package:amen_inch/utils/language/LanguageEN.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'AppConfig.dart';
import 'Constants.dart';

class AppUtils {
  static Color hexToColor(String code) {
    return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  static Color getSafeAreaColor() {
    return Color(
        int.parse(AppConfig.SAFE_AREA_COLOR.substring(1, 7), radix: 16) +
            0xFF000000);
  }

  static bool isiOSDevice() {
//   return true;
    return Platform.isIOS;
  }

  static void hideKeyBoard(BuildContext context) {
    FocusScope.of(context).unfocus();
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  static void showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_LONG,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 2,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static bool isTabDevice(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return shortestSide > 500;
  }

  static bool isSmallDevice(BuildContext context) {
    var shortestSide = MediaQuery.of(context).size.shortestSide;
    return shortestSide <= 320;
  }

  static String getLocalizedString(String key) {
    if (Constants.selectedLanCode.toString() == "en")
      return LanguageEN.lanEn[key];
    else if (Constants.selectedLanCode.toString() == "hy") {
      return LanguageEN.lanAM[key];
    } else if (Constants.selectedLanCode.toString() == "ru") {
      return LanguageEN.lanRu[key];
    } else {
      return LanguageEN.lanEn[key];
    }
  }

  static AudioCache cache; // you have this
  static AudioPlayer player; // create this

  static void playFile() async {
    AudioCache audioCache = AudioCache();
    AudioPlayer advancedPlayer = AudioPlayer();

   /* audioCache = new AudioCache(prefix: 'assets/audio/');
    advancedPlayer = await audioCache.play('notification_sound.mpeg',
        isNotification: true);*/

    advancedPlayer.play("https://www.amen-inch.com/public/default/ringtone.mp3");
  }

  static void playFileMute() async {
    player?.stop(); // stop the file like this
  }

  static void stopFile() {
    player?.stop();
  }
}
