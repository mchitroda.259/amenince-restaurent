import 'dart:io';

import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'AppConfig.dart';
import 'AppUtils.dart';

class UIHelper {
  static AppBar getAppBar(
      Color bgColor,
      String appbarTitle,
      double titleSpacing,
      bool isActionEnable,
      BuildContext context,
      String cartCount,
      String cartScreenId) {
    return AppBar(
      backgroundColor: bgColor ?? AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
      titleSpacing: titleSpacing ?? 0,
      iconTheme: IconThemeData(
        color: Colors.white, //change your color here
      ),
      title: Text(
        appbarTitle,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      bottomOpacity: 0.0,
      elevation: 0.0,
      actions: <Widget>[
        isActionEnable
            ? Badge(
                position: BadgePosition.topEnd(top: 0, end: 3),
                animationDuration: Duration(milliseconds: 300),
                animationType: BadgeAnimationType.slide,
                badgeContent: Text(
                  cartCount,
                  style: TextStyle(color: Colors.white),
                ),
                child: IconButton(
                    icon: Icon(Icons.shopping_cart),
                    onPressed: () {
                      /*Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CartScreen(
                                  id: cartScreenId,
                                  isFromDetails:
                                      cartScreenId == "" ? false : true,
                                )),
                      );*/
                    }),
              )
            : Text(
                "",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
      ],
    );
  }

  static AppBar getAppBarWithImageTitle(
      Color bgColor,
      String appbarTitle,
      double titleSpacing,
      bool isActionEnable,
      BuildContext context,
      String cartCount) {
    return AppBar(
      backgroundColor: bgColor ?? AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
      titleSpacing: titleSpacing ?? 0,
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      title: Align(
        alignment: Alignment.center,
        child: new Padding(padding: EdgeInsets.only(right:45),
        child: SizedBox(
          height: 40,
          width: 150,
          child: new Image(
            image: AssetImage("assets/images/dashboard_logo.png"),
          ),
        ),)
      ),
      bottomOpacity: 0.0,
      elevation: 0.0,
    );
  }

  static Widget getScreen({Widget scaffold}) {
    return Container(
      color: AppUtils.getSafeAreaColor(),
      child: SafeArea(
        bottom: false,
        top: false,
        child: scaffold,
      ),
    );
  }

  Widget getTextInput(
      {TextEditingController controller,
      String labelText,
      FocusNode focusNode,
      FormFieldValidator validator,
      TextInputType inputType,
      Function onSubmit}) {
    return Platform.isIOS
        ? Container(
            padding: EdgeInsets.all(10),
            child: CupertinoTextField(
              onSubmitted: onSubmit,
              keyboardType: inputType,
              focusNode: focusNode,
              padding: EdgeInsets.all(10),
              controller: controller,
              placeholder: labelText,
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
            ),
          )
        : Container(
            padding: EdgeInsets.all(10),
            child: TextFormField(
              controller: controller,
              keyboardType: inputType,
              focusNode: focusNode,
              validator: validator,
              onFieldSubmitted: onSubmit,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: labelText,
              ),
            ),
          );
  }

  static Widget getFlatButton(BuildContext context,
      {String buttonName, Function doOnPress, dynamic backGroundColor}) {
    return Platform.isIOS
        ? CupertinoButton(
            color: backGroundColor == null
                ? null
                : backGroundColor is Color
                    ? backGroundColor
                    : AppUtils.hexToColor(backGroundColor) ?? null,
            child: Text(
              buttonName,
              style: TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              AppUtils.hideKeyBoard(context);
              doOnPress();
            },
          )
        : FlatButton(
            color: backGroundColor == null
                ? null
                : backGroundColor is Color
                    ? backGroundColor
                    : AppUtils.hexToColor(backGroundColor) ?? null,
            child: Text(
              buttonName,
              style: TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              AppUtils.hideKeyBoard(context);
              doOnPress();
            },
          );
  }

  static Widget getFlatButtonDynamic(BuildContext context,
      {String buttonName,
      Function doOnPress,
      dynamic backGroundColor,
      dynamic textColor}) {
    return Platform.isIOS
        ? CupertinoButton(
            color: backGroundColor == null
                ? null
                : backGroundColor is Color
                    ? backGroundColor
                    : AppUtils.hexToColor(backGroundColor) ?? null,
            child: Text(
              buttonName,
              style: TextStyle(
                fontSize: 16,
                color: textColor == null
                    ? null
                    : textColor is Color
                        ? textColor
                        : AppUtils.hexToColor(textColor) ?? null,
              ),
            ),
            onPressed: () {
              AppUtils.hideKeyBoard(context);
              doOnPress();
            },
          )
        : FlatButton(
            color: backGroundColor == null
                ? null
                : backGroundColor is Color
                    ? backGroundColor
                    : AppUtils.hexToColor(backGroundColor) ?? null,
            child: Text(
              buttonName,
              style: TextStyle(
                fontSize: 16,
                color: textColor == null
                    ? null
                    : textColor is Color
                        ? textColor
                        : AppUtils.hexToColor(textColor) ?? null,
              ),
            ),
            onPressed: () {
              AppUtils.hideKeyBoard(context);
              doOnPress();
            },
          );
  }
}
