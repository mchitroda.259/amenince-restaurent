import 'package:permission_handler/permission_handler.dart';

class PermissionUtils {
  Future<PermissionStatus> requestPermission(Permission permission,
      {onPermissionGranted, onDenied, onPermanentlyDenied}) async {
    final status = await permission.request();
    switch (status) {
      case PermissionStatus.granted:
        onPermissionGranted();
        return PermissionStatus.granted;
        break;
      case PermissionStatus.denied:
        if (onDenied != null) {
          onDenied();
          return PermissionStatus.denied;
        }
        break;
      case PermissionStatus.permanentlyDenied:
        if (onPermanentlyDenied != null) {
          onPermanentlyDenied();
          return PermissionStatus.permanentlyDenied;
        }
        break;
      case PermissionStatus.restricted:
        return PermissionStatus.restricted;
        break;
      default:
    }
  }
  
}
