import 'package:shared_preferences/shared_preferences.dart';
// To set value PreferencesHelper.setTutorialString('AKS');
// To get value
// PreferencesHelper.tutorialString.then((value) {
//                                print(value);
//                              });

class PreferenceConst {
  static const String TUTORIAL_STRING = 'TUTORIAL_STRING';
  static const String INTRO_VISITED = 'INTRO_VISITED';
  static const String AUTHENTICATED = 'AUTHENTICATED';
  static const String TOKEN = 'TOKEN';
  static const String USER_ID = 'USER_ID';
  static const String USER_NAME = 'USER_NAME';
  static const String USER_EMAIL = 'USER_EMAIL';
  static const String USER_PROFILE = 'USER_PROFILE';

  static const String CART_COUNT = 'CART_COUNT';
  static const String SELECTED_LAN = 'SELECTED_LAN';
  static const String SELECTED_LAN_CODE = 'SELECTED_LAN_CODE';
  static const String PASSCODE = 'PASSCODE';
  static const String FINGERPRINT_ENABLED = 'FINGERPRINT_ENABLED';
  static const String IS_FROM_NOTIFICATION = 'IS_FROM_NOTIFICATION';
  static const String FCM_TOKEN = 'FCM_TOKEN';

}

class PreferencesHelper {
  static Future<String> get tutorialString =>
      Prefs.getString(PreferenceConst.TUTORIAL_STRING);

  static Future setTutorialString(String value) =>
      Prefs.setString(PreferenceConst.TUTORIAL_STRING, value);

  static Future<String> get tokenString =>
      Prefs.getString(PreferenceConst.TOKEN);

  static Future setToken(String token) =>
      Prefs.setString(PreferenceConst.TOKEN, token);

  static Future<String> get userId =>
      Prefs.getString(PreferenceConst.USER_ID);

  static Future<String> get userEmail =>
      Prefs.getString(PreferenceConst.USER_EMAIL);

  static Future<String> get userName =>
      Prefs.getString(PreferenceConst.USER_NAME);

  static Future<String> get userProfile =>
      Prefs.getString(PreferenceConst.USER_PROFILE);

  static Future setLanguage(String language) =>
      Prefs.setString(PreferenceConst.SELECTED_LAN, language);

  static Future<String> get language =>
      Prefs.getString(PreferenceConst.SELECTED_LAN);

  static Future setLanguageCode(String languageCode) =>
      Prefs.setString(PreferenceConst.SELECTED_LAN_CODE, languageCode);

  static Future<String> get languageCode =>
      Prefs.getString(PreferenceConst.SELECTED_LAN_CODE);

  static Future setUserId(String userId) =>
      Prefs.setString(PreferenceConst.USER_ID, userId);

  static Future setUserName(String userName) =>
      Prefs.setString(PreferenceConst.USER_NAME, userName);

  static Future setUserProfile(String userProfile) =>
      Prefs.setString(PreferenceConst.USER_PROFILE, userProfile);

  static Future setUserEmail(String userEmail) =>
      Prefs.setString(PreferenceConst.USER_EMAIL, userEmail);

  static Future<int> get cartCount =>
      Prefs.getInt(PreferenceConst.CART_COUNT);

  static Future setCartCount(int cartCount) =>
      Prefs.setInt(PreferenceConst.CART_COUNT, cartCount);

  static Future<bool> get authenticated =>
      Prefs.getBool(PreferenceConst.AUTHENTICATED);

  static Future setAuthenticated(bool value) =>
      Prefs.setBool(PreferenceConst.AUTHENTICATED, value);

  static Future<String> get passcode =>
      Prefs.getString(PreferenceConst.PASSCODE);

  static Future setPasscode(String value) =>
      Prefs.setString(PreferenceConst.PASSCODE, value);

  static Future<bool> get introVisited =>
      Prefs.getBool(PreferenceConst.INTRO_VISITED);

  static Future setIntroVisited(bool value) =>
      Prefs.setBool(PreferenceConst.INTRO_VISITED, value);

  static Future<bool> get isFromNotification =>
      Prefs.getBool(PreferenceConst.IS_FROM_NOTIFICATION);

  static Future setFromNotification(bool value) =>
      Prefs.setBool(PreferenceConst.IS_FROM_NOTIFICATION, value);

  static Future<String> get fcmToken =>
      Prefs.getString(PreferenceConst.FCM_TOKEN);

  static Future setFCMToken(String value) =>
      Prefs.setString(PreferenceConst.FCM_TOKEN, value);
}

class Prefs {
  static Future<bool> getBool(String key) async {
    final p = await prefs;
    return p.getBool(key) ?? false;
  }

  static Future setBool(String key, bool value) async {
    final p = await prefs;
    return p.setBool(key, value);
  }

  static Future<int> getInt(String key) async {
    final p = await prefs;
    return p.getInt(key) ?? 0;
  }

  static Future setInt(String key, int value) async {
    final p = await prefs;
    return p.setInt(key, value);
  }

  static Future<String> getString(String key) async {
    final p = await prefs;
    return p.getString(key) ?? '';
  }

  static Future setString(String key, String value) async {
    final p = await prefs;
    return p.setString(key, value);
  }

  static Future<double> getDouble(String key) async {
    final p = await prefs;
    return p.getDouble(key) ?? 0.0;
  }

  static Future setDouble(String key, double value) async {
    final p = await prefs;
    return p.setDouble(key, value);
  }

  static Future<SharedPreferences> get prefs => SharedPreferences.getInstance();
}
