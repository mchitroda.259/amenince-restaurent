
class AppConfig {
  static const String APP_NAME = 'Amen Inch';
  static const String COLOR_PRIMARY = '#f1b516 ';
  static const String COLOR_PRIMARY_DARK = '#f1b516 ';
  static const String COLOR_PRIMARY_LIGHT = '#f1b516 ';
  static const String COLOR_SECONDARY = '#1e1f1e';
  static const String RED = '#ff0000';
  static const String COLOR_SECONDARY_DARK = '#1e1f1e';
  static const String COLOR_SECONDARY_LIGHT = '#1e1f1e';
  static const String APPBAR_TITLE_COLOR = '#ffffff';
  static const String SAFE_AREA_COLOR = '#ffffff';
  static const String GRAY = '#808080';

  static Map getRequiredParams() {
    return {
      'session': 'ssn_Qkfhtb',
    };
  }

  static Map getRequiredHeaders() {
    return {
      'Content-Type': 'application/x-www-form-urlencoded',
     /* 'secretkey': 'A8236CF83858F9E332EA649D247BBA7F',
      'X-Requested-With': 'XMLHttpRequest',*/
      'Accept': 'application/json',
    };
  }
}
