
import 'package:audioplayers/audioplayers.dart';

class Constants {
  static const String REQUIRED_USERNAME = 'Username cannot be blank.';
  static const String REQUIRED_PASSWORD = 'Password cannot be blank.';
  static const String REQUIRED_CONFIRM_PASSWORD = 'Confirm password cannot be blank.';
  static const String REQUIRED_CONFIRM_PASSWORD_NOT_MATCH = 'Confirm password not match with password.';
  static const String REQUIRED_PHONE_NUMBER = 'Please enter phone number.';
  static const String ENTER_VALID_PHONE_NUMBER = 'Enter valid phone number.';
  static const String ENTER_VALID_COUNTRY_CODE = 'Enter valid country code.';

  static const String ENTER_EMAIL = 'Please enter Email';
  static const String ENTER_FIRST_NAME = 'Please enter first name';
  static const String ENTER_LAST_NAME = 'Please enter last name';
  static const String ENTER_AGE = 'Please enter first name';
  static const String SHOW = 'Show';
  static const String HIDE = 'Hide';
  static const String YES = 'Yes';
  static const String NO = 'No';
  static const String OK = 'Ok';
  static const String CANCEL = 'Cancel';
  static const String NOT_NOW = 'Not now';
  static const String ARE_YOU_SURE = 'Are you sure?';
  static const String ALERT = 'Alert!';
  static const String PERMISSION_NEEDED =
      'This app really need this permission to use this application, Please allow this permission.';


  static const String ENTER_TITLE = 'Please enter title';
  static const String ENTER_DEC = 'Please enter description';
  static const String ENTER_PRICE = 'Please enter price';


  static String selectedLan = "";
  static String selectedLanCode = "";
  static String userId = "";
  static bool isFromMenuUpdate = false;
  static bool isFromNotification = false;

  static bool isFromLan = false;
  static bool isInit = true;

  static String currentLat = "0.0000";
  static String currentLong = "0.0000";
  static String NOTIFICATION_TOKEN = "";


  static AudioCache cache;
  static AudioPlayer player;
}
