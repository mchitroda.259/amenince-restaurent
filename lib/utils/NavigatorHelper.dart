import 'package:flutter/material.dart';
import 'package:amen_inch/dialogutils/DialogService.dart';
import 'package:get_it/get_it.dart';

class NavigationHelper {
  static GlobalKey<NavigatorState> navKey = new GlobalKey<NavigatorState>();
  // ignore: non_constant_identifier_names
  static GetIt locator = GetIt.instance;
  DialogService dialogService = NavigationHelper.locator<DialogService>();

  void pushScreen(dynamic screenToNavigate) {
    NavigationHelper.navKey.currentState
        .push(MaterialPageRoute(builder: (context) => screenToNavigate));
  }

  void pushScreenOnTop(String newRoute) {
    NavigationHelper.navKey.currentState
        .pushNamedAndRemoveUntil(newRoute, (Route<dynamic> route) => false);
  }

  void pushReplacementNamed(String newRoute) {
    NavigationHelper.navKey.currentState.pop(true);
    NavigationHelper.navKey.currentState
        .pushReplacementNamed(newRoute);

  }
}

class Routes {
  static const SPLASH_SCREEN = "SPLASH_SCREEN";
  static const MAIN_SCREEN = "MAIN_SCREEN";
  static const LOGIN_SCREEN = "LOGIN_SCREEN";
  static const REGISTER_SCREEN = "REGISTER_SCREEN";
  static const HOME = "HOME_SCREEN";
  static const INTRO = "INTRO_SCREEN";
  static const LOCATION = "LOCATION_SCREEN";
  static const FORGOT_PASS = "FORGOT_PASS_SCREEN";
  static const OTP = "OTP_SCREEN";
  static const PROFILE = "PROFILE_SCREEN";
  static const CART = "CART_SCREEN";
  static const FOOD_DETAIL = "FOOD_DETAIL_SCREEN";
  static const FAV_SCREEN = "FAV_SCREEN";
  static const RESTAURANT_SCREEN = "RESTAURANT_SCREEN";
  static const CHECKOUT_SCREEN = "CHECKOUT_SCREEN";
  static const ADD_ADDRESS_SCREEN = "ADD_ADDRESS_SCREEN";
  static const ADDRESS_LIST_SCREEN = "ADDRESS_LIST_SCREEN";
  static const LANGUAGE_LIST_SCREEN = "LANGUAGE_LIST_SCREEN";
  static const MENU_DETAIL_SCREEN = "MENU_DETAIL_SCREEN";
  static const PAYMENT_SCREEN = "PAYMENT_SCREEN";
  static const RESET_PASS_SCREEN = "RESET_PASS_SCREEN";
  static const REVIEW_SCREEN = "REVIEW_SCREEN";
  static const ORDER_LIST_SCREEN = "ORDER_LIST_SCREEN";



}
