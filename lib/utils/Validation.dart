import 'Constants.dart';

class Validation {
  String validatePassword(String value) {
    if (value == null || value.isEmpty)
      return Constants.REQUIRED_PASSWORD;
    else
      return null;
  }

  String validateConfirmPassword(String value1, String value2) {
    if (value1 == null || value1.isEmpty)
      return Constants.REQUIRED_PASSWORD;
    else if (value2 == null || value2.isEmpty)
      return Constants.REQUIRED_CONFIRM_PASSWORD;
    else if (value1 != value2)
      return Constants.REQUIRED_CONFIRM_PASSWORD_NOT_MATCH;
    else
      return null;
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (value == null || value.isEmpty) {
      return Constants.ENTER_EMAIL;
    } else if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validateUsername(String value) {
    if (value == null || value.isEmpty) {
      return Constants.REQUIRED_USERNAME;
    } else
      return null;
  }

  String validatePhoneNumber(String value) {
    if (value == null || value.isEmpty) {
      return Constants.REQUIRED_PHONE_NUMBER;
    } else if (value.length > 10) {
      return Constants.ENTER_VALID_PHONE_NUMBER;
    } else
      return null;
  }

  String validateString(String value, String message) {
    if (value == null || value.isEmpty) {
      return message;
    } else
      return null;
  }
}
