class CartCountModel {
  String success;
  String status;
  String message;
  Data data;

  CartCountModel({this.success, this.status, this.message, this.data});

  CartCountModel.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  String itemCount;
  String totalQuantity;

  Data({this.itemCount, this.totalQuantity});

  Data.fromJson(Map<String, dynamic> json) {
    itemCount = json['item_count'];
    totalQuantity = json['total_quantity'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['item_count'] = this.itemCount;
    data['total_quantity'] = this.totalQuantity;
    return data;
  }
}
