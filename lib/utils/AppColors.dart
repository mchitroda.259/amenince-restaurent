
class AppColors {
  static const String TEXT_COLOR_PRIMARY = '#00363a';
  static const String TEXT_COLOR_SECONDARY = '#c6a700';
  static const String GRAY = '#bababa';
  static const String BORDER_COLOR = '#dadada';
  static const String BLACK_LIGHT = '#4D4D4D';
  static const String GRADIENT_START_COLOR = '#FCFCFC';
  static const String GRADIENT_END_COLOR = '#E2E2E2';
  static const String LIST_ITEM_SELECTION = '#efefef';
  static const String TRANSPARENT = '#00ffffff';
  static const String GREY_BACKGROUND = '#F9F9F9';
}
