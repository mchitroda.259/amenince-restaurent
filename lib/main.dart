import 'dart:io';

import 'package:amen_inch/ui/detail/FoodDetailScreen.dart';
import 'package:amen_inch/ui/favorites/FavScreen.dart';
import 'package:amen_inch/ui/forgotpass/ForgotPassScreen.dart';
import 'package:amen_inch/ui/home/HomeState.dart';
import 'package:amen_inch/ui/intro/IntroScreen.dart';
import 'package:amen_inch/ui/language/LanguageScreen.dart';
import 'package:amen_inch/ui/login/LoginState.dart';
import 'package:amen_inch/ui/main/MainScreen.dart';
import 'package:amen_inch/ui/menuItemDetails/MenuDetailScreen.dart';
import 'package:amen_inch/ui/orderList/OrderListScreen.dart';
import 'package:amen_inch/ui/otpVerification/OTPPassScreen.dart';
import 'package:amen_inch/ui/otpVerification/OTPPassState.dart';
import 'package:amen_inch/ui/profile/ProfileScreen.dart';
import 'package:amen_inch/ui/registration/RegisterState.dart';
import 'package:amen_inch/ui/resetPassword/ResetPassScreen.dart';
import 'package:amen_inch/ui/menuList/MenuListScreen.dart';
import 'package:amen_inch/ui/review/ReviewScreen.dart';
import 'package:amen_inch/ui/splash/SplashState.dart';
import 'package:amen_inch/ui/webview/PayMentScreen.dart';
import 'package:amen_inch/utils/Constants.dart';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:amen_inch/ui/home/HomeScreen.dart';
import 'package:amen_inch/ui/login/LoginScreen.dart';
import 'package:amen_inch/ui/registration/RegisterScreen.dart';
import 'package:amen_inch/ui/splash/SplashScreen.dart';
import 'package:amen_inch/utils/AppConfig.dart';
import 'package:amen_inch/utils/AppUtils.dart';
import 'package:amen_inch/utils/NavigatorHelper.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:redux/redux.dart';
import 'package:redux_persist/redux_persist.dart';
import 'package:redux_persist_flutter/redux_persist_flutter.dart';
import 'package:redux_thunk/redux_thunk.dart';

import 'ModelNotification.dart';
import 'appstore/AppReducer.dart';
import 'appstore/AppState.dart';
import 'dialogutils/DialogManager.dart';
import 'dialogutils/DialogService.dart';

import 'package:firebase_core/firebase_core.dart';
import 'dart:async';
import 'package:flutter/foundation.dart';

AudioCache cache;
AudioPlayer player;

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print('_firebaseMessagingBackgroundHandler');
  await Firebase.initializeApp();
  AppUtils.playFile();

  if (message.data.isEmpty) {
    return;
  }

  Future onDidReceiveLocalNotification(int id, String title, String body,
      String payload) async {
    print('onDidReceiveLocalNotification');
  }

  PreferencesHelper.setFromNotification(true);
  print('Background notification is:- ');

  var channelAction = AndroidNotificationChannelAction.createIfNotExists;

  var androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'Amen Inch-new-4',
    'Amen Inch-new-4',
    'Amen Inch',
    icon: 'ic_launcher',
    importance: Importance.max,
    priority: Priority.min,
    ticker: 'ticker',
    channelShowBadge: true,
    channelAction: channelAction,
    styleInformation: BigTextStyleInformation(''),
    setAsGroupSummary: true,
    visibility: NotificationVisibility.public,
    sound: RawResourceAndroidNotificationSound('notification_sound'),
    playSound: true,
    groupKey: '111',
  );

  var iOSPlatformChannelSpecifics = new IOSNotificationDetails(
      sound: 'notification_sound.aiff', presentSound: true);
  var platformChannelSpecificss = new NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics);
  var initializationSettingsAndroid =
  new AndroidInitializationSettings('ic_launcher');
  var initializationSettingsIOS = new IOSInitializationSettings();
  var initializationSettings = new InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
  var flutterLocalNotificationsPluginn =
  new FlutterLocalNotificationsPlugin();
  flutterLocalNotificationsPluginn.initialize(initializationSettings,
      onSelectNotification: onSelectNotification);
  await flutterLocalNotificationsPluginn.show(
      DateTime
          .now()
          .microsecond,
      message.notification.title,
      message.notification.body,
      platformChannelSpecificss,
      payload: '');

  FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
    print('A new onMessageOpenedApp event was published!');
    AppUtils.showToast("Notification click-1");
    NavigationHelper.navKey.currentState.pushNamedAndRemoveUntil(
        Routes.SPLASH_SCREEN, (Route<dynamic> route) => false,
        arguments: true);
  });

  print('Handling a background message ${message.messageId}');
}

Future<void> onDidReceiveLocalNotification(int id, String title, String body,
    String payload) async {
  print('onDidReceiveLocalNotification');
}

Future<void> onSelectNotification(String payload) async {
  print('onSelectNotification');
  AppUtils.showToast("Notification click-2");
  NavigationHelper.navKey.currentState.pushNamedAndRemoveUntil(
      Routes.SPLASH_SCREEN, (Route<dynamic> route) => false,
      arguments: true);
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([]);
  await Firebase.initializeApp();

  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  /// Update the iOS foreground notification presentation options to allow
  /// heads up notifications.
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );

  final persistor = Persistor<AppState>(
    storage: FlutterStorage(),
    debug: true,
    serializer: JsonSerializer<AppState>(AppState.fromJson),
  );

  var previousState = await persistor.load();
  if (previousState != null) {
    if (previousState.homeState == null) {
      previousState.homeState = HomeState.initial();
    } else if (previousState.loginState == null) {
      previousState.loginState = LoginState.initial();
    } else if (previousState.registerState == null) {
      previousState.registerState = RegisterState.initial();
    } else if (previousState.otpState == null) {
      previousState.otpState = OTPState.initial();
    } else if (previousState.splashState == null) {
      previousState.splashState = SplashState.initial();
    }
  }

  final store = Store<AppState>(
    reducer,
    initialState: previousState,
    middleware: [persistor.createMiddleware(), thunkMiddleware],
  );
  runApp(MyApp(store: store));
}

class MyApp extends StatefulWidget {
  final Store<AppState> store;

  const MyApp({Key key, this.store}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  static AppLifecycleState _notification;
  static FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  final _firebaseMessaging = FirebaseMessaging.instance;
  bool _initialized = false;

  @override
  Widget build(BuildContext context) {
    AppUtils.isiOSDevice()
        ? FlutterStatusbarcolor.setStatusBarColor(Colors.white)
        : FlutterStatusbarcolor.setStatusBarColor(
        AppUtils.hexToColor(AppConfig.COLOR_PRIMARY_DARK));
    return StoreProvider(
      store: widget.store,
      child: MaterialApp(
        builder: (context, widget) =>
            Navigator(
              onGenerateRoute: (settings) =>
                  MaterialPageRoute(
                    builder: (context) =>
                        DialogManager(
                          child: widget,
                        ),
                  ),
            ),
        initialRoute: '/',
        title: AppConfig.APP_NAME,
        theme: ThemeData(
            primarySwatch: Colors.yellow,
            accentColor: AppUtils.hexToColor(AppConfig.COLOR_PRIMARY_DARK),
            primaryColor: AppUtils.hexToColor(AppConfig.COLOR_PRIMARY),
            primaryColorDark: AppUtils.hexToColor(AppConfig.COLOR_PRIMARY_DARK),
            unselectedWidgetColor:
            AppUtils.hexToColor(AppConfig.COLOR_PRIMARY_DARK),
            cursorColor: AppUtils.hexToColor(AppConfig.COLOR_SECONDARY_DARK)),
        navigatorKey: NavigationHelper.navKey,
        home: Scaffold(
          body: SplashScreen(),
        ),
        debugShowCheckedModeBanner: false,
        routes: <String, WidgetBuilder>{
          Routes.SPLASH_SCREEN: (context) => SplashScreen(),
          Routes.MAIN_SCREEN: (context) => MainScreen(),
          Routes.LOGIN_SCREEN: (context) => LoginScreen(),
          Routes.REGISTER_SCREEN: (context) => RegisterScreen(),
          Routes.HOME: (context) => HomeScreen(),
          Routes.INTRO: (context) => IntroScreen(),
          Routes.FORGOT_PASS: (context) => ForgotPassScreen(),
          Routes.OTP: (context) => OTPScreen(),
          Routes.PROFILE: (context) => ProfileScreen(),
          Routes.ORDER_LIST_SCREEN: (context) => OrderListScreen(),
          Routes.FOOD_DETAIL: (context) => FoodDetailScreen(),
          Routes.FAV_SCREEN: (context) => FavScreen(),
          Routes.RESTAURANT_SCREEN: (context) => MenuListScreen(),
          Routes.LANGUAGE_LIST_SCREEN: (context) => LanguageScreen(),
          Routes.MENU_DETAIL_SCREEN: (context) => MenuDetailScreen(),
          Routes.PAYMENT_SCREEN: (context) => PayMentScreen(),
          Routes.RESET_PASS_SCREEN: (context) => ResetPassScreen(),
          Routes.REVIEW_SCREEN: (context) => ReviewScreen(),
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    NavigationHelper.locator.registerLazySingleton(() => DialogService());

    startFirebase(widget.store);

    var initializationSettingsAndroid =
    new AndroidInitializationSettings("ic_launcher");
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print('A new onMessageOpenedApp event was published!');
      NavigationHelper.navKey.currentState.pushNamedAndRemoveUntil(
          Routes.SPLASH_SCREEN, (Route<dynamic> route) => false,
          arguments: true);
    });
  }

  Future selectNotification(String payload) async {
    print(payload);
    NavigationHelper.navKey.currentState.pushNamedAndRemoveUntil(
        Routes.SPLASH_SCREEN, (Route<dynamic> route) => false,
        arguments: true);
  }

  Future<void> startFirebase(Store<AppState> store) async {
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {
        showNotification(message);
      }
    });

    if (Constants.NOTIFICATION_TOKEN == null ||
        Constants.NOTIFICATION_TOKEN.isEmpty) {
      await _firebaseMessaging.getToken().then((token) {
        print("New FirebaseMessaging token: $token");
        if (token != null && token.isNotEmpty) {
          Constants.NOTIFICATION_TOKEN = token;
          PreferencesHelper.setFCMToken(token);
        } else
          print("FCM token:- " + Constants.NOTIFICATION_TOKEN);
      });
    }

    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );

    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
        MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );

    _firebaseMessaging.requestPermission(
        sound: true, badge: true, alert: true, provisional: true);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      Constants.isFromNotification = true;
      showNotification(message);
    });
  }

  Future<void> showNotification(RemoteMessage message) async {
    AppUtils.playFile();
    var channelAction = AndroidNotificationChannelAction.createIfNotExists;

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      'Amen Inch-new-4',
      'Amen Inch-new-4',
      'Amen Inch',
      importance: Importance.max,
      priority: Priority.max,
      ticker: 'ticker',
      channelShowBadge: true,
      channelAction: channelAction,
      styleInformation: BigTextStyleInformation(''),
      setAsGroupSummary: true,
      visibility: NotificationVisibility.public,
      groupKey: '111',
      sound: RawResourceAndroidNotificationSound('notification_sound'),
      playSound: true,
    );

    var iOSPlatformChannelSpecifics = new IOSNotificationDetails(
        sound: 'notification_sound.aiff', presentSound: true);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
        DateTime
            .now()
            .microsecond,
        message.notification.title,
        message.notification.body,
        platformChannelSpecifics,
        payload: '111' + ',');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    setState(() {
      _notification = state;
    });
  }
}
