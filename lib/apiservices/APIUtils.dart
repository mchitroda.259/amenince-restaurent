class APIUtils {
  String getJSONString(json) {
    if (json != null && json != '')
      return json;
    else
      return '';
  }

  int getJSONInt(json) {
    if (json != null)
      return json;
    else
      return 0;
  }

  bool getJSONBoolean(json) {
    if (json != null)
      return json;
    else
      return false;
  }
}
