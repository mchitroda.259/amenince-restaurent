class APIException implements Exception {
  final _message;
  final int errorCode;

  APIException([this._message, this.errorCode]);

  String toString() {
    return "$errorCode :- $_message";
  }
}

class FetchDataException extends APIException {
  FetchDataException([message, errorCode])
      : super("Error During Communication: " + message, errorCode);
}

class BadRequestException extends APIException {
  BadRequestException([message, errorCode])
      : super("Invalid Request: " + message, errorCode);
}

class UnauthorisedException extends APIException {
  UnauthorisedException([message, errorCode])
      : super("Unauthorised: " + message, errorCode);
}

class InvalidInputException extends APIException {
  InvalidInputException([message, errorCode])
      : super("Invalid Input: " + message, errorCode);
}

class InvalidResponseException extends APIException {
  InvalidResponseException([message, errorCode])
      : super("Invalid Response: " + message, errorCode);
}
