class APIEndpoints {
  static const String BASE_URL = "https://www.amen-inch.com/";
  static const String LOGIN = "api/restaurant/login";
  static const String GET_ORDER_LIST = "api/restaurant/orders";
  static const String GET_COMPLETED_ORDER_LIST = "api/restaurant/complete_orders";
  static const String UPDATE_ORDER = "api/restaurant/changeOrderStatus";
  static const String GET_MENU_LIST = "api/restaurant/menuList";
  static const String GET_REVIEWS = "api/restaurant/reviews";
  static const String GET_LANGUAGE = "api/restaurant/get_languages";
  static const String GET_PROFILE = "api/restaurant/get_myProfile";
  static const String DASHBOARD = "api/restaurant/get_dashboardData";
  static const String UPDATE_MENU = "api/restaurant/updateMenu";
  static const String UPDATE_PROFILE = "api/restaurant/updateProfile";

  static const String REGISTRATION = "api/customer/registration";
  static const String UPDATE_PROFILE_PIC = "api/restaurant/update_profilePicture";


  static const String GET_FOOD_DETAILS = "api/customer/get_restaurantDetails";
  static const String ADD_CART = "api/customer/addToCart";
  static const String DELETE_CART = "api/customer/deleteCart";
  static const String GET_CART_COUNT = "api/customer/get_cartData";
  static const String ADD_FAV = "api/customer/save_favorites";

  static const String GET_CATEGORY = "api/customer/categories";


  static const String GET_CART_LIST = "api/customer/carts";
  static const String GET_TRENDING_LIST = "api/customer/get_favoriteList";
  static const String GET_RESTAURANT_LIST = "api/customer/get_restaurants";
  static const String GET_ADDRESS_LIST = "api/customer/get_addresses";
  static const String SAVE_ADDRESS = "api/customer/save_address";
  static const String GET_CITY_LIST = "api/customer/get_cities";
  static const String GET_COUNTRY_LIST = "api/customer/get_countries";
  static const String GET_STATE_LIST = "api/customer/get_state";

  static const String PLACE_ORDER = "api/customer/place_order";
  static const String ADD_VOICE_ORDER = "api/customer/add_voiceOrder";
  static const String DELETE_ADDRESS = "api/customer/delete_address";

  static const String VERIFY_OTP = "api/customer/verifyOTP";
  static const String SEND_OTP = "api/customer/sendOTP";
  static const String RESET_PASS = "api/customer/resetPassword";
  static const String SUBMIT_REVIEW = "api/customer/submitReview";

}
