import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:amen_inch/utils/PreferencesHelper.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import 'APIException.dart';

class ApiProvider {
  Future<dynamic> doGet(String endpoint, String url) async {
    var responseJson;
    try {
      String token = await PreferencesHelper.tokenString;
      String lanCode = await PreferencesHelper.languageCode;
      print('TOKEN :- ' + lanCode + " " + token);

      var getURL = Uri.parse(url + endpoint);

      final response = await http.get(
        getURL,
        headers: {
          HttpHeaders.authorizationHeader: 'Bearer $token',
          HttpHeaders.acceptLanguageHeader: lanCode
        },
      );

      responseJson = _response(response, endpoint);
    } on SocketException {
      throw FetchDataException('No Internet connection', 12163);
    } on TimeoutException {
      throw FetchDataException('Request Timeout', 408);
    } on FormatException {
      throw FetchDataException('Not Found', 404);
    }
    return responseJson;
  }

  Future<dynamic> doPost(baseUrl, String endpoint, {Map body}) async {
    var responseJson;
    try {
      var url = Uri.parse(baseUrl + endpoint);

      print('URL :- ' + url.toString() + '\n' + "R-Params" + body.toString());
      String lanCode = await PreferencesHelper.languageCode;
      print('TOKEN :- ' + lanCode);
      final response = await http.post(url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Accept-Language': lanCode,
          },
          body: body);
      responseJson = _response(response, endpoint);
    } on SocketException {
      throw FetchDataException('No Internet connection', 12163);
    } on TimeoutException {
      throw FetchDataException('Request Timeout', 408);
    } on FormatException {
      throw FetchDataException('Not Found', 404);
    }
    return responseJson;
  }

  Future<dynamic> doPostWithToken(baseUrl, String endpoint, {Map body}) async {
    var responseJson;
    try {
      var url = Uri.parse(baseUrl + endpoint);

      print(
          'URL :- ' + baseUrl + endpoint + '\n' + "R-Params" + body.toString());
      String token = await PreferencesHelper.tokenString;
      String lanCode = await PreferencesHelper.languageCode;
      print('TOKEN :- ' + lanCode + " " + token);
      final response = await http.post(url,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Authorization': 'Bearer $token',
            'Accept-Language': lanCode,
          },
          body: body);
      responseJson = _response(response, endpoint);
    } on SocketException {
      throw FetchDataException('No Internet connection', 12163);
    } on TimeoutException {
      throw FetchDataException('Request Timeout', 408);
    } on FormatException {
      throw FetchDataException('Not Found', 404);
    }
    return responseJson;
  }

  Future<dynamic> doMultipartRequest(MultipartRequest multipartRequest) async {
    var responseJson;
    try {
      final streamedResponse = await multipartRequest.send();
      final response = await http.Response.fromStream(streamedResponse);
      responseJson = _response(response, multipartRequest.fields.toString());
    } on SocketException {
      throw FetchDataException('No Internet connection', 12163);
    } on TimeoutException {
      throw FetchDataException('Request Timeout', 408);
    } on FormatException {
      throw FetchDataException('Not Found', 404);
    }
    return responseJson;
  }

  dynamic _response(http.Response response, String endpoint) {
    switch (response.statusCode) {
      case 200:
        print(response.body.toString());
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 400:
        print(json.decode(response.body.toString()));
        throw BadRequestException(
            response.body.toString(), response.statusCode);
      case 401:

      case 403:
        print(json.decode(response.body.toString()));
        throw UnauthorisedException(
            response.body.toString(), response.statusCode);
      case 404:
        print(json.decode(response.body.toString()));
        throw FetchDataException(response.body.toString(), response.statusCode);
      case 500:
      case 503:
      default:
        print(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
        throw FetchDataException(
            'Error occured while Communication with Server',
            response.statusCode);
    }
  }
}
